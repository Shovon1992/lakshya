import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { IwithdrawAmount } from 'src/app/interface/transation/iwithdraw-amount';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.page.html',
  styleUrls: ['./withdraw.page.scss'],
})
export class WithdrawPage extends BaseComponent implements OnInit {

  savingsAmount:number = 0;
  withdrawReasons = ['Medical','School','Personal'];
  currentSavings: number = 0;
  withdrawDetails: any = [];
  userAccountDetails  = JSON.parse(localStorage.getItem('userAccountDetails'))
  redeemOption:string = 'Y';
  loadder: any;   
  checked: boolean = false;
  paymentType = 1;
  withdrawType : String; 
  transationDetails: any;
  maxWithdrawAmount : any;
  ngOnInit() {
    document.addEventListener("backbutton",function(e) {
      console.log("disable back button")
    }, false);
  }

  ionViewDidEnter() {
    this.getBankAccountDetailsOfUser();
    this.getTransationDashbard();
  }

  // ionViewWillEnter(){
  //   this.getBankAccountDetailsOfUser();
  //   this.getTransationDashbard();
  // }

  getTransationDashbard(){
    // try {
      this.present('Sending your request..',10000);
      this.transationService.getTransationHistory(this.userAccountDetails.FolioNumber).subscribe(
        res => {
          this.dismiss();
          console.log("EEEEEEEEEEEEEEEEEEEE");
          if(res['Success']){  
            this.userAccountDetails = res['Account'];

            let totalSavingsAmount = parseInt(res['TotalBalanceAmount']);
            let currentSavingsAmount = parseInt(res['BalanceAmt']);
            this.savingsAmount = parseInt(res['TotalBalanceAmount']);

            if (totalSavingsAmount >= currentSavingsAmount) {
              this.currentSavings = currentSavingsAmount;
            }
            else {
              this.currentSavings = totalSavingsAmount;
            }            
          } else {  
            this.alertMessage('Error', (res['ExtendedMessage']) ? res['ExtendedMessage'] : res['ErrorMessage']);
          }         
        }, err =>{  
          this.dismiss();
          let dummyText = this.getInternationalText("popup","widthtxtpop5");
          this.alertMessage('Error', this.langText)
        }
      )
    // } catch (error) {  
    //   this.dismiss();
    //   this.alertMessage('Error','Sorry not able to fetch bank details get transaction')
    // }
  }
  /**
   * For withdraw amount
   */
  withdraw(){
    //if(!this.checked && (this.withdrawDetails.amount %1 != 0)) { //

    if( this.currentUserDetails.IsKYCCompleted){
      if(this.withdrawType == 'instant'){
        if(this.withdrawDetails.amount){
          if(!this.checked && !(this.withdrawDetails.amount.indexOf(".") == -1)) { 
            let dummyText = this.getInternationalText("popup","widthtxtpop5");
            this.alertMessage('Error',this.langText)
            this.withdrawDetails.amount = '';
            return false;
          }
      
          let enteredAmount = parseInt(this.withdrawDetails.amount);
          let nintyPercent = (enteredAmount/this.savingsAmount) * 90;
      
          if (!this.checked && nintyPercent >= 90) {
            let dummyText = this.getInternationalText("popup","widinstxtpop") ;
            this.alertMessage('Error',this.langText);
            this.withdrawDetails.amount = '';
            return false;
          }
          
          if(this.withdrawDetails.amount > this.maxWithdrawAmount){
            let dummyText = this.getInternationalText("popup","widthtxtpop7") ;
            this.alertMessage('Confirm !',this.langText+this.maxWithdrawAmount);
            return false;
          }
          // if (enteredAmount > this.currentSavings) {
          //   this.alertMessage('Error','Sorry! You don\'t have enough balance to withdraw.')
          //   this.withdrawDetails.amount = '';
          //   return false;
          // }
      
          if (this.checked) {
            this.paymentType = 2;
          } 
          else {
            this.paymentType = 1;
          }
        } else {
         // let dummyText = this.getInternationalText("popup","widthtxtpop7") ;
          this.alertMessage('Alet !','Please enter amount.')
          return false;
        }
        
      }
      
      let accountDetails = JSON.parse(localStorage.getItem('userAccountDetails'));
      let data : IwithdrawAmount = {
        FolioNo : accountDetails.FolioNumber,
        Amount: (this.withdrawType == 'instant') ? this.withdrawDetails.amount : this.savingsAmount,
        Units : 0,
        UserIpAddress: '10.10.10.10',
        Mobile: this.currentUserDetails.PhoneNumber,
        TransType: 2,
        RedemOption: (this.withdrawType == 'instant') ? 'Y' : 'N'
      }
      // if(data.Amount <= this.currentSavings){
        // try { 
          this.present('Sending your request..',30000);
          this.transationService.withdrawMoney(data).subscribe(
            res => {
              if(res['Success']){  
                this.dismiss();
                // this.alertMessage('Success', res['ExtendedMessage']);
                this.transationDetails = res;
                // this.getBackWithdrawPage(res['ExtendedMessage']);    
                if(this.withdrawType == 'instant'){
                  let dummyText = this.getInternationalText("popup","widthtxtpop6") ;
                  let msg ='Rs. '+this.withdrawDetails.amount+this.langText
                  this.router.navigate(['/withdrawalconfirm/' + this.withdrawDetails.amount +'/'+this.paymentType+'/'+msg]);
                } else {
                  this.router.navigate(['/withdraw-otp/' + res['ExtendedMessage'] + '/' + data.Amount]);
                } 
                // this.navCtrl.navigateRoot('withdraw')              
              } else {  
                this.dismiss();
                this.alertMessage('Error', (res['ExtendedMessage']) ? res['ExtendedMessage'] : res['ErrorMessage']);
              }         
            }, err =>{  
              this.dismiss();
              let dummyText = this.getInternationalText("popup","widthtxtpop5") ;
              this.alertMessage('Error', this.langText)
            }
          )
        // } catch (error) {  
        //   this.dismiss();
        //   this.alertMessage('Error','Sorry not able to fetch bank details')
        // } 
      // } else {
      //   this.alertMessage('Error','Sorry! You don\'t have enough balance to withdraw.');
      //     return false;
      // }
      
    } else{
      let dummyText = this.getInternationalText("popup","widthtxtpop4") ;
      this.alertMessage('Alert !', this.langText)
  }
      
  }

  // async getBackWithdrawPage(message){
  //   const alert = await this.alertCtrl.create({
  //       header: 'Confirm!',
  //       message: message,
  //       buttons: [
  //          {
  //           text: 'Ok',
  //           handler: () => {
  //             // this.events.publish('reloadHome', 'Load ');
  //             console.log(this.withdrawType)
  //             if(this.withdrawType == 'instant'){
  //               this.router.navigate(['/withdrawalconfirm/' + this.withdrawDetails.amount +'/'+this.paymentType+'/'+message]);
  //             } else if(this.withdrawType == 'complete'){
  //               this.router.navigate(['withdraw-otp/'+message])
  //             }                    
              
  //             // this.navCtrl.navigateRoot('withdrawalconfirm');                
  //           }
  //         }
  //       ]
  //     });
  
  //     await alert.present();
  // }

  /**
   * For checkbox
   */
  async checkTotal(event) {

    if(this.checked){
      this.withdrawDetails.amount = this.savingsAmount;
      let dummyText = this.getInternationalText("popup","widthtxtpop3") ;
      const alert = await this.alertCtrl.create({
        header: 'Confirm!',
        message: this.langText,
        buttons: [
           {
            text: 'Ok',
            handler: () => {
              this.redeemOption = 'N';                        
            }
          }
        ]
      });    
      await alert.present();    
    }
    else {
      this.withdrawDetails.amount = '';
      this.redeemOption = 'Y';
    }
  }


  checkMaxamount(event,amount) {
    const pattern = /[0-9,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
         // invalid character, prevent input
         event.preventDefault();
     }

     else if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
      event.preventDefault();
    } 
    if(this.withdrawDetails.amount > this.maxWithdrawAmount ) {
      this.withdrawDetails.amount = this.maxWithdrawAmount;
      event.preventDefault();
      return false;
    }
    let currentValue = String.fromCharCode(event.which);
    let finalValue = parseInt(amount + currentValue); 
    if(finalValue < 1 || finalValue >= this.savingsAmount || event.charCode == 46 ) {
      this.withdrawDetails.amount = this.maxWithdrawAmount;
      let dummyText = this.getInternationalText("popup","widthtxtpop9") ;
      let alerttxt:any;
      alerttxt= this.langText;
      let dummyText1 = this.getInternationalText("popup","widthtxtpop10") ;
      let alerttxt2 = this.langText;
      this.alertMessage('Alert',alerttxt+this.maxWithdrawAmount+alerttxt2)
      event.preventDefault();
    }
    

  }

  /**
   * Confirmation alert
   */
  async getBack(){
    //this.events.publish('reloadHome', 'Hello from page1!');                       
    this.navCtrl.navigateRoot('homeTab');
  }

  /**
   * Check withdraw type
   * 
   */
  async checkWithdrawType(type){
    // alert(type)
    switch(type){
      case 'instant' : 
      let amount = (this.savingsAmount*0.9).toString().split('.')[0]
      this.maxWithdrawAmount =parseInt(amount);
      let dummyText = this.getInternationalText("popup","widthtxtpop7") ;
          this.alertMessage('Confirm !',this.langText+amount);
          break;
      case 'complete' :
        let dummyText1 = this.getInternationalText("popup","widthtxtpop2") ;
        this.alertMessage('Confirm !',this.langText);
        break;
    }
  }


  async present(message,duration) {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      //duration: 10000,
      message: message,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        console.log(message);
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }
  /**
   *  End of class
   */
}
