import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-withdrawalconfirm',
  templateUrl: './withdrawalconfirm.page.html',
  styleUrls: ['./withdrawalconfirm.page.scss'],
})

export class WithdrawalconfirmPage extends BaseComponent implements OnInit {

  mileStones: any = [];
  html: any;
  goalDetails: any = [];
  getCurrentTransactionList: any =[];
  currentSaving = 0;
  
  totalNumberSet = 0;
  permonthAmount = 0;
  currentMilestone = 1;
  tansationAmount: any;
  transationType: any;
  statusMsg = '*Message sent to your number for UPI payment. Please check';
  bankName: any;
  submitPurchaseResponce: any = [];
  upiId: any;
  nextStop:number = 0;
  
  currentSavings1 = 0;
  currentSavings:any = 0;
  paymentModeResponce: any = [];
  userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'))
  transationDetails: any = [];

  transactionComplete = false;
  paidMessage = "Prossessing";
  disableContinueBtn = false;
  paidCounter = 0
  diabledDecBtn = false;
  diabledContinue = true;
  diabledContinueOnAmount = true;

  goalAmount = 0;
  totalInvest = 0;
  pathWidth: any;
  exactPathWidth: any;
  autoPosition = 5+"px";
  goalStatus = false;  
  posRation = 0;
  greenPos1:boolean = false;
  greenPos2:boolean = false;
  greenPos3:boolean = false;
  withdrawAmount = 0;
  paymentType = 1;
  transMessage : any;
  ngOnInit() {   
    this.getGoalStatus();
    this.activateRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('withdrawAmount')) {
        this.router.navigate(['/']);
      }
      this.withdrawAmount = +paramMap.get('withdrawAmount');
      this.paymentType = +paramMap.get('paymentType');
      this.transMessage = paramMap.get('transMessage');
    });
  }

	ionViewDidEnter() { }
  getGoalStatus() {
    try {
      this.present('Loading...',5000);
      this.transationService.getTransationHistory(this.userAccountDetails.FolioNumber).subscribe(res => {
         this.dismiss();
         if (res['Success']) {
            this.transationDetails = res;
            if(typeof(this.transationDetails['GoalDetail']['MileStone']) != 'undefined' || this.transationDetails['GoalDetail']['MileStone'] !== null) {
              this.goalStatus = true;
              this.getGoalDetails();
            } 
          }
        }, err => {
          // this.dismiss();
          // this.alertMessage('Error', 'Unable fetch transation details.');
        })
      } catch (error) {
          //console.log(error);
          // this.dismiss();
          // this.alertMessage('Error', 'Unable fetch transation details.');
      }
  }

  getGoalDetails() {
      try {
        this.present('Loading...',5000);
        this.transationService.getTransationHistory(this.userAccountDetails.FolioNumber).subscribe(res => {
          this.dismiss();

          if (res['Success']) {
            this.transationDetails = res;
            this.currentSavings1 = parseInt(res['BalanceAmt']);                    
            this.getTotalInvestment();

            console.log(this.transationDetails);

            if(typeof(res['GoalDetail']) != 'undefined' || res['GoalDetail'] !== null) {     
              this.goalStatus = true;
              // this.pathWidth = document.getElementById('save-auto-path').offsetWidth - 56;
           
              this.mileStones = res['GoalDetail']['MileStone'];
              this.goalDetails = res['GoalDetail'];
              this.currentGoalDetails(res['GoalDetail'],res['TransactionDetails']);
            
              this.currentSaving = 0;
              this.calculateCurrentSaving();//currentSaving
            

              this.totalNumberSet = Math.ceil((this.mileStones.length - 3) / 3);
              this.permonthAmount = parseInt(res['GoalDetail']['MileStone'][0]['MileStoneAmount']);
              this.currentMilestone = res['GoalDetail']['CurrentMileStone'];
              this.goalAmount = parseInt(res['GoalDetail']['GoalAmount']);
              this.nextStop = this.permonthAmount*(Math.floor(this.currentSavings1/this.permonthAmount) + 1);
              this.posRation = Math.floor(this.pathWidth/4);
              let autoPos = Math.floor((((this.currentSaving * 100)/this.goalAmount) * this.pathWidth)/100);  
              let autoExactPos;
      
              console.log(autoPos)
              console.log(this.currentSaving)

              if (autoPos <= 40) {
                autoExactPos = Math.floor(5);
              }
              else if(autoPos > this.pathWidth) {
                  autoExactPos = Math.floor(this.pathWidth);
              }
              else {
                autoExactPos = Math.floor(autoPos);
              }
                          
              console.log(this.posRation)
              console.log(autoExactPos);
      
              this.autoPosition = autoExactPos + 'px';
              this.greenPos1 = (this.posRation > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
              this.greenPos2 = (this.posRation * 2 > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
              this.greenPos3 = (this.posRation * 3 > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;

              this.currentSavings = 0;
              this.checkCurrentSavings();
            }

          } else {
            this.alertMessage('Error', res['ExtendedMessage']);
          }
        }, err => {
          this.dismiss();
          this.alertMessage('Error', 'Unable fetch transation details.');
        })
      } catch (error) {
          //console.log(error);
          this.dismiss();
          this.alertMessage('Error', 'Unable fetch transation details.');
      }
  }

    /**
     * Calculate Current goal
     */
    currentGoalDetails(goalDetails, transationList) {        
      let goalId = goalDetails['GoalId'];
      let goalStatus = goalDetails['GoalStatus'];
      this.getCurrentTransactionList = [];
      
      if (goalId && goalStatus == 2) {
          for(var i =0; i<transationList.length; i++) {
              if (transationList[i].GoalId == goalId) {
                  this.getCurrentTransactionList.push(transationList[i]);
              }
          }
      }
  }
  calculateCurrentSaving() {//currentSaving
     
      this.getCurrentTransactionList.forEach(element => {
          console.log(element.TransAmount);
          if (element.TransType == 1) {
              this.currentSaving += parseInt(element.TransAmount);
          } else if (element.TransType == 2) {
              this.currentSaving -= parseInt(element.TransAmount);
          }
      });

      console.log(this.currentSaving);
  }

  setAutoStyles() {
    let styles = {
        'width': "40px",
        'margin-bottom': "3px",
        'margin-top': '-43px',
        'margin-left': this.autoPosition,
        'position': 'relative',
    };
    return styles;
  }
  
  setPathStyles() {
    let styles = {
        'margin-bottom': '-3px',
        'margin-top': '-20px',
    };
    return styles;
  }
        
          
  setFlagStyles(selectedFlag) {
    if ((selectedFlag == 1 && this.greenPos1) || (selectedFlag == 2 && this.greenPos2) || (selectedFlag == 3 && this.greenPos3)) {
        let styles = {
            'display': 'inline',
            'width': '35px',
            'margin-bottom': '-8px',
            'float': 'right',
            'filter': 'invert(480%) sepia(130%) saturate(7%) hue-rotate(10deg) brightness(74%) contrast(13%)',
            'height': '65px'
        };
        return styles;    
    }
    else {
        let styles = {
            'display': 'inline',
            'width': '35px',
            'margin-bottom': '-8px',
            // 'margin-left': '13%',
            'float': 'right',
            'filter': 'invert(50%) sepia(164%) saturate(3007%) hue-rotate(101deg) brightness(95%) contrast(80%)',
            'height': '65px'
        };
        return styles;
    }
  } 
  
  setGoalStyles() {
    let styles = {
        'display': 'inline',
        'width': '40px',
        'margin-bottom': '-8px',
        'margin-left': '40px',
        'float': 'right',
        'filter': 'invert(19%) sepia(165%) saturate(3270%) hue-rotate(1300deg) brightness(85%) contrast(500%)',
        'height': '65px'
    };
    return styles;
  }
  
  /*
  *Add amount
  */
  addAmount(amount) {
      this.tansationAmount = (this.tansationAmount) ? this.tansationAmount : 0;
      this.tansationAmount = parseInt(this.tansationAmount) + parseInt(amount);
      this.diabledContinueOnAmount = false;      
  }
  /** 
   * Change bank transaction type
   */
  changeTransation(type) {
    this.transationType = type;
    this.diabledContinue = false;
  }

  /**
   * Check amount 
   */
  checkAmount() {
      if(this.transationDetails.TotalInvestmentAmount  == 0){
        if (this.tansationAmount % 100 != 0) {
          if (this.tansationAmount < 100) {
            // this.tansationAmount = this.tansationAmount - (this.tansationAmount % 100);
            this.tansationAmount = 100;
          }
          this.diabledContinueOnAmount = false;      

          if (this.tansationAmount > 100000) {
            this.tansationAmount = 100000;
          }      
        }
      } else if(this.transationDetails.TotalInvestmentAmount  >= 100){
        if (this.tansationAmount < 5) {
            this.tansationAmount = 5;
            this.diabledContinueOnAmount = false;      
        }
        if (this.tansationAmount > 100000) {
          this.tansationAmount = 100000;
        } 
    }
    else {
      this.diabledContinueOnAmount = true;      
    }
  }
  
  /**
   * Open 3rd party url and get responce from 
   */
  openThirdPartyUrl(url) {
    try {
      this.http.get(url).subscribe(res => {
          console.log(res)
      }, error => {
          this.alertMessage('Error', 'Failed to payment')
      })
    } catch (error) {
      this.alertMessage('Error', 'Server issue occured, please try again later')
    }
  }

  /**
   * For showing in save till date amount
   */
  checkCurrentSavings() {
    this.currentSavings = 0;
    this.transationDetails.TransactionDetails.forEach(element => {
        if (element.TransType == 1) {
            this.currentSavings += element.TransAmount;
        } else if (element.TransType == 2) {
            this.currentSavings -= element.TransAmount;
        }
    });
  }

  getTotalInvestment() {
    this.totalInvest = 0;
    this.transationDetails.TransactionDetails.forEach(element => {
        if (element.TransType == 1) {
            this.totalInvest += element.TransAmount;
        } else if (element.TransType == 2) {
            this.totalInvest -= element.TransAmount;
        }
    })
  }
  
  numberOnlyValidation(event: any, amount) {
    let finalValue = parseInt(amount);
    if(finalValue < 1 || amount > 100000){
      event.preventDefault(); 
    }
  
    const pattern = /[0-9,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }

    if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
      event.preventDefault();
    } 
  }

  /**
   * Confirmation alert
   */
  async getBack(){        
    this.events.publish('reloadHome', 'Hello from page1!');             
    this.navCtrl.navigateRoot('homeTab');//savings
  }  
}
