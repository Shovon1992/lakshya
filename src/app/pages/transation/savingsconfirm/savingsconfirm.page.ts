import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-savingsconfirm',
  templateUrl: './savingsconfirm.page.html',
  styleUrls: ['./savingsconfirm.page.scss'],
})

export class SavingsconfirmPage extends BaseComponent implements OnInit {

  mileStones: any = [];
  html: any;
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  goalDetails: any = [];
  getCurrentTransactionList: any =[];
  
  totalNumberSet = 0;
  permonthAmount = 0;
  currentMilestone = 1;
  tansationAmount: any;
  transationType: any;
  statusMsg = '*Message sent to your number for UPI payment. Please check';
  bankName: any;
  submitPurchaseResponce: any = [];
  upiId: any;
  nextStop:number = 0;
  
  currentSavings1 = 0;
  currSavings = 0;
  currentSaving = 0;
  paymentModeResponce: any = [];
  userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'))
  transationDetails: any = [];

  transactionComplete = false;
  paidMessage = "Prossessing";
  disableContinueBtn = false;
  paidCounter = 0
  diabledDecBtn = false;
  diabledContinue = true;
  diabledContinueOnAmount = true;
  goalType = '';
  goalAmount = 0;
  totalInvest = 0;
  pathWidth: any;
  exactPathWidth: any;
  goalstatustel = 0;
  autoPosition = 5+"px";
  goalStatus = false;  
  goalAchieved = false;
  posRation = 0;
  greenPos1:boolean = false;
  greenPos2:boolean = false;
  greenPos3:boolean = false;
                  
  savingAmount = 0;  
  savingsmessage : any;
  lastTransation = JSON.parse(localStorage.getItem('lastTransaction'))
  // slider1 = "./assets/images/confetti.svg";
  

  ngOnInit() {
    this.getGoalStatus();
    console.log('last transation data ----------->' ,this.lastTransation)
    this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    console.log(this.currentUserDetails);
    this.activateRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('savingAmount')) {
        this.router.navigate(['/']);
      }
      this.savingAmount = +paramMap.get('savingAmount');
      this.savingsmessage = paramMap.get('message');
      console.log(this.savingsmessage);
    });         
  }

	ionViewDidEnter() {
    localStorage.setItem('paymentDate', JSON.stringify(new Date()));
    localStorage.setItem('notifyDate', JSON.stringify(new Date()));
    localStorage.setItem('notifyStatus', JSON.stringify(2));
    //this.localNotifications.cancelAll();
  }
  
  getGoalStatus() {
    try {
      //this.present('Loading...',5000);
      console.log(this.userAccountDetails);
      this.transationService.getTransationHistory(this.userAccountDetails.FolioNumber).subscribe(res => {
         this.dismiss();
         if (res['Success']) {
            this.transationDetails = res;
            console.log(this.transationDetails);
            console.log(this.transationDetails);
            if(typeof(this.transationDetails['GoalDetail']['MileStone']) != 'undefined' || this.transationDetails['GoalDetail']['MileStone'] !== null) {
              this.goalStatus = true;
              this.currentSavings1 = parseInt(res['BalanceAmt']);                    
              this.getTotalInvestment();
    
              console.log(">>>>>>TPs>>>>>>>>>>>>>>>>");
              console.log(this.transationDetails);
    
              if(typeof(res['GoalDetail']) != 'undefined' || res['GoalDetail'] !== null) {     
                //  this.pathWidth =  document.getElementById('save-auto-path').offsetWidth - 56;
              
               // this.mileStones = res['GoalDetail']['MileStone'];
                this.goalDetails = res['GoalDetail'];
                this.currentGoalDetails(res['GoalDetail'],res['TransactionDetails']);
              
                this.currentSaving = 0;
                this.calculateCurrentSaving();//currentSaving
                console.log(this.currentSaving);
    
                this.totalNumberSet = Math.ceil((this.mileStones.length - 3) / 3);
                this.permonthAmount = parseInt(res['GoalDetail']['MileStone'][0]['MileStoneAmount']);
                this.currentMilestone = res['GoalDetail']['CurrentMileStone'];
                this.goalAmount = parseInt(res['GoalDetail']['GoalAmount']);
                this.nextStop = this.permonthAmount*(Math.floor(this.currentSavings1/this.permonthAmount) + 1);
                this.posRation = Math.floor(this.pathWidth/4);
                let autoPos = Math.floor((((this.currentSaving * 100)/this.goalAmount) * this.pathWidth)/100);  
                let autoExactPos;
                console.log("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<") 
                console.log(this.currentSaving)    
                console.log(this.goalAmount)   
                this.goalstatustel = parseInt(res['GoalDetail']['GoalStatus'])
                if (this.goalstatustel == 3) {
                  console.log("goalachived state  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                  this.goalAchieved = true;   
                  this.currentUserDetails.IsGoalCreated = false;
                  localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails));                         
                  this.goalStatus = false;     
                }
                
                if (autoPos <= 40) {
                  autoExactPos = Math.floor(5);
                }
                else if(autoPos > this.pathWidth) {
                    autoExactPos = Math.floor(this.pathWidth);
                }
                else {
                  autoExactPos = Math.floor(autoPos);
                }
                            
                console.log(this.posRation)
                console.log(autoExactPos);
        
                this.autoPosition = autoExactPos + 'px';
                this.greenPos1 = (this.posRation > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
                this.greenPos2 = (this.posRation * 2 > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
                this.greenPos3 = (this.posRation * 3 > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
    
                //this.checkCurrentSavings();
              }
            } 
          }
        }, err => {
          // this.dismiss();
        // this.alertMessage('Error', 'Unable fetch transation details.');
      });
    } catch (error) {
        //console.log(error);
        // this.dismiss();
        // this.alertMessage('Error', 'Unable fetch transation details.');
    }
  }

  /**
   * For showing in save till date amount
   */
  checkCurrentSavings() {
    this.currSavings = 0;
    this.transationDetails.TransactionDetails.forEach(element => {
      if (element.TransType == 1) {
          this.currSavings += element.TransAmount;
      } else if (element.TransType == 2) {
          this.currSavings -= element.TransAmount;
      }
      console.log(this.currSavings);
    });
  }
  /**
   * Calculate Current goal
   */
  currentGoalDetails(goalDetails, transationList) {        
    let goalId = goalDetails['GoalId'];
    let goalStatusCode = goalDetails['GoalStatus'];
    this.getCurrentTransactionList = [];
    
    if (goalId && goalStatusCode == 2) {
      for(var i =0; i<transationList.length; i++) {
        if (transationList[i].GoalId == goalId) {
            this.getCurrentTransactionList.push(transationList[i]);
        }
      }
    }
  }

  calculateCurrentSaving() {//currentSaving     
    this.getCurrentTransactionList.forEach(element => {
      if (element.TransType == 1) {
          this.currSavings += parseInt(element.TransAmount);
      } else if (element.TransType == 2) {
          this.currSavings -= parseInt(element.TransAmount);
      }
    });
    console.log(this.currSavings);      
  }


  getTotalInvestment() {
    this.totalInvest = 0;
    this.transationDetails.TransactionDetails.forEach(element => {
      if (element.TransType == 1) {
          this.totalInvest += element.TransAmount;
      } else if (element.TransType == 2) {
          this.totalInvest -= element.TransAmount;
      }
    });
  }
  


  setAutoStyles() {
    let styles = {
      'width': "40px",
      'margin-bottom': "3px",
      'margin-top': '-43px',
      'margin-left': this.autoPosition,
      'position': 'relative',
    };
    return styles;
  }
  
  setPathStyles() {
    let styles = {
      'margin-bottom': '-3px',
      'margin-top': '-20px',
    };
    return styles;
  }
        
          
  setFlagStyles(selectedFlag) {
    if ((selectedFlag == 1 && this.greenPos1) || (selectedFlag == 2 && this.greenPos2) || (selectedFlag == 3 && this.greenPos3)) {
      let styles = {
        'display': 'inline',
        'width': '35px',
        'margin-bottom': '-8px',
        'float': 'right',
        'filter': 'invert(480%) sepia(130%) saturate(7%) hue-rotate(10deg) brightness(74%) contrast(13%)',
        'height': '65px'
      };
      return styles;    
    }
    else {
      let styles = {
        'display': 'inline',
        'width': '35px',
        'margin-bottom': '-8px',
        // 'margin-left': '13%',
        'float': 'right',
        'filter': 'invert(50%) sepia(164%) saturate(3007%) hue-rotate(101deg) brightness(95%) contrast(80%)',
        'height': '65px'
      };
      return styles;
    }
  } 
  
  
  setGoalStyles() {
    let styles = {
      'display': 'inline',
      'width': '40px',
      'margin-bottom': '-8px',
      'margin-left': '40px',
      'float': 'right',
      'filter': 'invert(19%) sepia(165%) saturate(3270%) hue-rotate(1300deg) brightness(85%) contrast(500%)',
      'height': '65px'
    };
    return styles;
  }  

  numberOnlyValidation(event: any, amount) {
    let finalValue = parseInt(amount);
    if(finalValue < 1 || amount > 100000){
      event.preventDefault(); 
    }
  
    const pattern = /[0-9,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }

    if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
      event.preventDefault();
    } 
  }

  /**
   * Confirmation alert
   */
  async getBack(){                   
    this.events.publish('reloadHome', 'Hello from page1!');       
    this.navCtrl.navigateRoot('homeTab');
  }
}
