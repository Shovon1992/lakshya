import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';


import { IonicModule } from '@ionic/angular';

import { SavingsconfirmPage } from './savingsconfirm.page';
import { CustomComponentModule } from './../../../customComponents/custom-component/custom-component.module';

const routes: Routes = [
  {
    path: '',
    component: SavingsconfirmPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,TranslateModule,
    RouterModule.forChild(routes),
    CustomComponentModule,
  ],
  declarations: [SavingsconfirmPage]
})
export class SavingsconfirmPageModule {}
