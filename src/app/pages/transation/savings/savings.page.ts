import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { IsubmitPurchase } from 'src/app/interface/transation/isubmit-purchase';
import { timeout } from 'rxjs/operators';
import {IdeleteUpi} from 'src/app/interface/transation/idelete-upi';
@Component({
  selector: 'app-savings',
  templateUrl: './savings.page.html',
  styleUrls: ['./savings.page.scss'],
})
export class SavingsPage extends BaseComponent implements OnInit {
  mileStones: any = [];
  html: any;
  goalDetails: any = [];
  getCurrentTransactionList: any =[];
  currentSaving = 0;
  newUpi: any;
  totalNumberSet = 0;
  permonthAmount = 0;
  currentMilestone = 1;
  tansationAmount: any;
  transationType: any;
  statusMsg = '*Message sent to your number for UPI payment. Please check';
  bankName: any;
  submitPurchaseResponce: any = [];
  upiId: any;
  upiId1: any;
  nextStop:number = 0;
  
  currentSavings1 = 0;
  currentSavings:any = 0;
  paymentModeResponce: any = [];
  userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'))
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  transationDetails: any = [];

  transactionComplete = false;
  paidMessage = "Prossessing";
  disableContinueBtn = false;
  paidCounter = 0
  diabledDecBtn = false;
  diabledContinue = true;
  diabledContinueOnAmount = true;

  goalAmount = 0;
  totalInvest = 0;
  pathWidth: any;
  exactPathWidth: any;
  autoPosition = 5+"px";
    
  posRation = 0;
  greenPos1:boolean = false;
  greenPos2:boolean = false;
  greenPos3:boolean = false;
                  
  loadder: any;     

  ngOnInit() {
    this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'));
  }

	ionViewDidEnter() {
    this.getGoalDetails();
    this.getBankAccountDetailsOfUser();
    if(!this.currentUserDetails.IsKYCCompleted){
      this.navCtrl.navigateForward('masterpage')
    }
	}
  /**
   * Helper function to get status of button enable
   * 
   */
  getBtnStatus() {
    if (this.tansationAmount && (this.transationType == 'UPI') && this.upiId && (this.tansationAmount % 1 ==0 && this.tansationAmount > 0)) {
      return false;
    }
    else if(this.tansationAmount && (this.transationType == 'DC' && !this.diabledDecBtn) && (this.tansationAmount % 1 ==0 && this.tansationAmount > 0)) {
      return false;
    }
    else {
      return true;
    }
  }


  getGoalDetails() {
      try {
          //this.present('Loading...',5000);
          this.transationService.getTransationHistory(this.userAccountDetails.FolioNumber).subscribe(res => {
            this.dismiss();
            if (res['Success']) {
              this.transationDetails = res;
              this.currentSavings1 = parseInt(res['BalanceAmt']);                    
              this.getTotalInvestment();
              this.mileStones = res['GoalDetail']['MileStone'];
              this.goalDetails = res['GoalDetail'];
              this.currentGoalDetails(res['GoalDetail'],res['TransactionDetails']);
             
              this.currentSaving = 0;
              this.calculateCurrentSaving();//currentSaving
             
              this.totalNumberSet = Math.ceil((this.mileStones.length - 3) / 3);
              this.permonthAmount = parseInt(res['GoalDetail']['MileStone'][0]['MileStoneAmount']);
              this.currentMilestone = res['GoalDetail']['CurrentMileStone'];
              this.goalAmount = parseInt(res['GoalDetail']['GoalAmount']);
              this.nextStop = this.permonthAmount*(Math.floor(this.currentSavings1/this.permonthAmount) + 1);
              
              // if(this.currentUserDetails.IsGoalCreated){             
                this.pathWidth = document.getElementById('save-auto-path').offsetWidth - 56;
                this.posRation = Math.floor(this.pathWidth/4);
                let autoPos = Math.floor((((this.currentSaving * 100)/this.goalAmount) * this.pathWidth)/100);  
                let autoExactPos;
        
                if (autoPos <= 40) {
                  autoExactPos = Math.floor(5);
                }
                else if(autoPos > this.pathWidth) {
                    autoExactPos = Math.floor(this.pathWidth);
                }
                else {
                  autoExactPos = Math.floor(autoPos);
                }
                
                this.autoPosition = autoExactPos + 'px';
                this.greenPos1 = (this.posRation > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
                this.greenPos2 = (this.posRation * 2 > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
                this.greenPos3 = (this.posRation * 3 > autoPos) ? true : false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;

                this.currentSavings = 0;
                this.checkCurrentSavings();
              // }
            } else {
              this.alertMessage('Error', res['ExtendedMessage']);
            }
          }, err => {
            this.dismiss();
            this.alertMessage('Error', 'Unable fetch transation details.');
            this.navCtrl.navigateForward('masterpage')
          })
      } catch (error) {
          //console.log(error);
          this.alertMessage('Error', 'Unable fetch transation details.');
          this.navCtrl.navigateForward('masterpage')
      }
  }

    /**
     * Calculate Current goal
     */
    currentGoalDetails(goalDetails, transationList) {        
      let goalId = goalDetails['GoalId'];
      let goalStatus = goalDetails['GoalStatus'];
      this.getCurrentTransactionList = [];
      
      if (goalId && goalStatus == 2) {
          for(var i =0; i<transationList.length; i++) {
              if (transationList[i].GoalId == goalId) {
                  this.getCurrentTransactionList.push(transationList[i]);
              }
          }
      }
  }
  calculateCurrentSaving() {//currentSaving
     
      this.getCurrentTransactionList.forEach(element => {
          console.log(element.TransAmount);
          if (element.TransType == 1) {
              this.currentSaving += parseInt(element.TransAmount);
          } else if (element.TransType == 2) {
              this.currentSaving -= parseInt(element.TransAmount);
          }
      });

      console.log(this.currentSaving);
  }



  setAutoStyles() {
    let styles = {
        'width': "40px",
        'margin-bottom': "3px",
        'margin-top': '-43px',
        'margin-left': this.autoPosition,
        'position': 'relative',
    };
    return styles;
  }
  
  setPathStyles() {
    let styles = {
        'margin-bottom': '-3px',
        'margin-top': '-20px',
    };
    return styles;
  }
        
          
  setFlagStyles(selectedFlag) {
    if ((selectedFlag == 1 && this.greenPos1) || (selectedFlag == 2 && this.greenPos2) || (selectedFlag == 3 && this.greenPos3)) {
        let styles = {
            'display': 'inline',
            'width': '35px',
            'margin-bottom': '-8px',
            'float': 'right',
            'filter': 'invert(480%) sepia(130%) saturate(7%) hue-rotate(10deg) brightness(74%) contrast(13%)',
            'height': '65px'
        };
        return styles;    
    }
    else {
        let styles = {
            'display': 'inline',
            'width': '35px',
            'margin-bottom': '-8px',
            // 'margin-left': '13%',
            'float': 'right',
            'filter': 'invert(50%) sepia(164%) saturate(3007%) hue-rotate(101deg) brightness(95%) contrast(80%)',
            'height': '65px'
        };
        return styles;
    }
  } 
  
  
  setGoalStyles() {
    let styles = {
        'display': 'inline',
        'width': '40px',
        'margin-bottom': '-8px',
        'margin-left': '40px',
        'float': 'right',
        'filter': 'invert(19%) sepia(165%) saturate(3270%) hue-rotate(1300deg) brightness(85%) contrast(500%)',
        'height': '65px'
    };
    return styles;
  }
  
  /*
  *Add amount
  */
  addAmount(amount) {
      this.tansationAmount = (this.tansationAmount) ? this.tansationAmount : 0;
      this.tansationAmount = parseInt(this.tansationAmount) + parseInt(amount);
      this.diabledContinueOnAmount = false;      
  }
  /** 
   * Change bank transaction type
   */
  changeTransation(type,upi) {
    this.transationType = type;
    this.diabledContinue = false;
    this.upiId = upi
  }

  /**
   * Submit purchase
   */
  async submitPurchase() {

    if( this.currentUserDetails.IsKYCCompleted && this.tansationAmount){
      try {
        
        let data: IsubmitPurchase = {
            FolioNo: this.userAccountDetails.FolioNumber,
            Amount: this.tansationAmount,
            PaymentMode: this.transationType,
            /* PaymentMode: ‘DC’ for Debit Card, ‘UPI’ for UPI */
            TranId: (this.userAccountDetails.TranId) ? this.userAccountDetails.TranId : "",
            PAN: this.currentUserDetails.POIInformation.PAN, //"AOAPD7735F",
            TransType: 1,
            /**TransType: 1: 'Invest', 2: 'Withdraw' */
            GoalId: (this.goalDetails['GoalId']) ? this.goalDetails['GoalId'] : 0,
            GoalStatus: 1,
            VirtualPayemtAddress: (this.upiId) ? this.upiId : this.newUpi,
            BankAccountNo: this.userAccountDetails.AccountNumber,
            IpAddress: '10.10.10.10',
            ChkDigit: ""
        }
        if(/^\w.+@\w+$/.test(data.VirtualPayemtAddress)){
          this.present('Message sent to your number for UPI payment. Please check.',30000);
          this.transationService.getSubmitPurchase(data).subscribe(res => {
            //this.dismiss();
              if (res['Success']) {
                let transdata = {
                  transId : res['TransId'],
                  status : 'progress'
                }
                localStorage.setItem('lastTransaction', JSON.stringify(transdata));
                //this.present('Submitting your request...',30000);
                this.submitPurchaseResponce = res;
                if (data.PaymentMode == 'DC') { //IF user selects for debit card payment option
                    this.paymentModerequest();
                } else if (data.PaymentMode == 'UPI') {
                  this.disableContinueBtn = true;
                    let  __this =this //IF user selects for UPI payment option
                    let intervalID = setInterval(function() {
                      __this.paidCounter++
                        console.log('Test',this.disableContinueBtn);
                        if(__this.disableContinueBtn && __this.paidCounter < 19){
                          __this.getTransationPaymetStatus1();
                          console.log('calinnnnnnnnnhggggggggggg');
                        } else{
                          this.dismiss();
                          this.router.navigate(['/savingsconfirm/' + this.tansationAmount+'/failed']);
                        }                               
                      //   else if (__this.paidCounter >= 6) {
                      //     this.statusMsg = 'Transaction Failed.';
                      //     console.log("Stoppppppppppppp");
                      //     clearInterval(intervalID);
                      //     this.disableContinueBtn = false; 
                      //     this.toast.show('Transaction Failed.', '3000', 'center').subscribe(
                      //       toast => {
                      //         console.log(toast);
                      //       }
                      //     );
                      //     this.dismiss();
                      // }     
                  }, 1000 * 10);
                }
    
                
               // this.router.navigateByUrl('savings');
              } else {
                let serverMessage = res['ExtendedMessage'] ? res['ExtendedMessage'] : res['ErrorMessage'];
                if(serverMessage.toLowerCase().indexOf("incorrect vpa") > 0 ||serverMessage.toLocaleUpperCase().indexOf("FAILURE") > 0 || serverMessage.toLocaleLowerCase().indexOf("unable to process") > 0 || serverMessage.toLocaleLowerCase().indexOf("bank does not offer") > 0){
                  this.router.navigate(['/savingFail/' + serverMessage ]);
                } else{
                    this.alertMessage('Error', serverMessage)
                }
              }
              // console.log(res, this.mileStones)
            }, err => {
              this.dismiss();
              this.alertMessage('Error', 'Server issue occured, please try again later')
            })
        }else{
          this.alertMessage('Alert !', 'Please enter valid UPI id')
        }    
        
      } catch (error) {
        this.dismiss();
        console.log(error)
        this.alertMessage('Error', 'Server issue occured, please try again later')
      }
    }else{
      if(!this.currentUserDetails.IsKYCCompleted){
       this.alertMessage('Alert !', 'Your KYC is not complete. Do not worry KYC process is very simple and free')
      }
      if(!this.tansationAmount){
        this.alertMessage('Alert !', 'Please enter amount')
       }
  }
    
  }
  /**
   * Check amount 
   */
  checkAmount() {
      if(this.transationDetails.TotalInvestmentAmount  == 0){
        if (this.tansationAmount % 100 != 0) {
          if (this.tansationAmount < 100) {
            // this.tansationAmount = this.tansationAmount - (this.tansationAmount % 100);
            this.tansationAmount = 100;
          }
          this.diabledContinueOnAmount = false;      

          if (this.tansationAmount > 100000) {
            this.tansationAmount = 100000;
          }      
        }
      } else if(this.transationDetails.TotalInvestmentAmount  >= 100){
        if (this.tansationAmount < 5) {
            this.tansationAmount = 5;
            this.diabledContinueOnAmount = false;      
        }
        if (this.tansationAmount > 100000) {
          this.tansationAmount = 100000;
        } 
    }
    else {
      this.diabledContinueOnAmount = true;      
    }
  }

  
  /**
   * api call for payment mode request 
   */
  paymentModerequest() {
      try {
        this.present('Submitting your request...', 5000);
        this.transationService.paymentModeRequest(this.submitPurchaseResponce.TransId).subscribe(res => {
          if (res['Success']) {
            this.dismiss();
            this.html = res['Url'];
            this.createWebview(res['Url']);
          } else {
            this.alertMessage('Error', res['ExtendedMessage']);
            this.dismiss();
            this.disableContinueBtn = false           
          }
        }, err => {
          this.dismiss();               
          this.alertMessage('Error', 'Server issue occured, please try again later')
        })
      } catch (error) {
        this.dismiss();
        this.alertMessage('Error', 'Server issue occured, please try again later')
      }
  }
  /**
   * Open 3rd party url and get responce from 
   */
  openThirdPartyUrl(url) {
    try {
      this.http.get(url).subscribe(res => {
          console.log(res)
      }, error => {
          this.alertMessage('Error', 'Failed to payment')
      })
    } catch (error) {
      this.alertMessage('Error', 'Server issue occured, please try again later')
    }
  }
  /**
   * Get transation payment status 
   */
  getTransationPaymetStatus1() {
    try {
      // this.present('Submitting your request...', 30000);
      this.transationService.getPaidStattus(this.submitPurchaseResponce.TransId, this.currentUserDetails
        .POIInformation.PAN).subscribe(res => {
        
          if (res['Success']) { 
            this.dismiss();            
            this.transactionComplete = true;
            this.toast.show( 'Transaction Completed Successfully.', '3000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
            // this.router.navigateByUrl('savingsconfirm');//savings
            // this.navCtrl.push('savingsconfirm', {firstName: name});
            this.dismiss();
            
            this.setDataInJson(this.submitPurchaseResponce.TransId)
            // this.ionViewDidEnter();
            this.disableContinueBtn = false;                
            this.router.navigate(['/savingsconfirm/' + this.tansationAmount+'/success']);
          } else {
            if(res['ExtendedMessage'] != 'Incomplete' || this.paidCounter > 17){
              this.dismiss();
              this.router.navigate(['/savingsconfirm/' + this.tansationAmount+'/failed']);
              this.setDataInJson(this.submitPurchaseResponce.TransId)
            }
            
            // if(res['ExtendedMessage'] != 'Incomplete'){
            //   this.dismiss();            
            //   this.alertMessage('Error', res['ExtendedMessage']);
            // }else{
            //   this.present('Submitting your request...',30000);                              
            //   this.paidMessage ="Purchase in progress";
            // }                 
          }
      }, err => {
        this.dismiss();                   
        this.alertMessage('Error', 'Server issue occured, please try again later')
      })
    } catch (error) {
      this.dismiss();     
      this.alertMessage('Error', 'Server issue occured, please try again later')
    }
  }

  /**
   * Create webvew and wait for it's responce
   */
  createWebview(htmlData) {
    let url = 'data:text/html;base64,' + btoa(htmlData)
    var target = "_blank";
    var options = "hidden=no,location=yes,clearsessioncache=yes,clearcache=yes;hardwareback =yes;";
    let newBrowser = this.inAppBrowser.create(htmlData, target, options);
    newBrowser.on('exit').subscribe(res => {
        this.getTransationPaymetStatus1();
        console.log(res)
    }, err => {
        console.log(err)
    })
  }         

  /**
   * For showing in save till date amount
   */
  checkCurrentSavings() {
    this.currentSavings = 0;
    this.transationDetails.TransactionDetails.forEach(element => {
        if (element.TransType == 1) {
            this.currentSavings += element.TransAmount;
        } else if (element.TransType == 2) {
            this.currentSavings -= element.TransAmount;
        }
    });
  }

  getTotalInvestment() {
    this.totalInvest = 0;
    this.transationDetails.TransactionDetails.forEach(element => {
        if (element.TransType == 1) {
            this.totalInvest += element.TransAmount;
        } else if (element.TransType == 2) {
            this.totalInvest -= element.TransAmount;
        }
    })
  }
  
  numberOnlyValidation(event: any, amount) {
    let finalValue = parseInt(amount);
    if(finalValue < 1 || amount > 100000){
      event.preventDefault(); 
    }
  
    const pattern = /[0-9,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }

    if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
      event.preventDefault();
    } 

  }


  /**
   * Store data in local json file 
   */
  async setDataInJson(data){
    /**To store data in json file  */
    let today = new Date();
    let dd = (today.getDate() < 10) ? '0'+today.getDate() : today.getDate();
    let mm = (today.getMonth()+1 < 10) ? '0'+ today.getMonth()+1:  today.getMonth()+1; 
    let yyyy = today.getFullYear();
            let isInvested = false;
  let inputFormat = mm+'/'+(today.getDate())+'/'+yyyy;
  let investData = new Array();
  try {
    await this.file.readAsText(this.file.dataDirectory,'transation.json').then(
      res =>{
        if(res){
          investData = JSON.parse(res);
        }
        
        }, err =>{
          console.log(err)
        }
      ).catch(
      error =>{
        console.log(error)
      }
    );
  } catch (error) {
    console.log(error)
  }
    investData.push({
      user : this.currentUserDetails.PhoneNumber,
      date : today,
      data : data
    });
    console.log(investData)
    let __this = this
    this.file.writeFile(this.file.dataDirectory, 'transation.json', JSON.stringify(investData), {replace: true}).then(
      res =>{
          if(this.currentUserDetails['Status'] < 7){
            this.sendNotification(1,"Please complete your KYC to complete your profile ")
          } else if(this.currentUserDetails['Status'] == 7){
            this.sendNotification(2,"Dear Investor, your account creation is incomplete. \nComplete your account creation process to enjoy saving with Lakshya")
          }else if(!this.currentUserDetails.IsGoalCreated){
            this.sendNotification(3,"Dear Investor, You have not set your saving goal. \nSet a goal to make saving fun")
          }
      }
  )
  }
  
  /**
   * Confirmation alert
   */
  async getBack(){                    
    this.navCtrl.navigateRoot('homeTab');
  }

  /**
   * Delete upi id 
   */
  deleteUpi(upi){
    try {
      let data: IdeleteUpi ={
        UserId : this.currentUserDetails.UserId,
        UpiId: upi
      } 
      this.present('Deleteing UPI id ...', 30000);
      this.transationService.deleteUPI(data).subscribe(res => {
        
          if (res['Success']) { 
            this.dismiss();            
            //this.getBankAccountDetailsOfUser();
            this.toast.show( 'UPI deleted.', '3000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
            this.ionViewDidEnter()  
            this.ngOnInit();            
          } else {        
              this.alertMessage('Error', res['ExtendedMessage']);
          }                 
          
      }, err => {
        this.dismiss();                   
        this.alertMessage('Error', 'Server issue occured, please try again later')
      })
    } catch (error) {
      this.dismiss();     
      this.alertMessage('Error', 'Server issue occured, please try again later')
    }
  }


  async deleteUpiAlertConfirm(upiId) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Do you want to delete UPI id '+upiId+'?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.deleteUpi(upiId)
          }
        }
      ]
    });

    await alert.present();
  }

  async present(message,duration) {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      //duration: 15000,
      duration: 200100,
      message: message,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        console.log(message);
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }
  async dismiss() {
      this.isLoading = false;
      await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
  }


  sendNotification(id,message){
    this.localNotifications.clearAll();
    let today = new Date();
    this.localNotifications.schedule({
      id: id,
      led: { color: '#FF00FF', on: 500, off: 500 },
      title: 'Lakshya Notification',
      text: message,
      icon: 'http://climberindonesia.com/assets/icon/ionicons-2.0.1/png/512/android-chat.png',
      sound:  '../assets/swiftly.mp3',
      lockscreen: true,
     //trigger: { at: new Date(new Date().getTime() + 5000) },
      trigger: { every: {hour : 12},count : 2},
      //trigger: { in : 2 , every: ELocalNotificationTriggerUnit.MINUTE},
  }); 
  }
  /**
   * End of the class
   */
}