import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { ICompleteWithdraw } from 'src/app/interface/transation/icomplete-withdraw';

@Component({
  selector: 'app-withdraw-otp',
  templateUrl: './withdraw-otp.page.html',
  styleUrls: ['./withdraw-otp.page.scss'],
})
export class WithdrawOtpPage extends BaseComponent implements OnInit {
  Trxn_id : any = this.activateRoute.snapshot.paramMap.get('transationID');
  amount = this.activateRoute.snapshot.paramMap.get('amount');
  folioOtpDetails = JSON.parse(localStorage.getItem('folioOtpDetails'));
  OTP1: any;
  OTP2: any;
  OTP3: any;
  OTP4: any;
  OTP5: any;
  OTP: any = [];
  res_otpid: any;
  userDetails = JSON.parse(localStorage.getItem('userDetails'));
  withdrawReturnData : any;
  withdrawOtpData : ICompleteWithdraw = {
    UserId : this.userDetails.UserId,
    Mode : 'S',
    Trxn_id: this.Trxn_id,
    Success : true,
    Status: true
  }
  ngOnInit(
  ) {
    this.verifyOTP('S', null)
  }
  verifyOTP(mode, OTP_ID){
    const loadingMessage = (mode == 'S' || mode == 'RS')? 'Sending OTP..' : 'Validating OTP';
    if ( mode == 'S' || mode == 'RS' || (mode == 'V' && this.OTP1 && this.OTP2 && this.OTP3 && this.OTP4 && this.OTP5) ) {
      try {
        this.showLodder(loadingMessage);
        
        this.withdrawOtpData ={
          OTP_ID : OTP_ID,
          UserId : this.userDetails.UserId,
          Mode : mode,
          Trxn_id: this.Trxn_id,
          Success : true,
          Status: true,
          OTP_Text : (mode == 'S' || mode == 'RS')? null : this.OTP1+''+this.OTP2+''+this.OTP3+''+this.OTP4+''+this.OTP5,
        }
        console.log(this.withdrawOtpData);
  
        this.bankService.verifyWithdrawOtp(this.withdrawOtpData).subscribe(
          res =>{
            this.hideLoader();
            this.withdrawReturnData = res;
            if(res['Success']){
              if(mode == 'V'){
                this.router.navigate(['/withdrawalconfirm/' + this.amount +'/1/'+res['ExtendedMessage']]);
              }
            } else {
              this.alertMessage('Error !!', (res['ErrorMessage']) ?res['ErrorMessage'] : res['ExtendedMessage'] );
            }
            
            console.log(res);
          //   /*
          //   *On success populate data to form
          //   */
          // console.log("Suryabahan  Res");
          // console.log(res);
          // this.res_otpid = res['OTPID'];
          //   if(res['Success']){
          //     //this.alertMessage('Success','OTP virfied.');
          //     this.createFolio();
          //   }else{
          //     this.alertMessage('Error','Unable to validate otp.');
          //   }
          }, err =>{
            this.hideLoader();
            this.alertMessage('Error','Unable to validate otp.');
          }
        )
      } catch (error) {
        this.alertMessage('Error','Unable to validate otp.');
      }
    }
    else {
      console.log("false Suryabhan....");
      return;
    }
  }
  
  
  
  onKeydownEvent(event: KeyboardEvent,prev,keyPos): void {
    if (event.keyCode == 46 || event.keyCode == 8) {
      if (keyPos == 5) {
          this.OTP5 = "";
      }
      else if (keyPos == 4) {
        this.OTP5 = "";
        this.OTP4 = "";
      }
      else if (keyPos == 3) {
        this.OTP5 = "";
        this.OTP4 = "";
        this.OTP3 = "";
      }
      else if (keyPos == 2) {
        this.OTP5 = "";
        this.OTP4 = "";
        this.OTP3 = "";
        this.OTP2 = "";
      }
      else if (keyPos == 1) {
        this.OTP5 = "";
        this.OTP4 = "";
        this.OTP3 = "";
        this.OTP2 = "";
        this.OTP1 = "";
      }
      
      console.log(this.OTP);
      prev.setFocus();
    }
  }
  
  
  
  
  goToNextInput1(event: any,prev,current, next,keyPos, charCode) {
    const pattern = /[0-9,]/;
    let inputChar = String.fromCharCode(event.charCode);
  
    if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
        this.OTP[keyPos] = "";
        return false;
    } else {
        if (charCode.length == 0) {
            if (current.value.length == 0) {
                next.setFocus()
                return true;
            } else {
                return false;
            }
        }
    }
  }
  
  
  checkLength(ele, charCode) {
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
        if (ele.value.length != 0) {
            return false
        }
    }
  }
  
  
    /**
     * End of class
     */

  /**
   * End of the class
   */
}
