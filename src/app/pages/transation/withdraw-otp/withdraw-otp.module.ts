import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CustomComponentModule } from './../../../customComponents/custom-component/custom-component.module';
import { IonicModule } from '@ionic/angular';

import { WithdrawOtpPage } from './withdraw-otp.page';

const routes: Routes = [
  {
    path: '',
    component: WithdrawOtpPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CustomComponentModule
  ],
  declarations: [WithdrawOtpPage]
})
export class WithdrawOtpPageModule {}
