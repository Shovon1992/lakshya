import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-saving-fail',
  templateUrl: './saving-fail.page.html',
  styleUrls: ['./saving-fail.page.scss'],
})


export class SavingFailPage extends BaseComponent implements OnInit {

  serverMessage = 'You seem to have entered an incorrect VPA. Please check your VPA and try again';
  messages = ['You have entered incorrect UPI ID. Enter correct UPI ID','Server issue occured. Please try after some time']
  showMessage: any = "Something went wrong please try again";
  showServerError = false;
  async getBack(){
    // this.events.publish('reloadHome', 'Hello from page1!');     
    // this.router.navigateByUrl('homeTab');
    this.navCtrl.back();
    // this.navCtrl.navigateRoot('homeTab');
  }
  ngOnInit() {

    this.activateRoute.paramMap.subscribe(paramMap => {
      this.serverMessage = paramMap.get('message');
      if(this.serverMessage.toLowerCase().indexOf("incorrect vpa") > 0){
        this.showMessage =this.messages[0];
        this.showServerError = false;
      } else if(this.serverMessage.toLocaleUpperCase().indexOf("FAILURE") > 0 || this.serverMessage.toLocaleLowerCase().indexOf("unable to process") > 0 || this.serverMessage.toLocaleLowerCase().indexOf("bank does not offer") > 0){
        this.showMessage =this.messages[1];
        this.showServerError = true
      }
    });   
    

    
    
  }

}
  