import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from  "@angular/router";
import { NavController } from '@ionic/angular';
import { BaseComponent } from '../../../base/base.component';

@Component({
  selector: 'app-goalvideo',
  templateUrl: './goalvideo.page.html',
  styleUrls: ['./goalvideo.page.scss'],
})

export class GoalvideoPage extends BaseComponent implements OnInit {

  showvideo:Boolean = true;
  language = 'en';
  vidoLink = 'https://www.youtube.com/embed/KDcqhf46Vbw';
  //constructor(private  router:  Router, private translate: TranslateService, public navCtrl : NavController) { }
  
  ngOnInit() {
  }

  ionViewDidEnter() {
    this.checkVideo(this.language);
  } 
  checkVideo(lang){
    this.language = lang
    switch(lang){
      case 'en':
        this.vidoLink = 'https://www.youtube.com/embed/KDcqhf46Vbw';
        break;
      case 'ka':
        this.vidoLink = 'https://www.youtube.com/embed/R2ygVhWLx5E';
        break;
      case 'hn':
        this.vidoLink = 'https://www.youtube.com/embed/ELyDfTj7O4E';
        break;
      default:
          this.vidoLink = 'https://www.youtube.com/embed/KDcqhf46Vbw';
          break;
    }
  }
  skipvideo(){
  this.showvideo = false;
  }
  register(){
    this.router.navigateByUrl('login'); 
  }
  setGoal(){
    this.navCtrl.navigateRoot('goalsetting');
  }

  /**
   * Confirmation alert
   */
  async getBack(){                 
    this.navCtrl.navigateRoot('homeTab');
  }
  
}
