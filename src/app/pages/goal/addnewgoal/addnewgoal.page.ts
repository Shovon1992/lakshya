import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
// import { exists } from 'fs';


@Component({
  selector: 'app-addnewgoal',
  templateUrl: './addnewgoal.page.html',
  styleUrls: ['./addnewgoal.page.scss'],
})
export class AddnewgoalPage implements OnInit {
  constructor(public modalCrtl: ModalController, private navParams: NavParams) { }

  @Input() 
  public goalType: number;
  public goalname: string;
  public new_goal: string;

  ngOnInit() {
    console.table(this.navParams);
    this.goalType = this.navParams.data.goalType;
  // this.goal = {
  //   // type: this.goalType,
  //   name: 'chocolate cake'
  // };
    console.log(this.goalType);
  }
 

  addGoal() {
  
    let goal = {
      "type": this.new_goal,
    };
    
    this.modalCrtl.dismiss(goal);
  }

  async closeModal() {
    const onClosedData: string = null;
    await this.modalCrtl.dismiss(onClosedData);
  }
}
