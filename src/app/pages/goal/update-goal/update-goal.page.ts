import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { IupdateGoal } from 'src/app/interface/goal/iupdate-goal';

@Component({
  selector: 'app-update-goal',
  templateUrl: './update-goal.page.html',
  styleUrls: ['./update-goal.page.scss'],
})
export class UpdateGoalPage extends BaseComponent implements OnInit {
  minamount : number = 1000;  mintime: number = 1;
  maxamount: number = 100000; maxtime: number = 12;
  sliderrange: number = 1000; slidemonths: number = 1;
  goalType = 0;
  goalId: any;
  ngOnInit() {
    this.getGoalDetails();
  }

/**
 * For Setting new goal 
 * Shovon
 */
  updateGoal(){
    let goalData: IupdateGoal = {
      GoalId: this.goalId,
      Phonenumber: localStorage.getItem('phoneno'),
      GoalAmount: this.sliderrange,
      GoalTime: this.slidemonths,
      GoalType: this.goalType
    }

    try {
      this.showLodder('Updating your goal..');
      this.goalService.updateGoal(goalData).subscribe(
        res => {
          this.hideLoader();
          if(res['Success']){
            this.alertMessage('Error', res['ExtendedMessage']);
            this.navCtrl.navigateForward('homeTab');
          }else{
            this.alertMessage('Alert', res['ErrorMessage']);
          }
          //console.log(res)
        }, err =>{
          this.hideLoader();
          this.alertMessage('Error', 'Server issue occured, please try again later')
        }
      )
    } catch (error) {
      this.hideLoader();
      this.alertMessage('Error', 'Server issue occured, please try again later')
    }
    console.log(this.goalType, this.sliderrange, this.slidemonths)
  }

  getGoalDetails(){
    try {
     //this.showLodder('Getting your goal..');
      this.goalService.getGoalLists(this.currentUserDetails.PhoneNumber).subscribe(
        res => {
          this.hideLoader();
          if(res['Success']){
           // this.alertMessage('Error', res['ExtendedMessage']);
            this.goalId = res['GoalDetail'][0]['GoalId'];
            this.sliderrange = res['GoalDetail'][0]['GoalAmount']
            this.slidemonths = res['GoalDetail'][0]['GoalTime']
            this.goalType = res['GoalDetail'][0]['GoalType']
           // this.navCtrl.navigateForward('homeTab');
          }
          //console.log(res)
        }, err =>{
          this.hideLoader();
          this.alertMessage('Error', 'Server issue occured, please try again later')
        }
      )
    } catch (error) {
      this.hideLoader();
      this.alertMessage('Error', 'Server issue occured, please try again later')
    }
    console.log(this.goalType, this.sliderrange, this.slidemonths)
  }


  decreaseAmount(){
    if(this.sliderrange > this.minamount && this.sliderrange <=this.maxamount ){
     this.sliderrange -= 10000;
    }
  }
  increaseAmount(){
    if(this.sliderrange>= this.minamount && this.sliderrange <this.maxamount ){
      this.sliderrange += 10000;
    }
  }



  decreaseTime(){
    if(this.slidemonths > this.mintime && this.slidemonths <= this.maxtime ){
      this.slidemonths -= 1;
     }

  }
  increaseTime(){
    if(this.slidemonths >= this.mintime && this.slidemonths <this.maxtime ){
      this.slidemonths += 1;
    }
  }


  /**
   * End of class
   */
}

 