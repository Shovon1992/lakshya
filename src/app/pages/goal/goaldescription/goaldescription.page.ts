import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../base/base.component';
import { GoalService } from 'src/app/services/goalServices/goal.service';
@Component({
  selector: 'app-goaldescription',
  templateUrl: './goaldescription.page.html',
  styleUrls: ['./goaldescription.page.scss'],
})
export class GoaldescriptionPage  implements OnInit {

  constructor(public goalService: GoalService,) { }
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  mileStones: any = [];
  goalDetails: any = [];
  totalNumberSet = 0;
  permonthAmount = 0;

  ngOnInit() {
    this.getGoalDetails();
    console.log(this.currentUserDetails.PhoneNumber)
  }

  /** 
   * For getting new goal 
   * Shovon
   */
  getGoalDetails(){
   // console.log(this.currentUserDetails.PhoneNumber)
        try {
      //this.showLodder('Getting your goal..');
          this.goalService.getGoalLists(this.currentUserDetails.PhoneNumber).subscribe(
            
            res => {
              //this.hideLoader();
              //if(res['Success']){
                this.mileStones = res['GoalDetail'][0]['MileStone'];
                this.goalDetails = res['GoalDetail'][0];
                this.totalNumberSet = Math.ceil((this.mileStones.length-3)/3);
                this.permonthAmount = res['GoalDetail'][0]['MileStone'][0]['MileStoneAmount']
                //this.alertMessage('Error', res['ExtendedMessage']);
                //this.navCtrl.navigateForward('homeTab');
              //}
              console.log(res,this.mileStones)
            }, err =>{
          //this.hideLoader();    
          console.log(err)
     
          //this.alertMessage('Error', 'Server issue occured, please try again later')
        }
      )
    } catch (error) {
      //this.hideLoader();
      console.log(error)
      //this.alertMessage('Error', 'Server issue occured, please try again later')
    }
    
  }
  setyourgoal(){
    
  }
  /**
   * End of class
   */
}
