import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { GoaldescriptionPage } from './goaldescription.page';

const routes: Routes = [
  {
    path: '',
    component: GoaldescriptionPage
  }
];

@NgModule({ 
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GoaldescriptionPage]
})
export class GoaldescriptionPageModule {}
