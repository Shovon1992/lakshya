import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { GoalsettingPage } from './goalsetting.page';
import { TranslateModule } from '@ngx-translate/core';
// import { AddnewgoalPage } from '../addnewgoal/addnewgoal.page';


const routes: Routes = [
  {
    path: '',
    component: GoalsettingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,TranslateModule,
    RouterModule.forChild(routes),
  ],
  declarations: [GoalsettingPage],
  // entryComponents: [AddnewgoalPage]

})
export class GoalsettingPageModule {}
