import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { BaseComponent } from '../../../base/base.component';
import { IcreateGoal } from '../../../interface/goal/icreate-goal';
import { AddnewgoalPage } from '../addnewgoal/addnewgoal.page';

@Component({
  selector: 'app-goalsetting',
  templateUrl: './goalsetting.page.html',
  styleUrls: ['./goalsetting.page.scss'],
})
export class GoalsettingPage extends BaseComponent implements OnInit {
  minamount : number = 1000;  mintime: number = 1;
  maxamount: number = 100000; maxtime: number = 12;
  sliderrange: number = 1000; slidemonths: number = 1;
  dataReturned:any;
  new_goal: string;
  goalType: string;

  ngOnInit() { }

  /**
   * For Setting new goal 
   * Shovon
   */
  async openModal() {
    
    const modal = await this.modalController.create({
      component: AddnewgoalPage,
      componentProps: {
        "goalType": 'demo_demo'
      }
    });

    modal.onDidDismiss().then((dataReturned) => {
      console.log("dataReturned");  
      console.log(dataReturned);  

      this.dataReturned = dataReturned.data;
      
      if (this.dataReturned !== null) {
        let goalData: IcreateGoal = {
          Phonenumber: localStorage.getItem('phoneno'),
          GoalAmount: this.sliderrange,
          GoalTime: this.slidemonths,
          GoalType: this.dataReturned.type
        }

        try {
          this.present('Setting your goal..',5000);
          this.goalService.createNewGoal(goalData).subscribe(
            res => {
              this.dismiss();
              if(res['Success']){
                // this.alertMessage('Success', res['ExtendedMessage']);
                this.currentUserDetails.IsGoalCreated = true;
                localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails))
               // this.events.publish('reloadHome', 'Hello from page1!');
               this.navCtrl.navigateRoot('goalcreated');
              }
              //console.log(res)
            }, err =>{
              this.dismiss();
              this.alertMessage('Error', 'Server issue occured, please try again later')
            }
          )
        } catch (error) {
          this.dismiss();
          this.alertMessage('Error', 'Server issue occured, please try again later')
        }
        console.log(this.goalType, this.sliderrange, this.slidemonths)
      }
    });

    return await modal.present();
  }

 createNewGoal(){
    let goalData: IcreateGoal = {
      Phonenumber: localStorage.getItem('phoneno'),
      GoalAmount: this.sliderrange,
      GoalTime: this.slidemonths,
      GoalType: this.goalType
    }

    try {
      this.present('Setting your goal..',5000);
      this.goalService.createNewGoal(goalData).subscribe(
        res => {
          this.dismiss();
          if(res['Success']){
            this.alertMessage('Success', res['ExtendedMessage']);
            this.currentUserDetails.IsGoalCreated = true;
            localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails))
            // this.events.publish('reloadHome', 'Hello from page1!');
            this.navCtrl.navigateRoot('goalcreated');
          }
          //console.log(res)
        }, err =>{
          this.dismiss();
          this.alertMessage('Error', 'Server issue occured, please try again later')
        }
      )
    } catch (error) {
      this.dismiss();
      this.alertMessage('Error', 'Server issue occured, please try again later')
    }
    console.log(this.goalType, this.sliderrange, this.slidemonths)
  }



  decreaseAmount(){
    if(this.sliderrange > this.minamount && this.sliderrange <=this.maxamount ){
     this.sliderrange -= 100;
    }
  }
  increaseAmount(){
    if(this.sliderrange>= this.minamount && this.sliderrange <this.maxamount ){
      this.sliderrange += 100;
    }
  }



  decreaseTime(){
    if(this.slidemonths > this.mintime && this.slidemonths <= this.maxtime ){
      this.slidemonths -= 1;
     }

  }
  increaseTime(){
    if(this.slidemonths >= this.mintime && this.slidemonths <this.maxtime ){
      this.slidemonths += 1;
    }
  }

  change() {
    console.log("Suryabhan");
  }
  /**
   * End of class
   */
}
