import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { GoalService } from 'src/app/services/goalServices/goal.service';


@Component({
  selector: 'app-goalcreated',
  templateUrl: './goalcreated.page.html',
  styleUrls: ['./goalcreated.page.scss'],
})
export class GoalcreatedPage extends BaseComponent implements OnInit {
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  mileStones: any = [];
  goalDetails: any = [];
  totalNumberSet = 0;
  permonthAmount = 0;
  status: any;
  goalTime:any = 0
  

  ngOnInit() {
    this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  }
  ionViewDidEnter() {
    this.getGoalDetails();
    console.log(this.currentUserDetails.PhoneNumber)   
    this.status =parseInt(this.currentUserDetails['Status']);
    console.log(this.status);      
  }
  /** 
   * For getting new goal 
   * Shovon
   */
  getGoalDetails(){
    try {
      this.present('Getting your goal...', 5000);
      this.goalService.getGoalLists(this.currentUserDetails.PhoneNumber).subscribe(            
        res => {
          this.dismiss();
          console.log(res);
          if(res['Success']){
            this.mileStones = res['GoalDetail'][0]['MileStone'];
            this.goalDetails = res['GoalDetail'][0];
            this.goalTime = this.goalTime['GoalTime'];
            this.totalNumberSet = Math.ceil((this.mileStones.length-3)/3);
            this.permonthAmount = res['GoalDetail'][0]['MileStone'][0]['MileStoneAmount']
          }
          else {
              this.alertMessage('Error', res['ErrorMessage']);
          }
          console.log(res,this.mileStones)
        }, err =>{
        this.dismiss();      
        this.alertMessage('Error', 'Server issue occured, please try again later')
      });
    } catch (error) {
      this.dismiss();      
      this.alertMessage('Error', 'Server issue occured, please try again later')
    }    
  }

  /**
   * Confirmation alert
   */
  async getBack(){                    
    this.navCtrl.navigateRoot('homeTab');
  }

  setyourgoal(){
    
  }
}
