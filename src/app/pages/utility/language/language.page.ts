import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-language',
  templateUrl: './language.page.html',
  styleUrls: ['./language.page.scss'],
})
export class LanguagePage extends BaseComponent implements OnInit {

 languageList = [
   {
     code : 'KA',
     english : 'Kannada',
     language : 'ಕನ್ನಡ'
   },
   {
    code : 'EN',
    english : '',
    language : 'English'
  }
 ]

  ngOnInit() {
  }


/** Confirm meaage change  */
async confirmLanguage(language) {

  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Confirm!',
    message: 'Do you want to change your language to '+((language.english) ? language.english : language.language),
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Okay',
        handler: () => {
          this.changeLanguage(language.code)
          console.log('Confirm Okay');
        }
      }
    ]
  });

  await alert.present();
}


/** Change language in user */
changeLanguage(language){
    this.showLodder('Changing the language..')
    this.utilService.changeLanguage(language,this.currentUserDetails.UserId).subscribe(
        res =>{
            
            console.log(res);
            if(res['Success']){
                this.currentUserDetails.Lang = language;
                localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails));
                this.languageService.setLanguage(language.toLowerCase())
                this.alertMessage('Alert','Language changed');
            } else{
                this.alertMessage('Alert', (res['ErrorMessage']) ? res['ErrorMessage'] : res['ExtendedMessage']);
            }
            this.hideLoader();
            
        }, err =>{
            this.alertMessage('Alert', 'All fields mandatary');
            console.log(err);
        }
    )
}
  /** End */
}
