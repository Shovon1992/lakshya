import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { BlobService, UploadConfig, UploadParams } from 'angular-azure-blob-service'
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import {File, FileEntry} from '@ionic-native/file/ngx';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends BaseComponent implements OnInit {

  userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'));
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  uploadimg: any;
  currentFile: any; 
  userImage = 'https://lakresourcevmdiag.blob.core.windows.net/selfiimage/'+this.currentUserDetails.UserId+'.jpg?random+\=' +  new Date();    

  Config: UploadParams = {
    sas: '?sv=2019-02-02&ss=bfqt&srt=sco&sp=rwdlacup&se=2021-11-06T13:32:40Z&st=2020-02-16T05:32:40Z&spr=https,http&sig=BlGY3GergCp9iqFgU0X%2BJpRYlRl59jQyF5XFirr5jnw%3D',
    storageAccount: 'lakresourcevmdiag',
    containerName: 'selfiimage'
  };

  ngOnInit() {
    this.userImage = 'https://lakresourcevmdiag.blob.core.windows.net/selfiimage/'+this.currentUserDetails.UserId+'.jpg?random+\=' +  new Date();    

  }
fileName: any;

/** */
capturePic(sourceType){
  const options: CameraOptions = {
    quality: 50,
    targetWidth: 200,
    targetHeight: 200,
    destinationType: this.camera.DestinationType.FILE_URI,
    sourceType : sourceType,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    cameraDirection : this.camera.Direction.FRONT,
    correctOrientation : true,
    allowEdit : true
  }
  this.camera.getPicture(options).then((imageData) => {




    this.file.resolveLocalFilesystemUrl(imageData).then((entry: FileEntry) => 
      {
        entry.file(file => {
          console.log(file);
          this.fileName = file.name;
          this.readFile(file);
        });
      });


    
   }, (err) => {
    // Handle error
   });
}
config : any;


get_file(file){
  console.log(file)
  this.currentFile = file.target.files[0];
  // this.upload();
  const baseUrl = this.blob.generateBlobUrl(this.Config, this.fileName);
  this.config = {
    baseUrl: baseUrl,
    sasToken: this.Config.sas,
   // blockSize: 1024 * 64, // OPTIONAL, default value is 1024 * 32
    file: file.target.files[0], //res.nativeURL,
    complete: () => {
      console.log('Transfer completed !');
    },
    error: (err) => {
      console.log('Error:', err);
    },
    progress: (percent) => {
      console.log(percent);
    }
  }
  this.blob.upload(this.config);
}



readFile(file) {
  const reader = new FileReader();
  reader.onload = () => {
    const blob = new Blob([reader.result], {
      type: file.type
    });
    this.showLodder('Uploading image')
    const baseUrl = this.blob.generateBlobUrl(this.Config, this.currentUserDetails.UserId+'.jpg');
    console.log(blob,baseUrl);
    this.config = {
      baseUrl: baseUrl,
      sasToken: this.Config.sas,
    // blockSize: 1024 * 64, // OPTIONAL, default value is 1024 * 32
      file: blob, //res.nativeURL,
      complete: () => {
        console.log('Transfer completed !');
        this.zone.run(()=>{
          this.userImage = 'https://lakresourcevmdiag.blob.core.windows.net/selfiimage/'+this.currentUserDetails.UserId+'.jpg?random+\=sadsadsadas' +  new Date();    
          this.ngOnInit();
          this.events.publish('userLogged', { profileImage : this.userImage });
          this.hideLoader();
        })
        
      },
      error: (err) => {
        console.log('Error:', err);
      },
      progress: (percent) => {
        console.log(percent);
      }
    }
    this.blob.upload(this.config);
    };
   reader.readAsArrayBuffer(file);
};



async presentActionSheet() {
  const actionSheet = await this.actionSheetController.create({
    header: 'Update profile picture',
    mode : 'ios',
    cssClass: 'my-custom-class',
    buttons: [ {
      text: 'Capture Image',
      icon: 'camera',
      handler: () => {
        this.capturePic(this.camera.PictureSourceType.CAMERA);
      }
    }, {
      text: 'Upload Image',
      icon: 'images',
      handler: () => {
        this.capturePic(this.camera.PictureSourceType.PHOTOLIBRARY)
      }
    }, {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present();
}


/** End of class */
}
