import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { BankaccountPage } from './bankaccount.page';
import { CustomComponentModule } from 'src/app/customComponents/custom-component/custom-component.module';

const routes: Routes = [
  {
    path: '',
    component: BankaccountPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,ReactiveFormsModule,
    IonicModule,TranslateModule,
    RouterModule.forChild(routes),
    CustomComponentModule,
  ],
  declarations: [BankaccountPage]
})
export class BankaccountPageModule {}
