import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../../base/base.component';
import { FolioCreation } from '../../../interface/folio-creation';

@Component({
  selector: 'app-bankaccount',
  templateUrl: './bankaccount.page.html',
  styleUrls: ['./bankaccount.page.scss'],
})
export class BankaccountPage extends BaseComponent implements OnInit {
  bankdetails : any = [];
  nomieeDetails : any = [];
  
  panDetails = JSON.parse(localStorage.getItem('pandetails'));
  userDetails = JSON.parse(localStorage.getItem('userDetails'));
  currentDate = new Date();
  accountTypes: any = [];
 occupationList = ['Public Sector Job', 'Private Sector Job', 'Auto rickshaw (business)', 'Others' ];
 nomineeTypes = ['Father' , 'mother' , 'brother' , 'sister',  'Husband','Wife', 'Son', 'daughter', 'others'];
  /** Mandatory data to create folio or account */
  accountDetails: FolioCreation = {
    AccountNumber: this.bankdetails.accountno,
    Occupation: this.nomieeDetails.cooupation,
    Nominee: (this.nomieeDetails.nonominee) ? this.nomieeDetails.nomieeName : '',
    NomineeRelationship: (this.nomieeDetails.nonominee) ? this.nomieeDetails.relation : '',
    HasNomination: (this.nomieeDetails.nonominee) ? this.nomieeDetails.nonominee : false,
    Name:  (this.userDetails.POIInformation) ? this.userDetails.POIInformation.Name : '',
    Dob : (this.nomieeDetails.DOB) ? this.nomieeDetails.DOB : '',
    IFSC: this.bankdetails.ifsccode,
    UserId: this.userDetails.UserId,
    OTPID: null,
    FolioNumber: null,
    Mobile: this.userDetails.PhoneNumber,
    PAN: (this.userDetails.POIInformation) ?  this.userDetails.POIInformation.PAN : '',
    EmailId: this.userDetails.EmailId,

  };


  step = 1;
  OnInit(){
    document.addEventListener("backbutton",function(e) {
      console.log("disable back button")
    }, false);

    console.log("Suryabhan");
    console.log(this.nomieeDetails);
    this.userDetails.POIInformation = (this.userDetails.POIInformation) ? this.userDetails.POIInformation : []
  }
  ionViewDidEnter(){
    this.nomieeDetails.nonominee = false;
    console.log("Suryabhan");
    console.log(this.nomieeDetails);
    this.getAllTaxStatus();
  }


  /**
   * Confirmation alert
   */
  async getBack(){
    // this.events.publish('reloadHome', 'Hello from page1!');     
    // this.router.navigateByUrl('homeTab');
    this.navCtrl.back();
    // this.navCtrl.navigateRoot('homeTab');
  }

/**
 * Get bank details according to ifc code 
 */
getBankDetailsByIfc(code){
  try {
    this.showLodder('Fetching bank details..');
    this.bankService.getBankDetailsByIfc(code).subscribe(
      res =>{
        this.hideLoader();
        /*
        *On success populate data to form
        */
        if(res['Success']){
          this.bankdetails.bankname = res['Bank'];
          this.bankdetails.branch = res['Branch'];
          this.bankdetails.state = res['State'];
          this.bankdetails.city = res['City'];
        }else{
          this.alertMessage('Error',(res['ExtendedMessage'])? res['ExtendedMessage']: res['ErrorMessage']);
        }
      }, err =>{
        this.hideLoader();
        this.alertMessage('Error','Unable to fetch bank details. Please chek IFCI code.');
      }
    )
  } catch (error) {
    this.alertMessage('Error','Unable to fetch bank details. Please chek IFC code.');
  }
  
}

/**
 * Goto to next step
 */
nestStep(step){
  if(step == 1){
    if(this.nomieeDetails.cooupation){
      this.step = 2;
    }
  }else if(step ==2 ){
    try {

    this.accountDetails = {
    AccountNumber: this.bankdetails.accountno,
    Occupation: this.nomieeDetails.cooupation,
    Nominee: (this.nomieeDetails.nomieeName) ? this.nomieeDetails.nomieeName : '',
    OTPID: null,
    NomineeRelationship: (this.nomieeDetails.relation) ? this.nomieeDetails.relation : '',
    HasNomination: (this.nomieeDetails.nonominee) ? this.nomieeDetails.nonominee : false,
    Name: (this.userDetails.POIInformation) ? this.userDetails.POIInformation.Name : '',
    Dob : this.nomieeDetails.DOB,
    IFSC: this.bankdetails.ifsccode,
    UserId: this.userDetails.UserId,
    FolioNumber: null,
    Mobile: this.userDetails.PhoneNumber,
    PAN: (this.userDetails.POIInformation) ? this.userDetails.POIInformation.PAN : '',
    EmailId: this.userDetails.EmailId,

      };
      let selectedDOB = new Date(this.accountDetails['DOB'])
      let diffDays = Math.ceil(Math.abs((selectedDOB.valueOf() - this.currentDate.valueOf()) / (1000 * 3600 * 24*365))) ;
      let tempAccountDetails = this.accountDetails;
       
      this.accountDetails['HasNomination'] = true;
      if (this.accountDetails['HasNomination']) {
        this.accountDetails['Name'] = "";
        this.accountDetails['NomineeRelationship'] = "";
        this.accountDetails['Dob'] = "";
      }


      // if(diffDays < 18){
        this.showLodder('Creating account..');
      this.bankService.createNewAcount(this.accountDetails).subscribe(
        res =>{
           tempAccountDetails['BankAccType'] = this.bankdetails.accountType;
      console.log(tempAccountDetails, this.bankdetails.accountType, this.bankdetails)
      localStorage.setItem('tempAccountDetails',JSON.stringify(this.accountDetails));
          this.hideLoader();
          /* 
          *On success populate data to form
          */
         console.log("Sur");
         console.log(res);
         //this.showConfirmationAlert(res['ExtendedMessage']);

         //this.showConfirmationAlert('Pan name and panee tansfer name dose not matched. Do you want to continue');
          if(res['Success']){
           this.showConfirmationAlert(res['ExtendedMessage']+'<br/><br/>  Lakshya can disable the account later if bank account does not belong to you');
          }else{
            this.alertMessage('Error',(res['ExtendedMessage'])? res['ExtendedMessage']: res['ErrorMessage']);
          }
        }, err =>{
          this.hideLoader();
          this.alertMessage('Error',('Unable to create account.'));
        }
      )
      // } else{
      //   this.alertMessage('Error','Nominee must be 18 years old.');
      // }
      
    } catch (error) {
      console.log(error)
      this.alertMessage('Error','Unable to create account.');
    }
  }
}


/**
 * If name dosen't match show alert message with confirmation box
 */
async showConfirmationAlert(message){
let alert = await this.alertCtrl.create({
header: 'Note !',
message: message,
buttons: [
  {
    text: 'Cancel',
    role: 'cancel',
    handler: () => {
      console.log('Cancel clicked');
    }
  },
  {
    text: 'Continue',
    handler: () => {
      this.sendFolioOtp(this.userDetails.PhoneNumber);
    }
  }
]
});
alert.present();
}




/**
 * account Type
 */
getAccountType(){
  try {
      
    let data= {
      PAN: (this.userDetails.POIInformation) ? this.userDetails.POIInformation.PAN : '',
      TaxCode: this.bankdetails.taxtype,
    };
      this.showLodder('Loading accounts..');
      this.transationService.getAccountTypes(data).subscribe(
        res =>{
          this.hideLoader();
          if(res['Success']){
           this.accountTypes = res['AccountType'];
          }else{
            this.alertMessage('Error',(res['ExtendedMessage'])? res['ExtendedMessage']: res['ErrorMessage']);
          }
        }, err =>{
          this.hideLoader();
          this.alertMessage('Error','Unable to create account.');
        }
      )
      // } else{
      //   this.alertMessage('Error','Nominee must be 18 years old.');
      // }
      
    } catch (error) {
      console.log(error)
      this.alertMessage('Error','Unable to create account.');
    }
}
/**
  * END
*/
}
