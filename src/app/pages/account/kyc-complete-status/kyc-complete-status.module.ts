import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { KycCompleteStatusPage } from './kyc-complete-status.page';
import { CustomComponentModule } from 'src/app/customComponents/custom-component/custom-component.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: KycCompleteStatusPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CustomComponentModule,TranslateModule
  ],
  declarations: [KycCompleteStatusPage]
})
export class KycCompleteStatusPageModule {}
