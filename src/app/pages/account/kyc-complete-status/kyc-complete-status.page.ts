import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { TranslateService } from '@ngx-translate/core';
//import { TransationService } from 'src/app/services/transationService/transation.service';
@Component({
  selector: 'app-kyc-complete-status',
  templateUrl: './kyc-complete-status.page.html',
  styleUrls: ['./kyc-complete-status.page.scss'],
})
export class KycCompleteStatusPage extends BaseComponent  implements OnInit {

  status = ['POA Upload', 'Signature Upload' ];
  userCurrentStatus = parseInt(this.currentUserDetails.Status);
  username: string = '';
  mobileno: any = '';
  emailid: any = '';
  //translate = new TranslateService
  ngOnInit() {
    document.addEventListener("backbutton",function(e) {
        console.log("disable back button")
    }, false);
  }  
 

  ionViewDidEnter() {   
    this.menuCtrl.enable(true); 
    this.userCurrentStatus = parseInt(this.currentUserDetails.Status);
    if (this.currentUserDetails.POIInformation !== null) {
      this.username = this.currentUserDetails['POIInformation']['Name'];
    }
    this.mobileno = this.currentUserDetails['PhoneNumber'];
    this.emailid = this.currentUserDetails['EmailId'];
    let data = {'name': this.username, 'mobileno': this.mobileno, 'emailid': this.emailid,'UserId' : this.currentUserDetails.UserId};
    this.events.publish('userLogged', data);

    document.addEventListener("backbutton",function(e) {
      console.log("disable back button")
    }, false);
  }
  /**
   * For next page redirection 
   */
  goToNextPage(stage){
    console.log(this.userCurrentStatus)
    switch(stage){
      case 3:
        this.navCtrl.navigateForward('verifykyc');
        break;
      case 4:
          this.navCtrl.navigateForward('uploadpoa');
          break;
      case 5:
          this.navCtrl.navigateForward('uploadsign');
          break;
      case 6:
          this.navCtrl.navigateForward('videoselfie');
          break;
      case 7:
        this.navCtrl.navigateForward('esign');
        break;
      default : 
          this.navCtrl.navigateForward('verifykyc');
          break; 
    }
  }

  continue() {
    let nextPage  = this.userCurrentStatus + 1;
    console.log(this.userCurrentStatus,nextPage)
    this.goToNextPage(nextPage)
  }
}
