import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VerifyFolioOtpPage } from './verify-folio-otp.page';
import { CustomComponentModule } from 'src/app/customComponents/custom-component/custom-component.module';

const routes: Routes = [
  {
    path: '',
    component: VerifyFolioOtpPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CustomComponentModule
  ],
  declarations: [VerifyFolioOtpPage]
})
export class VerifyFolioOtpPageModule {}
