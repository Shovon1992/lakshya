import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
//declare var SMSReceive: any;
@Component({
  selector: 'app-verify-folio-otp',
  templateUrl: './verify-folio-otp.page.html',
  styleUrls: ['./verify-folio-otp.page.scss'],
})
export class VerifyFolioOtpPage extends BaseComponent implements OnInit {
 folioOtpDetails = JSON.parse(localStorage.getItem('folioOtpDetails'));
 OTP1: any;
 OTP2: any;
 OTP3: any;
 OTP4: any;
 OTP5: any; 
 OTP: any = [];
 regNumber = localStorage.getItem('phoneno');
  res_otpid: any;
 userDetails = JSON.parse(localStorage.getItem('userDetails'));
  
/** ngOnInit() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
      success => console.log('Permission granted'),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
  );

  this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
  }
  


  start() {
    SMSReceive.startWatch(
        (success) => {
            document.addEventListener('onSMSArrive', (e: any) => {
                var IncomingSMS = e.data;
                //Your One Time Password (OTP) is 77016. ICICI Prudential Mutual Fund.
                console.log(e.data)
                this.OTP = IncomingSMS.body.slice(12, 16);
                this.OTP1 = this.OTP[0]
                this.OTP2 = this.OTP[1]
                this.OTP3 = this.OTP[2]
                this.OTP4 = this.OTP[3]
                this.OTP5 = this.OTP[4]
                //this.processSMS();
            });
        },
        (error) => {
           // alert(error);
        }
    )
}
 */


  
/**
 * Create Folio
 */
verifyOTP(){
  console.log("Demo Suryabhan....");
  console.log(this.OTP1);
  console.log(this.OTP2);
  console.log(this.OTP3);
  console.log(this.OTP4);
  console.log(this.OTP5);
  if (this.OTP1 && this.OTP2 && this.OTP3 && this.OTP4 && this.OTP5) {
    try {
      this.showLodder('Validating the OTP.');
      
      let data ={
        "Mobile": this.userDetails.PhoneNumber,
        "OTPId":this.folioOtpDetails.OTPID,
        "OTP": this.OTP1+''+this.OTP2+''+this.OTP3+''+this.OTP4+''+this.OTP5,
      }
      console.log(data);

      this.bankService.verifyFolioOtp(data).subscribe(
        res =>{
          this.hideLoader();
          /*
          *On success populate data to form
          */
        console.log("Suryabahan  Res");
        console.log(res);
        this.res_otpid = res['OTPID'];
          if(res['Success']){
            //this.alertMessage('Success','OTP virfied.');
            this.createFolio();
          }else{
            this.alertMessage('Error','Unable to validate otp.');
          }
        }, err =>{
          this.hideLoader();
          this.alertMessage('Error','Unable to validate otp.');
        }
      )
    } catch (error) {
      this.alertMessage('Error','Unable to validate otp.');
    }
  }
  else {
    console.log("false Suryabhan....");
    return;
  }
}

/**
 * Send folio OTP
 */
createFolio(){
  //try {
   this.showLodder('Creating Folio..');
    let data = JSON.parse(localStorage.getItem('tempAccountDetails'));
    
    console.log("createFolio RES");
    console.log(data)
    //data.OTPId =  this.folioOtpDetails.OTPId;
        
    data.OTPId = this.res_otpid;
    console.log(data)
    this.bankService.createNewFolio(data).subscribe(
      res =>{
       this.hideLoader();
        /*
        *On success populate data to form
        */
       console.log(res)
       //this.navCtrl.navigateForward('verify-folio-otp');
       //localStorage.setItem('folioOtpDetails',JSON.stringify(res));
        if(res['Success']){
         this.currentUserDetails.Status = 8;
         localStorage.setItem('userDetails',JSON.stringify(this.currentUserDetails));
         this.navCtrl.navigateRoot('successpage');
         
        }else{
          this.alertMessage('Error','Unable to create Folio.');
        }
      }, err =>{
        this.hideLoader();
        this.alertMessage('Error','Unable to create Folio.');
      }
    )
  // } catch (error) {
  //   this.alertMessage('Error','Unable to create Folio.');
  // }
}

onKeydownEvent(event: KeyboardEvent,prev,keyPos): void {
  if (event.keyCode == 46 || event.keyCode == 8) {
    if (keyPos == 5) {
        this.OTP5 = "";
    }
    else if (keyPos == 4) {
      this.OTP5 = "";
      this.OTP4 = "";
    }
    else if (keyPos == 3) {
      this.OTP5 = "";
      this.OTP4 = "";
      this.OTP3 = "";
    }
    else if (keyPos == 2) {
      this.OTP5 = "";
      this.OTP4 = "";
      this.OTP3 = "";
      this.OTP2 = "";
    }
    else if (keyPos == 1) {
      this.OTP5 = "";
      this.OTP4 = "";
      this.OTP3 = "";
      this.OTP2 = "";
      this.OTP1 = "";
    }
    
    console.log(this.OTP);
    prev.setFocus();
  }
}




goToNextInput1(event: any,prev,current, next,keyPos, charCode) {
  const pattern = /[0-9,]/;
  let inputChar = String.fromCharCode(event.charCode);

  if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
      this.OTP[keyPos] = "";
      return false;
  } else {
      if (charCode.length == 0) {
          if (current.value.length == 0) {
              next.setFocus()
              return true;
          } else {
              return false;
          }
      }
  }
}


checkLength(ele, charCode) {
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  } else {
      if (ele.value.length != 0) {
          return false
      }
  }
}


  /**
   * End of class
   */
}
