import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { EsignModalPage } from '../esign-modal/esign-modal.page';

@Component({
  selector: 'app-esign',
  templateUrl: './esign.page.html',
  styleUrls: ['./esign.page.scss'],
})
export class EsignPage extends BaseComponent implements OnInit {

  
  pdfData: any =[];
  ngOnInit() {
    console.log(this.currentUserDetails.UserId);
    this.showLodder('Loading ');
    this.kycService.createPdfUrl(this.currentUserDetails.UserId).subscribe(
      res =>{
        this.hideLoader()
        this.pdfData = res
        console.log(res)
      }, err =>{
        this.hideLoader()
        console.log(err)
      }
    )
  }


  /**
   * Create webvew and wait for it's responce
   */
  createWebview(htmlData) {
    // this.openModal()
    let url = 'data:text/html;base64,' + btoa(htmlData)
    var target = "_blank";
    var options = "hidenavigationbuttons=no,hidden=no,hideurlbar=yes,location=yes,closebuttoncolor=red,clearsessioncache=yes,clearcache=yes,closebuttoncaption=Close,footer=no,zoom=no,hidespinner=no,hidenavigationbuttons=no";
    let newBrowser = this.inAppBrowser.create(htmlData, target, options);
    newBrowser.on('exit').subscribe(res => {
        console.log('testtttt udfgjhdgfdhjfgjhdsf jhgidfhhjsdgfgsdhjfgjhghjds ',res)
        this.saveAadharSign();
    }, err => {
        console.log(err)
    })
  }
  
  
/** save adhar sign */
saveAadharSign(){
  this.showLodder('Saving your status');
  this.kycService.saveEsing(this.currentUserDetails.UserId).subscribe(
    res =>{
      this.hideLoader()
      if(res['Success']){
        this.router.navigateByUrl('homeTab');
      }else {
        this.alertMessage('Error', (res['ErrorMessage'])  ? res['ErrorMessage'] : res['ExtendedMessage'] )
      }
      console.log(res)
    }, err =>{
      this.hideLoader()
      console.log(err)
    }
  )
}


async openModal() {
  const modal = await this.modalController.create({
  component: EsignModalPage
  });
  return await modal.present();
 }
  /** end of class  */
}
 