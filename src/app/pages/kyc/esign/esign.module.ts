import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EsignPage } from './esign.page';
import { TranslateModule } from '@ngx-translate/core';
import { CustomComponentModule } from 'src/app/customComponents/custom-component/custom-component.module';

const routes: Routes = [
  {
    path: '',
    component: EsignPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes),
    CustomComponentModule,
  ],
  declarations: [EsignPage]
})
export class EsignPageModule {}
