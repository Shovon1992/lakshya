import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoldHomePage } from './gold-home.page';

describe('GoldHomePage', () => {
  let component: GoldHomePage;
  let fixture: ComponentFixture<GoldHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoldHomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoldHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
