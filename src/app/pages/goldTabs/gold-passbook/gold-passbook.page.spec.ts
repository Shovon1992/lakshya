import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoldPassbookPage } from './gold-passbook.page';

describe('GoldPassbookPage', () => {
  let component: GoldPassbookPage;
  let fixture: ComponentFixture<GoldPassbookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoldPassbookPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoldPassbookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
