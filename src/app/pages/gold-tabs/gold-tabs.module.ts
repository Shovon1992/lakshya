import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GoldTabsPage } from './gold-tabs.page';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: 'tabs',
    component: GoldTabsPage,
    children:[
        { path: 'home', loadChildren: '../goldTabs/gold-home/gold-home.module#GoldHomePageModule' },
        {path: 'passbook', loadChildren: '../goldTabs/gold-passbook/gold-passbook.module#GoldPassbookPageModule'},
    ]
  },
  {
    path:'',
    redirectTo:'/goldTab/tabs/home',
    pathMatch:'full'
  },
  {
    path:'passbookTab',
    redirectTo:'/goldTab/tabs/passbook',
    pathMatch:'full'
  },
];

@NgModule({ 
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [GoldTabsPage]
})
export class GoldTabsPageModule {}
