import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoldTabsPage } from './gold-tabs.page';

describe('GoldTabsPage', () => {
  let component: GoldTabsPage;
  let fixture: ComponentFixture<GoldTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoldTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoldTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
