import { Component, OnInit } from '@angular/core';
import {BaseComponent} from 'src/app/base/base.component';
import { ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';


@Component({
  selector: 'app-masterpage',
  templateUrl: './masterpage.page.html',
  styleUrls: ['./masterpage.page.scss'],
})
export class MasterpagePage extends BaseComponent implements OnInit {
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  status: any;
  notifySMS = '';
  ngOnInit() {
    // this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    // if (this.currentUserDetails !== null && this.currentUserDetails.PhoneNumber !== null ) {
    //   console.log(this.currentUserDetails);
    //   this.status = parseInt(this.currentUserDetails['Status']);
    //   console.log(this.status);
    //   this.pageNavigation(this.status);    
    // }
    //this.userLogin();
    this.checkUpdate()
  }

  checkUpdate(){
    this.platform.ready().then(() => { 
  
           
  } )
      //this.codePush.sync({}, downloadProgress).subscribe((syncStatus) => console.log(syncStatus));
  }

  async ionViewDidEnter() {    
    this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    if (this.currentUserDetails !== null && this.currentUserDetails.PhoneNumber !== null ) {
      console.log(this.currentUserDetails);
      this.status = parseInt(this.currentUserDetails['Status']);
      console.log(this.status);
      this.pageNavigation(this.status);          
    } /** else {
      this.navCtrl.navigateRoot('otplogin'); //added 
    } **/

    let today = new Date();
    let dd = (today.getDate() < 10) ? '0'+today.getDate() : today.getDate();
    let mm = (today.getMonth()+1 < 10) ? '0'+ today.getMonth()+1:  today.getMonth()+1; 
    let yyyy = today.getFullYear();
  //this.localNotifications.cancelAll();
  let data = await this.file.readAsText(this.file.dataDirectory,'userinfo.json').then(
      res =>{
        console.log('hurrrrr ',res)
        this.currentUserDetails = JSON.parse(res)
      }
    );
  let isInvested = false;
  let inputFormat = mm+'/'+(today.getDate())+'/'+yyyy
  try {
    await this.file.readAsText(this.file.dataDirectory,'transation.json').then(
      res =>{
        res = JSON.parse(res);
        console.log('hurrrrr trans',res)

          if(res[0]['user'] == this.currentUserDetails.PhoneNumber){
            
            if(res && res[0]['data']){
              for(let i =0 ; i< res.length; i ++){
                var tempDiff = new Date(res[i]['date'])
                var dateDiff = (today.getDate() - tempDiff.getDate() )
                
              }
              console.log(tempDiff,dateDiff)
              if(dateDiff >= 1 ){
                  isInvested = true;
              }
              console.log(tempDiff,dateDiff, isInvested)
            }
          } else {
            let investData = new Array()
            this.file.writeFile(this.file.dataDirectory, 'transation.json', JSON.stringify(investData), {replace: true}).then(
              res =>{
                  console.log('file write res', res)
            })
          }
      }, err =>{
        let investData = new Array()
        console.log(err)
            this.file.writeFile(this.file.dataDirectory, 'transation.json', JSON.stringify(investData), {replace: true}).then(
              res =>{
                  console.log('file write res', res)
            })
      }
    ).catch(
      error =>{
        console.log(error);
        let investData = new Array()
            this.file.writeFile(this.file.dataDirectory, 'transation.json', JSON.stringify(investData), {replace: true}).then(
              res =>{
                  console.log('file write res', res)
            })
      }
    );
  } catch (error) {
    console.log(error)
  }
  if (this.currentUserDetails !== null && this.currentUserDetails.PhoneNumber !== null ) {
    console.log(this.currentUserDetails);
    this.status = parseInt(this.currentUserDetails['Status']);
    console.log(this.status);
    //this.pageNavigation(this.status);          
  }
  console.log('lllllllllllllll',this.currentUserDetails['Status'])
    if(this.currentUserDetails['Status'] < 7){
      this.notifySMS = "Please complete your KYC to complete your profile ";
      this.sendNotification(1,this.notifySMS)
    }
    if(this.currentUserDetails['Status'] == 7 ){
      this.notifySMS = "Dear Investor, your account creation is incomplete. \nComplete your account creation process to enjoy saving with Lakshya";
      this.sendNotification(2,this.notifySMS)
    } else{
      if (!isInvested) {
        //below changed by TP to remove the Rupees milestone
        this.notifySMS = "Dear Investor, You have missed your saving cycle .\nYou can save more today to reach your goal on time.";
        this.sendNotification(3,this.notifySMS);
        //console.log(this.notifySMS);
        //this.notifySMS = "Dear Investor, You have missed your saving cycle on " + this.mm+'/'+this.dd+'/'+this.yyyy+" of amount Rs. " + this.mileStones + ".You can save more today to reach your goal on time.";
      } 
      if(!this.currentUserDetails.IsGoalCreated) {
          
          this.notifySMS = "Dear Investor, You have not set your saving goal. \nSet a goal to make saving fun";
          this.sendNotification(4,this.notifySMS)
      }
    }
     	        
      
  }
  sendNotification(id,message){
    console.log(message)
    this.localNotifications.clearAll();
    let today = new Date();
    this.localNotifications.schedule({
      id: id,
      led: { color: '#FF00FF', on: 500, off: 500 },
      title: 'Lakshya Notification',
      text:'hii '+message,
      icon: 'http://climberindonesia.com/assets/icon/ionicons-2.0.1/png/512/android-chat.png',
      //icon: 'res://icon.png',
      sound:  '../app/assets/swiftly.mp3',
      lockscreen: true,
     //trigger: { at: new Date(new Date().getTime() + 5000) },
    //  trigger: { in : 2 , every: ELocalNotificationTriggerUnit.MINUTE},
      trigger: { every: {hour : 12 },count : 2},
      
  }); 
  }
  pageNavigation(status) {
    this.present('Loading ...', 5000);
          
    setTimeout(() => {
      this.dismiss();             
      this.navCtrl.navigateRoot('masterpage');          
      if (status < 3) {
        // this.router.navigateByUrl('/verifykyc');
        this.navCtrl.navigateRoot('verifykyc');
        // this.router.navigate(['/verifykyc'])
      } else if (status > 2 && status < 7) {
        // this.router.navigate(['/kyc-complete-status'])
        this.navCtrl.navigateRoot('kyc-complete-status');
        // this.router.navigateByUrl('/kyc-complete-status');
      } else {
        // this.router.navigateByUrl('/homeTab');
        // this.router.navigate(['/homeTab'])
        this.navCtrl.navigateRoot('homeTab');
      }
    }, 100);
  }
}
