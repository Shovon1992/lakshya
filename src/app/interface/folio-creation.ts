export interface FolioCreation {
    UserId: any;
    Name: any;
    Dob: any;
    Occupation: any;
    OTPID: any;
    Nominee?: any;
    NomineeRelationship?: any;
    HasNomination?: Boolean;
    AccountNumber: String;
    IFSC: any;
    FolioNumber?: any
    PAN: string;
    Mobile: string;
    EmailId?: string;
    BankAccType?: String;
}
 