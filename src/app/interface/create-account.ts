export interface CreateAccount {

    UserId: any;
    Name: any;
    Dob: any;
    Occupation: any;
    Nominee?: any;
    NomineeRelationship?: any;
    HasNomination?: Boolean;
    AccountNumber: any;
    IFSC: any;
    FolioNumber?: any
  }
