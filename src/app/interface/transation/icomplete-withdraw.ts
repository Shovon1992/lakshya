export interface ICompleteWithdraw {
    UserId: string,
    Mode: string,
    OTP_ID ?: string,
    Trxn_id: string,
    Folio_no ?: string,
    OTP_Text ?: string,
    Status : true,
    RETRY_COUNT ?: string,
    Success: true,
    ErrorMessage ?: string,
    ExtendedMessage ?: string
}
