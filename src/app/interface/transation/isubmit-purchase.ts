export interface IsubmitPurchase {
        FolioNo: any,
        Amount: any,
        PaymentMode: any, /* PaymentMode: ‘DC’ for Debit Card, ‘UPI’ for UPI */
        TranId: any,
        PAN: any,
        TransType: Number, /**TransType: 1: 'Invest', 2: 'Withdraw' */
        GoalId: any,
        GoalStatus: any,
        VirtualPayemtAddress?: any, /** it is mandatary for upi transation */
        BankAccountNo: any,
        IpAddress: any,/** it is mandatary for upi transation */
        ChkDigit: any,
}
