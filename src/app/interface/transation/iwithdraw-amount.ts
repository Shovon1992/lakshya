export interface IwithdrawAmount {
    FolioNo: any,
    Amount: any,
    Units: any, //pass 0 only
    UserIpAddress: any, //for now using 10.10.10.10
    Mobile: any,
    TransType: any, // 2 is withdraw
    RedemOption: any
}
