import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterService } from './../services/register.service';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { AlertController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-pinauthentication',
  templateUrl: './pinauthentication.page.html',
  styleUrls: ['./pinauthentication.page.scss'],
})
export class PinauthenticationPage implements OnInit {
  OTP= []; CONOTP= [];
  showOTPInput: boolean = false;
  urlparam: any;
  flag:boolean = true;
  showInputType = 'password';
  showInputText = 'Show PIN';
  verifyCode = parseInt(localStorage.getItem('verifyCode')); 
 

  public pinGroup : FormGroup;
  constructor(public  router:  Router,public translate: TranslateService,public formBuilder: FormBuilder,public toast: Toast,
    public registerservice:RegisterService,public uniqueDeviceID: UniqueDeviceID,public loadingCtrl: LoadingController, public alertController : AlertController) {   
     this.pinGroup = this.formBuilder.group({
        PhoneNumber: localStorage.getItem('phoneno'),
        MobileNumber: localStorage.getItem('phoneno'),
        Email: localStorage.getItem('email'),
        PIN: localStorage.getItem('pin'),
        DeviceId: localStorage.getItem('uuid'),
        Lang: localStorage.getItem('lang'), 
        ReferralCode:localStorage.getItem('referalcode'),
        NewPin: [''],
        ConfirmPin: [''],
      });
    }

   //For Loadder 
   loadder: any;
   async showLodder(message){
     this.loadder = await this.loadingCtrl.create(
       {
         message: message
       }
     );
   this.loadder.present();
   }    

  ngOnInit() {
    this.uniqueDeviceID.get().then((uuid: any) => {
     // alert(uuid);
      localStorage.setItem('uuid',uuid);
    }).catch((error: any) => {
     // alert(error)
    });
    }
  gotokyc(){
    
    localStorage.setItem('pin',this.OTP.toString().replace(/,/g,''));
    // if (this.verifyCode == 2) {      
    //   this.pinGroup.patchValue({
    //     NewPin: this.OTP.toString().replace(/,/g,''), 
    //   });
    // } else {
      this.pinGroup.patchValue({
        NewPin: this.OTP.toString().replace(/,/g,''), 
        ConfirmPin: this.CONOTP.toString().replace(/,/g,''), 
        PIN: this.OTP.toString().replace(/,/g,''), 
      });
    // } 
    // const urlparam = '/Authentication/ResetPin';
    // this.registerservice.postCall(urlparam,this.pinGroup.value, false).subscribe(result => {
    //   //  this.information = result;
    //    alert(JSON.stringify(result));
    //   this.router.navigateByUrl('otplogin');
    //   },err=>{
    //     alert(JSON.stringify(err));
    //   });
      console.log("Data generated");
      console.log(this.pinGroup.value);
      
    if(this.pinGroup.get('NewPin').value !== '' && this.pinGroup.get('NewPin').value  == this.pinGroup.get('ConfirmPin').value){
      console.log("OTP Confirm submit");
      if (this.verifyCode == 2) {      
        this.urlparam = '/Authentication/ForgotPin';
      } else {
        this.urlparam = '/User/CreateUser';
      } 
      
      console.log(this.pinGroup.value);
      console.log(this.urlparam);
      this.showLodder('creating account..');
      this.registerservice.postCall(this.urlparam,this.pinGroup.value,true).subscribe(result => {
        //  this.information = result;
        this.loadder.dismiss();
      
        console.log('result');
        console.log(result);
        if(result.Success){
          this.presentAlertConfirm()
        }
        else{
          this.toast.show('Something went wrong please try again.', '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            });
        }
      }, err => {
          this.loadder.dismiss();
          this.toast.show('Something went wrong please try again.', '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            });
      });
    } else{
      this.toast.show('new pin and confirm pin do not match', '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        });
    }   
     // this.router.navigateByUrl('verifykyc');
  }


    /**
     * Toggle show hide
    */
   toggleView(){
      this.flag = !this.flag;
      this.showInputText = (this.showInputText == 'Show PIN') ? 'Hide PIN' : 'Show PIN';
    } 

  goToNextInput(charCode, nextStep){
    
    // let next = 'input'+parseInt(currentStage+1);
    // console.log(charCode.length,next)

    if (charCode > 31 && (charCode < 48 || charCode > 57) ) {
      return false;
    }else {
      if(charCode.length == 0 ){
        document.getElementById(nextStep).querySelector('input').focus();
        return true;
      }
    }
  }
  goToNextInput1(event: any,prev,current, next,keyPos, charCode) {
    const pattern = /[0-9,]/;
    let inputChar = String.fromCharCode(event.charCode);

    if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
        this.OTP[keyPos] = "";
        return false;
    } else {
      if (charCode.length == 0) {
          if (current.value.length == 0) {
              next.setFocus()
              return true;
          } else {
              return false;
          }
      }
    }
  }

  
onKeydownEvent(event: KeyboardEvent,prev,keyPos): void {
  if (event.keyCode == 46 || event.keyCode == 8) {
    if (keyPos == 8) {
        this.CONOTP[3] = "";
    }
    else if (keyPos == 7) {
        this.CONOTP[3] = "";
        this.CONOTP[2] = "";
    }
    else if (keyPos == 6) {
        this.CONOTP[3] = "";
        this.CONOTP[2] = "";
        this.CONOTP[1] = "";
    }
    else if (keyPos == 5) {
        this.CONOTP[3] = "";
        this.CONOTP[2] = "";
        this.CONOTP[1] = "";
        this.CONOTP[0] = "";
    }
    else if (keyPos == 4) {
        this.OTP[3] = "";
        this.CONOTP[3] = "";
        this.CONOTP[2] = "";
        this.CONOTP[1] = "";
        this.CONOTP[0] = "";
    }
    else if (keyPos == 3) {
        this.OTP[3] = "";
        this.OTP[2] = "";
        this.CONOTP[3] = "";
        this.CONOTP[2] = "";
        this.CONOTP[1] = "";
        this.CONOTP[0] = "";
    }
    else if (keyPos == 2) {
        this.OTP[3] = "";
        this.OTP[2] = "";
        this.OTP[1] = "";
        this.CONOTP[3] = "";
        this.CONOTP[2] = "";
        this.CONOTP[1] = "";
        this.CONOTP[0] = "";
    }
    else if (keyPos == 1) {
        this.OTP[3] = "";
        this.OTP[2] = "";
        this.OTP[1] = "";
        this.OTP[0] = "";
        this.CONOTP[3] = "";
        this.CONOTP[2] = "";
        this.CONOTP[1] = "";
        this.CONOTP[0] = "";
    }
    console.log(this.OTP);
    prev.setFocus();
  }
}


checkLength(event: any,ele,charCode) {
  const pattern = /[0-9,]/;
  let inputChar = String.fromCharCode(event.charCode);

  if (!pattern.test(inputChar)) {
    // invalid character, prevent input
    event.preventDefault();
    }
    else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
      if (ele.value.length != 0) {
          return false
      }
    }
  }


  /**Custom alert */
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Sucess!',
      message: 'You have successfully registerd. Please login with user name and password',
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            this.router.navigateByUrl('otplogin');
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  /**
   * End of class
   */
}
