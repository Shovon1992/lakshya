import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { PinauthenticationPage } from './pinauthentication.page';

const routes: Routes = [
  {
    path: '',
    component: PinauthenticationPage
  }
];

@NgModule({
  imports: [
    CommonModule,TranslateModule,
    FormsModule,ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PinauthenticationPage]
})
export class PinauthenticationPageModule {}
