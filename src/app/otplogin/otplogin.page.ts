import { Component,OnInit,ViewChild,ElementRef} from '@angular/core';
// import { Router } from  "@angular/router";
import { ToastController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterService } from './../services/register.service';
import { LanguageService } from './../services/language.service';
import { Toast } from '@ionic-native/toast/ngx';
import { BaseComponent } from '../base/base.component';
//import { BarcodeScanner ,BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

@Component({
    selector: 'app-otplogin',
    templateUrl: './otplogin.page.html',
    styleUrls: ['./otplogin.page.scss'],
})

export class OtploginPage extends BaseComponent implements OnInit {
   
    public status: Boolean = false;
    showInputType = 'password';
    showInputText = 'Show PIN';
    flag:boolean = true;
    phonenum: string = '';
    OTP = [];
    public otpGroup: FormGroup;
    showOTPInput: boolean = false;
    lang: string; 

    ngOnInit() {
        this.phonenum = '';
        this.otpGroup = this.formBuilder.group({
            PhoneNumber: ['', Validators.required],
            Pin: ['', Validators.required],
        });
    }

	ionViewDidEnter() {  
        this.menuCtrl.enable(false);
    }

    next() {
        this.router.navigateByUrl('language-popover');
    }
    verify() {
        this.router.navigateByUrl('otpverfication');
    }
    register() {
        localStorage.setItem('verifyCode','1');
        this.router.navigateByUrl('language-popover');
    }

    forgotPin() {
        localStorage.setItem('verifyCode','2');
        this.router.navigateByUrl('login');    
    }
    login() {
        this.otpGroup.patchValue({
            Pin: this.OTP.toString().replace(/,/g, ''),
        });
        localStorage.setItem('pin', this.OTP.toString().replace(/,/g, ''));
        const urlparam = '/Authentication/Login';
        if (this.otpGroup.valid && this.otpGroup.value['Pin']) {
            console.log(this.otpGroup.value['PhoneNumber'])
            
            if (this.otpGroup.value['PhoneNumber'].toString().length == 10) {
                localStorage.setItem('lastLogin', new Date().toISOString()); 
                this.present('Validating login details..', 5000);   
            
                try {
                    this.registerservice.postCall(urlparam, this.otpGroup.value, false).subscribe(result => {
                
                        if (result['TokenResponse'] && result['TokenResponse']['Token']) {
                            localStorage.setItem('lastLogin', new Date().toISOString()); 
                            const token = result['TokenResponse']['Token'];
                            
                            if (result['Lang'] !== null && result['Lang'] !== null ) {  
                                this.lang = result['Lang'].toLowerCase();                            
                            } 
                            else {
                                this.lang = 'en';
                            }
                            if (this.lang != 'en' && this.lang != 'ka') {  
                                this.lang = 'en';
                            }

                            localStorage.setItem('token', token);
                            localStorage.setItem('userDetails', JSON.stringify(result));
                            this.languageService.setLanguage(this.lang);
                            localStorage.setItem('phoneno', this.otpGroup.controls['PhoneNumber'].value)
                    
                            this.dismiss();
                            //this.sqlStorage.set('userInfo',JSON.stringify(result));
    
                            this.file.writeFile(this.file.dataDirectory, 'userinfo.json', JSON.stringify(result), {replace: true}).then(
                                res =>{
                                    console.log('file write res', res)
                                }
                            )
                            this.navCtrl.navigateRoot('masterpage');
                        } else {
                            this.dismiss();
                            //this.alertMessage('Error', "Unable to reach the server please retry.");
                            this.alertMessage('Error', (result['ExtendedMessage']) ? result['ExtendedMessage'] :
                                 'Something weent worng pleasse try again')
                        }
                    }, (err) => {
                        this.dismiss();
                        this.alertMessage('Error', "Unable to reach the server please retry.")
                    });
                } catch (error) {
                    this.dismiss();
                    console.log(error)
                    this.alertMessage('Error', 'Unable to reach the server please retry.')
                }
            } else {
                this.alertMessage('Error', 'Please validate the entered details')
            }            
        } else {
            this.alertMessage('Error', 'Please Enter 10 digit mobile number and 4 digit pin')
        }
    }
    
    onKeydownEvent(event: KeyboardEvent,prev,keyPos): void {
        const pattern = /[0-9,]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode == 46 || event.keyCode == 8) {
            if (keyPos == 4) {
                this.OTP[3] = "";
            }
            else if (keyPos == 3) {
                this.OTP[3] = "";
                this.OTP[2] = "";
            }
            else if (keyPos == 2) {
                this.OTP[3] = "";
                this.OTP[2] = "";
                this.OTP[1] = "";
            }
            else if (keyPos == 1) {
                this.OTP[3] = "";
                this.OTP[2] = "";
                this.OTP[1] = "";
                this.OTP[0] = "";
            }
            prev.setFocus();
        }
    }

    goToNextInput1(event: any,prev,current, next,keyPos, charCode) {
        const pattern = /[0-9,]/;
        let inputChar = String.fromCharCode(event.charCode);
    
        if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            this.OTP[keyPos] = "";
            return false;
        } else {
            if (charCode.length == 0) {
                if (current.value.length == 0) {
                    next.setFocus()
                    return true;
                } else {
                    return false;
                }
            }

        }
    }


    checkLength(ele, charCode) {
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            if (ele.value.length != 0) {
                return false
            }
        }
    }
    
    checkLengthMob(event: any,charCode) {
        const pattern = /[0-9,]/;
        let inputChar = String.fromCharCode(event.charCode);

        if (!pattern.test(inputChar)) {
             // invalid character, prevent input
             event.preventDefault();
         }
         else if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            return false;           
        } else {    
            if (charCode.length < 10) {
                return true;
            } else {
                return false
            }
         }    
    }


    /**
     * Toggle show hide
    */
    toggleView(){
        // this.showInputType = (this.showInputType == 'password') ? 'text' : 'password';
        // this.showInputText = (this.showInputText == 'Show Pin') ? 'Hide Pin' : 'Show Pin';
        this.flag = !this.flag;
        this.showInputText = (this.showInputText == 'Show PIN') ? 'Hide PIN' : 'Show PIN';
    } 



    

    
}
