import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
//import { ImagePicker,ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { RegisterService } from './../services/register.service';
import {NgxImageCompressService} from 'ngx-image-compress';
import { LoadingController, NavController, NavParams,AlertController } from '@ionic/angular';
import { Toast } from '@ionic-native/toast/ngx';
import { KycService } from '../services/kycService/kyc.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { BaseComponent } from '../base/base.component';
@Component({
  selector: 'app-poaimage',
  templateUrl: './poaimage.page.html',
  styleUrls: ['./poaimage.page.scss'],
})
export class PoaimagePage extends BaseComponent implements OnInit {
  side:string = 'front';
  uploadimg:any ="/assets/images/aadhaarcard.png";
  frontimage:any ="/assets/images/aadhaarcard.png"; 
  backimage:any ="/assets/images/aadhaarcard.png"; 
  showFrontImage:any ;
  showBackImage:any;
  documentType : any;
  front_name: any;
  back_name: any;
  poa_type: any;
  fileContent = {
    video : null,
    image : null,
  }
  docs = ['Null', 'Aadhaar', 'Driving Licence', 'Voter ID','Passport']
  // constructor(private  router:  Router,public alertCtrl: AlertController,private translate: TranslateService,private registerservice:RegisterService,
  //   private imagePicker: ImagePicker,private imageCompress: NgxImageCompressService,
  //   private camera: Camera, public loadingCtrl: LoadingController,public activateRoute: ActivatedRoute, public toast: Toast, public kycService : KycService, public navCtrl: NavController) { 
  // }
  userDetails: any;
  ngOnInit() {
    this.documentType = this.activateRoute.snapshot.paramMap.get('documentType')
    if(localStorage.getItem('side') !== 'back'){
      localStorage.setItem('side','front');
    }
  }

  ionViewDidEnter(){
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'));
    console.log(this.userDetails);
    console.log(this.docs);
    //var ddMMyyyy = this.datePipe.transform(new Date(),"dd-MM-yyyy");
    //console.log(this.docs);
    
    //this.side = localStorage.getItem('side');
  }


  /**
   * Image capture 
   */
  captureImage(){}
 /* getPic(){
    const options: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 50,
      width: 512,
      height: 150,
      outputType: 1
    }
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.uploadimg= 'data:image/jpeg;base64,' + results[0];
        if(localStorage.getItem('side') === 'front'){
          
         this.imageCompress.compressFile(this.uploadimg, orientation, 50, 50).then(
          result => {
            alert(result);
            this.frontimage =result.split(',')[1];
            localStorage.setItem("frontimage",result.split(',')[1] ) });
        }
        if(localStorage.getItem('side') === 'back'){
         
        //  this.backimage =results[0];
          this.imageCompress.compressFile(this.uploadimg, orientation, 50, 50).then(
            result => {
              this.frontimage =result.split(',')[1];
              localStorage.setItem("backimage",result.split(',')[1]) });
        }
      
      this.router.navigate(['docconfirmation'], { state: { page: 'uploadpoa',uploadimg: this.uploadimg } });
      }
      }, (err) => {
      // console.log(‘err’ + err);
      });
   // this.router.navigate(['docconfirmation'], { state: { page: 'uploadpan' } });
  }
  capturePic(){
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 512,
      targetHeight: 150,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      this.uploadimg='data:image/jpeg;base64,' + imageData;
      if(localStorage.getItem('side') === 'front'){
        localStorage.setItem("frontimage",imageData);
        this.frontimage =imageData;
       }
       if(localStorage.getItem('side') === 'back'){
         localStorage.setItem("backimage",imageData);
         this.backimage =imageData;
       }
     
      this.router.navigate(['docconfirmation'], { state: { page: 'uploadpoa',uploadimg: this.uploadimg } });
     }, (err) => {
      // Handle error
     });
  }*/
  

 /**
   * 
   * type should be 0 (gallary) 1 (camera)
   * Updated code by shovon  11.11.19 for uploading image or capture image 
   * 
   * */

   //For Loadder 
   loadder: any;
   async showLodder(message){
     this.loadder = await this.loadingCtrl.create(
       {
         message: message
       }
     );
   this.loadder.present();
   }

 
  getPic(type){
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 912,
      targetHeight: 950,
      destinationType: this.camera.DestinationType.NATIVE_URI,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: type,
      cameraDirection: this.camera.Direction.BACK
    } 
    this.camera.getPicture(options).then((imageData) => {
      
      //this.uploadimg='data:image/jpeg;base64,' + imageData;
      // this.videosrc = data[0]['fullPath'];
      imageData = imageData.includes("?") ? imageData.substr(0, imageData.lastIndexOf('?')) : imageData;
      console.log('poa image data', imageData)
      var dirpath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
       //dirpath = imageData.includes("file://") ? imageData : "file://" + imageData;
      if( this.side == 'front'){
        this.showLodder('Uploading document details..');
        let filename = imageData.substr(imageData.lastIndexOf('/') + 1);
        let tempFileName = this.date.getTime() + '_' + filename;
        this.file.copyFile(dirpath, filename, this.file.dataDirectory, tempFileName).then(
          resss => {
              console.log(resss.nativeURL);
              let url = "https://persist.signzy.tech/api/files/upload";
              let parameter = new Object();
              parameter['ttl'] = this.ttl
              let options: FileUploadOptions = {
                  
                  params: parameter,
                  fileName: tempFileName,
              }
              this.transfer.create().upload(resss.nativeURL, url, options).then((data: any) => {
                  let returnData = JSON.parse(data.response)

                  this.loadder.dismiss();
                  console.log(data)
                this.frontimage = returnData.file.directURL; 
                this.showFrontImage = this.sanitizer.bypassSecurityTrustUrl(this.frontimage)
                //this.frontimage = resss.nativeURL

                //const resolvedImg = this.webview.convertFileSrc(storedPhoto);
                //this.frontimage = this.sanitizer.bypassSecurityTrustUrl(returnData.file.directURL);
                //this.showFrontImage = true;       
                localStorage.setItem("frontimage",returnData.file.directURL);
                this.toast.show('Front image uploaded successfully', '5000', 'center').subscribe(
                  toast => {
                   
                  }
                );
              }, (err) => {
                this.loadder.dismiss();
                
                this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
                  toast => {
                    console.log(err);
                  }
                );          
              });
          }, err => {
            this.loadder.dismiss();
            this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
              toast => {
                console.log(err);
              }
            );          
          });

      }
      
      if(this.side === 'back'){
        this.showLodder('Uploading document details..');
        let filename = imageData.substr(imageData.lastIndexOf('/') + 1);
        let tempFileName = this.date.getTime() + '_' + filename;
        this.file.copyFile(dirpath, filename, this.file.dataDirectory, tempFileName).then(
          resss => {
              console.log(resss.nativeURL);
              let url = "https://persist.signzy.tech/api/files/upload";
              let parameter = new Object();
              parameter['ttl'] = this.ttl
              let options: FileUploadOptions = {
                  
                  params: parameter,
                  fileName: tempFileName,
              }
              this.transfer.create().upload(resss.nativeURL, url, options).then((data: any) => {
                  let returnData = JSON.parse(data.response)

                  this.loadder.dismiss();
                  console.log(data)
                this.backimage = returnData.file.directURL; 
                this.showBackImage = this.sanitizer.bypassSecurityTrustUrl(this.backimage)
                //this.showBackImage = true;       
                localStorage.setItem("backimage",returnData.file.directURL);
                this.toast.show('Back image uploaded successfully', '5000', 'center').subscribe(
                  toast => {
                   
                  }
                );
                  
              }, (err) => {
                this.loadder.dismiss();
                this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
                  toast => {
                    console.log(toast);
                  }
                );          
              });
          }, err => {
            this.loadder.dismiss();
            this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );          
          });
      }
     
      //this.router.navigate(['docconfirmation'], { state: { page: 'uploadpoa',uploadimg: this.uploadimg } });
    }, (err) => {
      // Handle error
    });
  }
  /*** Submit PAO Form */
  submitpoaform(side) {
    // if(this.userDetails['MerchantId'] !== null) {   
      //const poadata = JSON.parse(localStorage.pandetails);
      //alert(side)
      if(side == 'front'  ){
        localStorage.setItem('side','back');
        this.side = 'back';

      } else if(side == 'back'){
        // console.log(this.documentType);
        if (this.documentType == 1) {
          this.front_name = "adhar_front.jpeg";
          this.back_name = "adhar_back.jpeg";  
        } else if (this.documentType == 2) {
          this.front_name = "driving_li_front.jpeg";
          this.back_name = "driving_li_back.jpeg";  
        } else if (this.documentType == 3) {
          this.front_name = "voterid_front.jpeg";
          this.back_name = "voterid_back.jpeg";  
        } else if (this.documentType == 4) {
          this.front_name = "password_front.jpeg";
          this.back_name = "password_back.jpeg";  
        }

        const urlparam = '/KYC/RegisterPOA';
        let poagroup = {
            "POAType": this.documentType,
            "PhoneNumber": localStorage.getItem('phoneno'),
            "MerchantId": this.userDetails['MerchantId'],
            "OnBoardToken": this.userDetails['OnBoardToken'],
            "ImageList": [
              { //driving_back, voterid_back, passport_back
                "FileName": this.front_name,
                "FileContentBase64":  this.frontimage,
              },
              {
                "FileName": this.back_name,
                "FileContentBase64":   this.backimage,
              }
            ]
          };
        //alert(JSON.stringify(poagroup));
          this.showLodder('Uploading document details..');
          //his.showLodder('Uploadding pancard..')
          this.kycService.uploadAdharImage(poagroup).subscribe(result => {
        
          this.loadder.dismiss();
        
          localStorage.setItem('side','front');
          //alert(JSON.stringify(result));
          localStorage.setItem('adharDetails',JSON.stringify(result));
          // localStorage.setItem('frontimage',poagroup.ImageList[0]['FileContentBase64']);
          // localStorage.setItem('backimage',poagroup.ImageList[1]['FileContentBase64']);
          this.router.navigateByUrl(`poaform/${this.documentType}`);
            }, err => { 
              this.loadder.dismiss();
                this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
                  toast => {
                    console.log(toast);
                  }
                );
              
            }); 
        // this.registerservice.postCall(urlparam,poagroup,true).subscribe(result => {
        //   //  this.information = result;
        //   this.loadder.dismiss();
        //  this.router.navigateByUrl('poaform');
        // //  localStorage.setItem('side','front');
        //    alert(JSON.stringify(result));
        //    localStorage.setItem('adharDetails',JSON.stringify(result));
        //    localStorage.setItem('frontimage',poagroup.ImageList[0]['FileContentBase64']);
        //    localStorage.setItem('backimage',poagroup.ImageList[1]['FileContentBase64']);
          
        //   }, err => {
        //     localStorage.setItem('side','front');
        //     // this.router.navigateByUrl('poaform');
        //     // alert(err)
        //     this.loadder.dismiss();
        //       this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
        //         toast => {
        //           console.log(toast);
        //         }
        //       );
        //   }); 
          // this.router.navigateByUrl('poaform');
      }
      
      else{
        this.toast.show( 'Please upload an image.', '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      }
    //}
    // else {
    //     this.alertMessage('Error', 'Something went wrong ,please relogin after sometime')
    //     return false;
    // } 
   // this.router.navigateByUrl('poaform');
  }
  /**
 * Alert message
 */
  async alertMessage(header,message){
    let modal = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });
    await modal.present();
  }



   
 /**
     * 
     * SZ file upload
     * @param dirpath 
     * @param filename 
     * @param contentType 
     */
    uploadingFileToSZServer(dirpath, filename,contentType){
      let tempFileName = this.date.getTime() + '_' + filename;
      this.file.copyFile(dirpath, filename, this.file.dataDirectory, tempFileName).then(
          resss => {
              console.log(resss.nativeURL);
              let url = "https://persist.signzy.tech/api/files/upload";
              let parameter = new Object();
              parameter['ttl'] = this.ttl
              let options: FileUploadOptions = {
                  mimeType: (contentType == 'video') ? 'video/mp4': '',
                  params: parameter,
                  fileName: tempFileName,
              }
              this.transfer.create().upload(resss.nativeURL, url, options).then((data: any) => {
                  let returnData = JSON.parse(data.response)
                  console.log('fielupload success', data, returnData.file.directURL)
                  if(contentType == "video"){
                      this.fileContent.video = returnData.file.directURL;
                  } else if(contentType == "image"){
                      this.fileContent.image = returnData.file.directURL;
                  }
              }, (err) => {
                  console.log('file upload error', err)
                  // error
              });
          }, err => {
              console.log('file copy error', err)
          });
  }

}
 