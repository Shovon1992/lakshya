import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
//import { ImagePicker,ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { RegisterService } from './../services/register.service';
import { LoadingController, NavController } from '@ionic/angular';
import { Toast } from '@ionic-native/toast/ngx';
import { KycService } from '../services/kycService/kyc.service';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-uploadpan',
  templateUrl: './uploadpan.page.html',
  styleUrls: ['./uploadpan.page.scss'],
})
export class UploadpanPage implements OnInit {
  
 showpic: false;
 options: any;  uploadimg:any; baseimg:any;
 date = new Date();
  constructor(public router:  Router,public translate: TranslateService,
    public camera: Camera, public registerservice:RegisterService, public loadingCtrl: LoadingController,
    public toast: Toast, public kycService: KycService, public navCtrl : NavController , public file: File,public transfer: FileTransfer,public sanitizer: DomSanitizer,) {
     
     }

  ngOnInit() { 
    this.uploadimg = "/assets/images/pan-card.png"
    this.baseimg = ""
  }
  /** Old code  */
  /**getPic(){
    const options: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 50,
      width: 512,
      height: 150,
      outputType: 1
    }
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
      this.baseimg = results[0];
      this.uploadimg= 'data:image/jpeg;base64,' + results[0];
      localStorage.setItem('panimage',this.uploadimg);
      }
      }, (err) => {
      // console.log(‘err’ + err);
      });
  }
  capturePic(){
    const options: CameraOptions = {
      quality: 50,
      targetWidth: 512,
      targetHeight: 150,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
     this.baseimg=imageData;
     this.uploadimg='data:image/jpeg;base64,' + imageData;
    }, (err) => {
     // Handle error
    });
  }
  */
  /**
   * 
   * type should be 0 (gallary) 1 (camera)
   * Updated code by shovon  11.11.19 for uploading image or capture image 
   * 
   * */

   //For Loadder 
loadder: any;
async showLodder(message){
  this.loadder = await this.loadingCtrl.create(
    {
      message: message
    }
  );
this.loadder.present();
}


  getImage(type){
    const options: CameraOptions = { 
      quality: 100,
      targetWidth: 912,
      targetHeight: 950,
      destinationType: this.camera.DestinationType.FILE_URI,
      allowEdit: true,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: type,
      cameraDirection: this.camera.Direction.BACK
    }
    
    this.camera.getPicture(options).then((imageData) => {

      imageData = imageData.includes("?") ? imageData.substr(0, imageData.lastIndexOf('?')) : imageData;
      console.log('poa image data', imageData)
      var dirpath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
        this.showLodder('Uploading document details..');
        let filename = imageData.substr(imageData.lastIndexOf('/') + 1);
        let tempFileName = this.date.getTime() + '_' + filename;
        this.file.copyFile(dirpath, filename, this.file.dataDirectory, tempFileName).then(
          resss => {
              console.log(resss.nativeURL);
              let url = "https://persist.signzy.tech/api/files/upload";
              let parameter = new Object();
              parameter['ttl'] = "3 years"
              let options: FileUploadOptions = {
                  
                  params: parameter,
                  fileName: tempFileName,
              }
              this.transfer.create().upload(resss.nativeURL, url, options).then((data: any) => {
                  let returnData = JSON.parse(data.response)

                  this.loadder.dismiss();
                  console.log(data)
                  this.baseimg = returnData.file.directURL; 
                  this.uploadimg = this.sanitizer.bypassSecurityTrustUrl(returnData.file.directURL); 
                  
              }, (err) => {
                this.loadder.dismiss();
                
                this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
                  toast => {
                    console.log(err);
                  }
                );          
              });
          }, err => {
            this.loadder.dismiss();
            this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
              toast => {
                console.log(err);
              }
            );          
          });

      
     //this.baseimg=imageData;
     //this.uploadimg='data:image/jpeg;base64,' + imageData;
    //  this.showLodder('Uploading document details..');
    //  this.kycService.uploadSZImage(imageData).subscribe(result => {
    //   // let response_json = JSON.parse(result);
    //   this.loadder.dismiss();
    //   this.baseimg = result['file']['directURL']; 
    // }, err => { 
    //   this.loadder.dismiss();
    //     this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
    //       toast => {
    //         console.log(toast);
    //       }
    //     );
      
    // });
    }, (err) => {
     // Handle error
    }); 
  }
  // strToReplace.replace(/^data:image\/[a-z]+;base64,/, "")
  submitpan(){
      const urlparam = '';
      const pangroup = {
          "PAN": localStorage.getItem('panno'),
          "PhoneNumber": localStorage.getItem('phoneno'),
          "FileName":  localStorage.getItem('panno')+".jpeg",
          "FileContentBase64": this.baseimg,
        };

        localStorage.setItem('panimgDetails',JSON.stringify(pangroup));
        localStorage.setItem('panimg',this.baseimg)

      this.showLodder('Uploading pancard..')
      this.kycService.uploadPanImage(pangroup).subscribe(result => {
      this.loadder.dismiss();
      //  this.information = result;
      // this.router.navigateByUrl('panform');
      // const localresult =JSON.stringify(result)
      localStorage.setItem("pandetails",JSON.stringify(result)); //alert(JSON.stringify(result));
      this.navCtrl.navigateRoot('panform')
      //this.router.navigate(['panform']);
        
        }, err => { 
          this.loadder.dismiss();
            alert( 'Something weent worng pleasse try again.');
              
          
        }); 
       // this.router.navigateByUrl('panform'); 
  }
  pankyc(){

  }
}
