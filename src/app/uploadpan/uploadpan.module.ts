import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { UploadpanPage } from './uploadpan.page';

const routes: Routes = [
  {
    path: '',
    component: UploadpanPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,TranslateModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UploadpanPage]
})
export class UploadpanPageModule {}
