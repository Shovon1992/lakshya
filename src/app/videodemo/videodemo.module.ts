import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { VideodemoPage } from './videodemo.page';
import { CustomComponentModule } from '../customComponents/custom-component/custom-component.module';

const routes: Routes = [
  {
    path: '',
    component: VideodemoPage
  }
];

@NgModule({
  imports: [
    CommonModule,TranslateModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    CustomComponentModule,
  ],
  declarations: [VideodemoPage]
})
export class VideodemoPageModule {}
