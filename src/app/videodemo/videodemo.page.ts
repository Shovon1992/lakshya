import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from  "@angular/router";
import { NavController } from '@ionic/angular';
import { BaseComponent } from '../base/base.component';
@Component({
  selector: 'app-videodemo',
  templateUrl: './videodemo.page.html',
  styleUrls: ['./videodemo.page.scss'],
})
export class VideodemoPage extends BaseComponent implements OnInit {
  showvideo:Boolean = true;
  pageheader:String = '';
  language = localStorage.getItem('lang');
  vidoLink = 'https://www.youtube.com/embed/KDcqhf46Vbw';
  //constructor(private  router:  Router, private translate: TranslateService, public navCtrl : NavController) { }
  ngOnInit() {
    this.pageheader = this.router.getCurrentNavigation().extras.state.pageheader;
    this.checkVideo(this.language)
  }

  checkVideo(lang){
    this.language = lang
    switch(lang){
      case 'en':
        this.vidoLink = 'https://www.youtube.com/embed/KDcqhf46Vbw';
        break;
      case 'ka':
        this.vidoLink = 'https://www.youtube.com/embed/0DiQC9fxHfg';
        break;
      case 'hn':
        this.vidoLink = 'https://www.youtube.com/embed/zvrRziO-OcE';
        break;
      default:
          this.vidoLink = 'https://www.youtube.com/embed/KDcqhf46Vbw';
          break;
    }
  }
  skipvideo(){
  this.showvideo = false;
  }
  register(){
    this.router.navigateByUrl('login'); 
  }
  setGoal(){
    this.router.navigateByUrl('goaldescription'); 
  }
  
}
