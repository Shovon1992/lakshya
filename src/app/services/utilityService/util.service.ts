import { Injectable } from '@angular/core';
import { RootService } from '../root.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilService extends RootService{
  constructor(http: HttpClient){
    super(http);
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
  }

  /**
   * Get faq details
   */
  getFaq() {
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.get(this.base_url+'/'+this.faq_details, this.httpOptions);
  }

  /**
   * Get support details 
   */

  getSupportDetails(): Observable<any> {
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.get(this.base_url+'/'+this.support_details, this.httpOptions);
  }
/**
   * Get support details 
   */

  getVideoDetails(language): Observable<any> {
    let data = {
      language: language
    }
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.video_details, data, this.httpOptions);
  }
 
  /**
   * 
   */
  getInformationDetails(data){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.get_mismatch_info, data, this.httpOptions);
  }
  /** Get app version */
  getAppVersion(){
    return this.http.get(this.base_url+'/'+this.appVersion);
  }


  /** Change language */
  changeLanguage(language,userId){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    let data ={
      "userId": userId,
      "Lang": language
    }
    return this.http.post(this.base_url+'/'+this.languageChange, data, this.httpOptions);
  }
  /**
   * End of class
   */
}
