import { Injectable } from '@angular/core';
import { RootService } from '../root.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ICompleteWithdraw } from '../../interface/transation/icomplete-withdraw';
@Injectable({
  providedIn: 'root'
})
export class BankService extends RootService{
  constructor(http: HttpClient){
    super(http);
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
  }

  /**
   * For getting bank details according to ifc code  
   */
  getBankDetailsByIfc(ifcCode) {
    let data = { 
      IfscCode : ifcCode,
      PAN: null,
    }
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    console.log('Bearer '+localStorage.getItem('token'))
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.fetch_ifc, data, this.httpOptions);
  }


  /** 
   * For creating account 
   */
  createNewAcount(data){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.create_account, data, this.httpOptions);
  }

  /** 
   * For creating Folio 
   */
  createNewFolio(data){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.folio_creation, data, this.httpOptions);
  }

  /**
   * Send Folio OTP
   */
  sendFolioOtp(number){
    let data ={
      PhoneNumber : number
    }
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.send_folio_otp, data, this.httpOptions);
  }
/**
 * 
 *  verify Otp
 */
  verifyFolioOtp(data){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.verify_folio_otp, data, this.httpOptions);
  }

  /**
 * 
 *  verify withdraw Otp
 */
  verifyWithdrawOtp(data : ICompleteWithdraw){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.verify_withdraw_otp, data, this.httpOptions);
  }
  /**
   * End of class
   */
}
