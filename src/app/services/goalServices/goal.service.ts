import { Injectable } from '@angular/core';
import { RootService } from '../root.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { IcreateGoal } from 'src/app/interface/goal/icreate-goal';
import { IupdateGoal } from 'src/app/interface/goal/iupdate-goal';

@Injectable({
  providedIn: 'root'
})
export class GoalService extends RootService{
  constructor(http: HttpClient){
    super(http);
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
  }


  /**
   * Get the goal lists
   */
  getGoalLists(phone){
    let data = {
        "PhoneNumber": phone 
    }
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    console.log(this.base_url+'/'+this.get_goal_list,data, this.httpOptions)
    return this.http.post(this.base_url+'/'+this.get_goal_list,data, this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    );
  }
/**
 * Create new Goal
 */
createNewGoal(data : IcreateGoal){
  //console.log(this.httpOptions)
  this.headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  this.httpOptions = {
    headers: this.headers_object
  };
  return this.http.post(this.base_url+'/'+this.create_new_goal, data, this.httpOptions).pipe(
    timeout(this.timeOutLimit)
  );
}

/**
 * Update existing goal
 */
updateGoal(data : IupdateGoal){
  this.headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  this.httpOptions = {
    headers: this.headers_object
  };
  return this.http.post(this.base_url+'/'+this.update_goal,data, this.httpOptions).pipe(
    timeout(this.timeOutLimit)
  );
}
  /**
   * End of Class
   */
}
 