import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RootService {
  headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  httpOptions = {
    headers: this.headers_object
  };
  
  constructor(
    public http: HttpClient
  ) { 
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+ localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
  }
timeOutLimit = 1000*15; 

  base_url = 'https://lakshaywireless.com/LakshayApis'; //UAT
  //base_url = 'http://52.172.11.232:88/lakshayuat'; //UAT
  //base_url = 'http://localhost:59428/';  //lOCAL



  upload_pan_image = 'api/KYC/RegisterPOI';
  upload_adhar_image = 'api/KYC/RegisterPOA';
  upload_video_selfi = 'api/KYC/UploadVideoSelfie';
  fetch_ifc = 'api/KYC/IFSCVerification_Aadhar';
  create_account = 'api/KYC/CreateAccount';
  folio_creation = 'api/Folio/CreateFolio';
  faq_details = 'api/UserHelp/GetFaqDetails';
  support_details = 'api/UserHelp/GetSupportDetals';
  video_details = 'api/UserHelp/GetVideos';
  send_folio_otp = 'api/Folio/SendFolioOTP';
  verify_folio_otp = 'api/Folio/ValidateFolioOTP';
  get_mismatch_info = 'api/KYC/MatchNames';
  get_goal_list = 'api/Goal/GetGoalList';
  create_new_goal = 'api/Goal/CreateGoal'
  update_goal = 'api/Goal/EditGoal';

  /** For transation */
  get_bank_list = 'api/KYC/GetBankList';
  get_user_account_details = 'api/KYC/GetAccountDetails';
  submit_purchase = 'api/Transaction/SubmitPurchase';
  payment_mode_request = 'api/Transaction/PaymentModeRequest';
  get_purchase_request = 'api/Transaction/SubmitPurchase';
  get_paid_status = 'api/Transaction/GetPaidStatus';
  withdraw = 'api/Transaction/SubmitRedeem';

  get_transation_history = 'api/Transaction/GetTransactionDashboard';
  get_tax_status = 'api/Folio/GetTaxStatus';
  get_account_type = 'api/Folio/GetBankAccountType';

  verify_withdraw_otp = 'api/Transaction/TransactionOTP';

  deleteUpi = 'api/Transaction/DeleteUPIId';
  appVersion = 'api/User/GetAPKVersion';


  /** Esign */

  createPdf = 'api/KYC/CreateContractPDFURL';
  saveEsign = 'api/KYC/SaveAadhaarEsign';
  PANFetch = 'api/KYC/PANFetch';
  DLFetch = '/api/KYC/DLFetch';

  languageChange = '/api/User/ChangeUserLanguage';
  /**
   * End
   */
}
