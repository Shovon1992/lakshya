import { Injectable } from '@angular/core';
import { RootService } from '../root.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { timeout } from 'rxjs/operators';
import { IsubmitPurchase } from 'src/app/interface/transation/isubmit-purchase';
import { IwithdrawAmount } from 'src/app/interface/transation/iwithdraw-amount';
import { IdeleteUpi } from 'src/app/interface/transation/idelete-upi';

@Injectable({
  providedIn: 'root'
})
export class TransationService extends RootService{

  navigate : any;
  username: string;
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    
  userAccoutnDetails = JSON.parse(localStorage.getItem('userAccountDetails'));
  constructor(http: HttpClient){
    super(http);
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
  }

  /**
   * Get bank name by type
   * “NIIB” (for Internet Banking) OR “NIDC” (for Debit Card)
   */

getBankListByType(transationType){
  let data = {
    "PaymentMode" : transationType,
  }
  this.headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  this.httpOptions = {
    headers: this.headers_object
  };
  return this.http.post(this.base_url+'/'+this.get_bank_list,data,this.httpOptions).pipe(
    timeout(this.timeOutLimit)
  )
}

/**
   * Get users account details
   */

  getAccountDetailsByNumber(phone){
    let data = {
      "PhoneNumber" : phone,
    }
    console.log("phone");
    console.log(phone);
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });

    console.log("Tokan Details");
    console.log(this.headers_object);
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.get_user_account_details,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    )
  }

  /**
   * Get submit purchase
   */
 
  getSubmitPurchase(data : IsubmitPurchase){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.submit_purchase,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    )
  }


  /**
   * Request payment for paymeny mode 
   * @transationId will  get from api/Transaction/SubmitPurchase response
   */
  paymentModeRequest(transationId){
    let data = {
      TranId : transationId,
    }
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };

    return this.http.post(this.base_url+'/'+this.payment_mode_request,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    )
  }
  
  /**
   * get paid status
   * @transationId will  get from api/Transaction/SubmitPurchase response
   */
  getPaidStattus(transationId,PAN){
    let data = {
      TranId : transationId,
      PAN : PAN,
    }
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };

    return this.http.post(this.base_url+'/'+this.get_paid_status,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    )
  }
  
  /**
   * Request payment for paymeny mode 
   * @transationId will  get from api/Transaction/SubmitPurchase response
   */
  withdrawMoney(data: IwithdrawAmount){
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.withdraw,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    )
  }
  
/**
 * Get transation history
 */
  getTransationHistory(FolioNumber){
    let data = {
      FolioNumber : (FolioNumber) ? FolioNumber : this.userAccoutnDetails.FolioNumber,
    }
    console.log(data);
    console.log(localStorage.getItem('token'));
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    }); 
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.get_transation_history,data,this.httpOptions).pipe(
      timeout(this.timeOutLimit)
    )
  }

  /**
 * Get transation history
 */
getTaxStatus(){
  let data = {}
  console.log('tax...')
  this.headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  }); 
  this.httpOptions = {
    headers: this.headers_object
  };
  return this.http.post(this.base_url+'/'+this.get_tax_status,data,this.httpOptions).pipe(
    timeout(this.timeOutLimit)
  )
}

/**
 * Get transation history
 */
getAccountTypes(data){
  console.log('tax...')
  this.headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  }); 
  this.httpOptions = {
    headers: this.headers_object
  };
  return this.http.post(this.base_url+'/'+this.get_account_type,data,this.httpOptions).pipe(
    timeout(this.timeOutLimit)
  )
}


/**
 * Delete upi ids 
 */
deleteUPI(data: IdeleteUpi){
  this.headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  }); 
  this.httpOptions = {
    headers: this.headers_object
  };
  return this.http.post(this.base_url+'/'+this.deleteUpi,data,this.httpOptions).pipe(
    timeout(this.timeOutLimit)
  )
}
   /**
    * End of class
    */
}
 