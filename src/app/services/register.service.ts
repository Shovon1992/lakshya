import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient,HttpHeaders, HttpBackend } from '@angular/common/http';
//import { ImagePicker,ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
// Typescript custom enum for search types (optional)
export const Globalaccess ={
  selectedlang :localStorage.getItem('lang'),
  PhoneNumber: localStorage.getItem('phoneno'),
  Pin: localStorage.getItem('pin'),
  ReferralCode:localStorage.getItem('referalcode'),
  uuid:localStorage.getItem('uuid'),
  panno:localStorage.getItem('panno'),
  email: localStorage.getItem('email'),
}; 
@Injectable({
  providedIn: 'root'
})
export class RegisterService {
 url = 'https://lakshaywireless.com/LakshayApis/api';   
 //url = 'http://52.172.11.232:88/lakshayuat//api';  //NON PRD
  //url = 'http://localhost:59428/api';  //lOCAL


  apiKey = ''; 
  uploadimg:any;
  


 constructor(private handler: HttpBackend,private http: HttpClient,
  private camera: Camera) { }
  postCall(urlparam,loginGroup, page): Observable<any> {
    this.http = new HttpClient(this.handler);

    if(page){   
      const headers_object = new HttpHeaders({
        'Content-Type': 'application/json',
         'Authorization': 'Bearer '+localStorage.getItem('token')
      });
      const httpOptions = {
        headers: headers_object
      };
      return this.http.post(this.url+urlparam,loginGroup,httpOptions).pipe(
        map(results => results)
      );
    }

    if(!page ){
      const param= loginGroup;
      return this.http.post(this.url+urlparam,param).pipe(
        map(results => results)
      );
    }
    if(page === 'upload'){      
      const headers_object = new HttpHeaders({
        'Content-Type': 'application/json',
         'Authorization': 'Bearer '+localStorage.getItem('token')
      });
      const httpOptions = {
        headers: headers_object
      };
      //alert(loginGroup);
      return this.http.post(this.url+urlparam,loginGroup,httpOptions).pipe(
        map(results => results)
      );
    }    
  }

  getCall(urlparam,loginGroup, page): Observable<any> {
    if(page === 'getotp'){
      return this.http.get(this.url+urlparam+loginGroup.PhoneNumber,loginGroup.PhoneNumber).pipe(
        map(results => results)
      );
    }

    if(page === 'verifyotp'){
      return this.http.get(this.url+urlparam+loginGroup.PhoneNumber+'&Code='+loginGroup.Code,loginGroup).pipe(
        map(results => results)
      );
    }
    if(page === 'verifykyc'){
      const headers_object = new HttpHeaders({
        'Content-Type': 'application/json',
         'Authorization': 'Bearer '+localStorage.getItem('token')
      });
      const httpOptions = {
        headers: headers_object
      };
      return this.http.get(this.url+urlparam+loginGroup.PAN,httpOptions).pipe(
        map(results => results)
      );
    }  

    if(page ===  'videos'){
      const headers_object = new HttpHeaders({
        'Content-Type': 'application/json',
         'Authorization': 'Bearer '+localStorage.getItem('token')
      });
      const httpOptions = {
        headers: headers_object
      };
      
      return this.http.get(this.url+urlparam+loginGroup.language,httpOptions).pipe(
        map(results => results)
      );
    }
    
  }
  

}
