import { Injectable } from '@angular/core';
import { RootService } from '../root.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class KycService  extends RootService{
  constructor(http: HttpClient){
    super(http)
  }

  /**
   * Get faq details
   */
  uploadPanImage(pandata) {
   console.log(this.httpOptions, localStorage.getItem('token'));
   this.headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  this.httpOptions = {
    headers: this.headers_object
  };
    return this.http.post(this.base_url+'/'+this.upload_pan_image,pandata, this.httpOptions);
  }

  /**
   * Upload Image and get image of Signzy     * 
   */
  uploadSZImage(imageFile) {
    const uploadData = {
      'file': imageFile,
      'ttl': 'infinity',
    };
  
    let url = "https://persist.signzy.tech/api/files/upload"

    return this.http.post(url, uploadData);
  }

  uploadAdharImage(adharData) {
   // alert(JSON.stringify(adharData));
   this.headers_object = new HttpHeaders({
    'Content-Type': 'application/json',
     'Authorization': 'Bearer '+localStorage.getItem('token')
  });
  this.httpOptions = {
    headers: this.headers_object
  };
     return this.http.post(this.base_url+'/'+this.upload_adhar_image,adharData, this.httpOptions);
   }

   uploadVideoSelfi(videoData) {
    //alert(JSON.stringify(videoData));
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
      return this.http.post(this.base_url+'/'+this.upload_video_selfi,videoData, this.httpOptions);
    }



    uploadSZVideoSelfi(videoData) {
      //alert(JSON.stringify(videoData));

      const uploadData = {
        'file': videoData ,
        'ttl': '1 month',
    };
    this.headers_object = new HttpHeaders({
      'enctype': 'multipart/form-data; boundary=WebAppBoundary',
      'Content-Type': 'multipart/form-data; boundary=WebAppBoundary',
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    
    console.log(uploadData);
      let url = "https://persist.signzy.tech/api/files/upload"
        return this.http.post(url, uploadData);
    }

    
    /** get /api/KYC/CreateContractPDFURL */
    createPdfUrl(userId) {
      
      const data = {
        'UserId': userId,
      };
      this.headers_object = new HttpHeaders({
        'Content-Type': 'application/json',
         'Authorization': 'Bearer '+localStorage.getItem('token')
      });
      this.httpOptions = {
        headers: this.headers_object
      };
      return this.http.post(this.base_url+'/'+this.createPdf,data, this.httpOptions);

    }

    /** get /api/KYC/CreateContractPDFURL */
    saveEsing(userId) {
      
      const data = {
        'UserId': userId,
      };
      this.headers_object = new HttpHeaders({
        'Content-Type': 'application/json',
         'Authorization': 'Bearer '+localStorage.getItem('token')
      });
      this.httpOptions = {
        headers: this.headers_object
      };
      return this.http.post(this.base_url+'/'+this.saveEsign,data, this.httpOptions);

    }


  /**   PANFetch  */    

  panFetch(pan) {
      
    const data = {
      'PAN': pan,
    };
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.PANFetch,data, this.httpOptions);

  }

  fetchDL(dl, dob){
    const data = {
        "DL": dl,
        "DOBDL": dob
    };
    this.headers_object = new HttpHeaders({
      'Content-Type': 'application/json',
       'Authorization': 'Bearer '+localStorage.getItem('token')
    });
    this.httpOptions = {
      headers: this.headers_object
    };
    return this.http.post(this.base_url+'/'+this.DLFetch,data, this.httpOptions);
  }
   /**
    * End of class
    */
}
 