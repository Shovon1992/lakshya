import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
//import { ImagePicker,ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { RegisterService } from './../services/register.service';
import { LoadingController } from '@ionic/angular';
import { BaseComponent } from '../base/base.component';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
@Component({
  selector: 'app-uploadsign',
  templateUrl: './uploadsign.page.html',
  styleUrls: ['./uploadsign.page.scss'],
})
export class UploadsignPage extends BaseComponent implements OnInit {
  uploadimg: any = "./assets/images/signature.png";
  baseImage: any;
  // constructor(private  router:  Router,private translate: TranslateService,
  //   private registerservice:RegisterService,
  //   public loadingCtrl : LoadingController,
  //   private imagePicker: ImagePicker,
  //   private camera: Camera) { }
  ngOnInit() {}
  /*
    getPic(){
      const options: ImagePickerOptions = {
        maximumImagesCount: 1,
        quality: 50,
        width: 512,
        height: 150,
        outputType: 1
      }
      this.imagePicker.getPictures(options).then((results) => {
        for (var i = 0; i < results.length; i++) {
          this.uploadimg= 'data:image/jpeg;base64,' + results[0];
        this.router.navigate(['docconfirmation'], { state: { page: 'uploadsign',uploadimg: this.uploadimg } });
        }
        }, (err) => {
        // console.log(‘err’ + err);
        });
     // this.router.navigate(['docconfirmation'], { state: { page: 'uploadpan' } });
    }
    capturePic(){
      const options: CameraOptions = {
        quality: 100,
        targetWidth: 512,
        targetHeight: 150,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
        this.uploadimg='data:image/jpeg;base64,' + imageData;
        this.router.navigate(['docconfirmation'], { state: { page: 'uploadsign',uploadimg: this.uploadimg } });
       }, (err) => {
        // Handle error
       });
    }*/
  //For Loadder 
  //  loadder: any;
  //  async showLodder(message){
  //    this.loadder = await this.loadingCtrl.create(
  //      {
  //        message: message
  //      }
  //    );
  //  this.loadder.present();
  //  }
  capturePic(type) {
      const options: CameraOptions = {
          quality: 100,
          targetWidth: 912,
          targetHeight: 950,
          destinationType: this.camera.DestinationType.FILE_URI,
          allowEdit: true,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          sourceType: type,
          cameraDirection: this.camera.Direction.BACK
      }
      this.camera.getPicture(options).then((imageData) => {
        imageData = imageData.includes("?") ? imageData.substr(0, imageData.lastIndexOf('?')) : imageData;
        console.log('poa image data', imageData)
        var dirpath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
          this.showLodder('Uploading Signature');
          let filename = imageData.substr(imageData.lastIndexOf('/') + 1);
          let tempFileName = this.date.getTime() + '_' + filename;
          this.file.copyFile(dirpath, filename, this.file.dataDirectory, tempFileName).then(
            resss => {
                console.log(resss.nativeURL);
                let url = "https://persist.signzy.tech/api/files/upload";
                let parameter = new Object();
                parameter['ttl'] = this.ttl
                let options: FileUploadOptions = {
                    
                    params: parameter,
                    fileName: tempFileName,
                }
                this.transfer.create().upload(resss.nativeURL, url, options).then((data: any) => {
                    let returnData = JSON.parse(data.response)
  
                    this.loadder.dismiss();
                    console.log(data)
                    this.baseImage = returnData.file.directURL;
                    this.uploadimg  = this.sanitizer.bypassSecurityTrustUrl(returnData.file.directURL);
                    
                }, (err) => {
                  this.loadder.dismiss();
                  
                  this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
                    toast => {
                      console.log(err);
                    }
                  );          
                });
            }, err => {
              this.loadder.dismiss();
              this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
                toast => {
                  console.log(err);
                }
              );          
            });
         
      });
  }
  submitpan() {
      this.router.navigateByUrl('panform');
  }
  uploadsign() {
    // if (this.currentUserDetails['MerchantId'] !== null) {              
      const urlparam = '/KYC/UploadSignature';
      const signgroup = {
          "PhoneNumber": localStorage.getItem('phoneno'),
          "MerchantId": this.currentUserDetails['MerchantId'],
          "OnBoardToken": this.currentUserDetails['OnBoardToken'],
          "FileName": "demo_sign.jpeg",
          "FileContentBase64": this.baseImage
      }
      //alert(JSON.stringify(signgroup));
      this.showLodder('Uploading Signature..');
      this.registerservice.postCall(urlparam, signgroup, true).subscribe(result => {
          //  this.information = result;
          this.hideLoader();
          //alert(JSON.stringify(result));
          if (result.Success) {
              this.currentUserDetails.Status = 5;
              localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails));
              this.router.navigateByUrl('videoselfie');
          } else {
              this.alertMessage('Error !!', result['ExtendedMessage'])
          }
      }, err => {
          //this.router.navigateByUrl('videoselfie');
          //alert(JSON.stringify(err));
          this.hideLoader();
      });      
    // }
    // else {
    //     this.alertMessage('Error', 'Something went wrong ,please relogin after sometime')
    //     return false;
    // } 
  }
}
