
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DashboardPage } from './dashboard.page';
import { ListPage } from './../list/list.page';
const routes: Routes = [
  {
    path: '',
    redirectTo: '/menu/home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: DashboardPage,
    children: [
      {
        path: 'home',
        loadChildren: '../tabs/home/home.module#HomePageModule'
      },
      {
        path: 'list',
        loadChildren: './../list/list.module#ListPageModule'
      },

      {
        path: 'schedule',
        loadChildren: './../list/list.module#ListPageModule'
      },
      { path: 'qrscan',
      loadChildren: './../qrscanner/qrscanner.module#QrscannerPageModule'
    }]
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ DashboardPage ]
})
export class DashboardPageModule {}