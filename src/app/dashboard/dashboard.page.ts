import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  pages = [
    {
      title: 'home',
      url: '/menu/home',
      icon: 'home'
    },
    {
      title: 'list',
      url: '/menu/list',
      icon: 'list'
      // children: [
      //   {
      //     title: 'Ionic',
      //     url: '/menu/ionic',
      //     icon: 'logo-ionic'
      //   },
      //   {
      //     title: 'Flutter',
      //     url: '/menu/flutter',
      //     icon: 'logo-google'
      //   },
      // ]
    },
    {
      title: 'scanner',
      url: '/menu/qrscan',
      icon: 'qr-scanner'
    },
    
  ];

  constructor(private translate: TranslateService,) { 
  }

  ngOnInit() {
  }

}
