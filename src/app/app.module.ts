import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { OtploginPageModule } from './otplogin/otplogin.module';
import { AppRoutingModule } from './app-routing.module';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader} from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';
import { MediaCapture} from '@ionic-native/media-capture/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { DeviceAccounts } from '@ionic-native/device-accounts/ngx';
import { Sim } from '@ionic-native/sim/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { HTTP } from '@ionic-native/http/ngx';
// import { DeviceAccounts } from '@ionic-native/device-accounts';
import {ModalpagePageModule} from './verifykyc/modalpage/modalpage.module';
import { RegisterService } from './services/register.service';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
//import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Toast } from '@ionic-native/toast/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { NgxImageCompressService } from 'ngx-image-compress';
import { CustomComponentModule } from './customComponents/custom-component/custom-component.module';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { BaseComponent } from './base/base.component';
import { VideoEditor } from '@ionic-native/video-editor/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
//import { BlobModule } from 'angular-azure-blob-service';
import { AddnewgoalPageModule } from './pages/goal/addnewgoal/addnewgoal.module';
import { Covid19PageModule } from './tabs/home/covid19/covid19.module';
import { PassbookFilterPageModule } from './tabs/transation-history/passbook-filter/passbook-filter.module';
import { Interceptor } from './interceptors/interceptor';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
//import { CodePush } from '@ionic-native/code-push/ngx';
import { Market } from '@ionic-native/market/ngx';
import { EsignModalPageModule } from './pages/kyc/esign-modal/esign-modal.module';
import { WebIntent } from '@ionic-native/web-intent/ngx';
import { BlobModule, BlobService } from 'angular-azure-blob-service';
import { DatePipe } from '@angular/common';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');

}
@NgModule({
  declarations: [AppComponent, BaseComponent],
  entryComponents: [],
  imports: [
    FormsModule,
    
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    //BlobModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    BlobModule,
    OtploginPageModule,
    ModalpagePageModule,
    AddnewgoalPageModule,
    Covid19PageModule,
    CustomComponentModule,
    PassbookFilterPageModule,
    EsignModalPageModule,
    
  ],
  providers: [
    StatusBar,
    DatePipe,
    //BarcodeScanner,
    WebIntent,
    LocalNotifications,
    SplashScreen,GoogleAnalytics,Sim,AndroidPermissions,HTTP,Toast,Base64,File,NgxImageCompressService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true},
    CallNumber,FileTransfer,FileOpener,DocumentViewer,SocialSharing,
    BlobService,
    QRScanner,MediaCapture,Camera,DeviceAccounts,UniqueDeviceID,RegisterService,Calendar, VideoEditor, InAppBrowser, Market
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}




