import { Component, NgZone, OnInit } from '@angular/core';
import { NavController, LoadingController, AlertController, ModalController,MenuController, ActionSheetController } from '@ionic/angular';
import { BankService } from '../services/bankService/bank.service';
import { Platform } from '@ionic/angular';
import { RegisterService } from '../services/register.service';
import { UtilService } from '../services/utilityService/util.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Events } from '@ionic/angular';
import { LanguageService } from '../services/language.service';
import { FormBuilder } from '@angular/forms';
//import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { MediaCapture } from '@ionic-native/media-capture/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { File } from '@ionic-native/file/ngx';
import { GoalService } from '../services/goalServices/goal.service';
import { TransationService } from '../services/transationService/transation.service';
import { HttpClient } from '@angular/common/http';
//import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { KycService } from '../services/kycService/kyc.service';
import { VideoEditor } from '@ionic-native/video-editor/ngx';
import { VideoPlayer } from '@ionic-native/video-player';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Toast } from '@ionic-native/toast/ngx';
//import { Storage } from '@ionic/storage';
// import { LocalNotifications } from '@ionic-native/local-notifications';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { WebIntent } from '@ionic-native/web-intent/ngx';
//import { CodePush } from '@ionic-native/code-push/ngx';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { BlobService } from 'angular-azure-blob-service';
import { DatePipe } from '@angular/common';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
})
export class BaseComponent implements OnInit {

  constructor(
    //public codePush: CodePush,
    public navCtrl : NavController,
    public menuCtrl: MenuController,
    public bankService : BankService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public registerservice: RegisterService,
    public utilService: UtilService,
    public callNumber: CallNumber,
    public sanitizer: DomSanitizer,
    public translate: TranslateService,
    public router: Router,
    public languageService : LanguageService,
    public formBuilder : FormBuilder,
    public camera: Camera, 
    //public imagePicker: ImagePicker,
    public mediaCapture: MediaCapture,
    public base64: Base64,
    public file: File,
    public events: Events,
    public goalService: GoalService,
    public transationService: TransationService,
    public http : HttpClient,
    public kycService: KycService,
    //public webview : WebView
    public calendar : Calendar,
    public videoEditor: VideoEditor,
    public inAppBrowser : InAppBrowser,
    public activateRoute: ActivatedRoute,
    public androidPermissions: AndroidPermissions,
    public transfer: FileTransfer,
    public localNotifications: LocalNotifications,
    // public iLocalNotifyEvery: ILocalNotificationEvery,
    // public iLocalNotifyTrigger: ILocalNotificationTrigger,
    public alertController : AlertController,
    public modalController : ModalController,
    public toast: Toast,
    public platform: Platform,
    public ft: FileTransfer, 
    public fileOpener: FileOpener,
    public document: DocumentViewer,
    public socialSharing: SocialSharing,
    //public sqlStorage : Storage,
    public webIntent: WebIntent,
    public blob : BlobService,
    public zone: NgZone,
    public actionSheetController: ActionSheetController,
    public qrScanner: QRScanner,
    //public barcodeScanner: BarcodeScanner
    public datePipe : DatePipe,
    ) { 
      this.menuCtrl.enable(true);
    } 
  userImage: any = "https://lakresourcevmdiag.blob.core.windows.net/selfiimage/'+this.currentUserDetails.UserId+'.jpg";
  ngOnInit() {
    this.events.subscribe('userLogged', (data) => {
      this.userImage = (data['profileImage']) ? data['profileImage'] :'https://lakresourcevmdiag.blob.core.windows.net/selfiimage/'+data['UserId']+'.jpg?random+\=' +  new Date();    

    });

    //this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    // this.getBankAccountDetailsOfUser();
    // if(this.userAccountDetails){
    //   this.getBankAccountDetailsOfUser();
    // }
  }
ttl: String = "3 years";
currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
transationDetails : any =[];
currentSavings = 0;
bankDetails: any = [];
date = new Date();
userAccountDetails: any = [];
taxData: any = [];
isLoading = false;

langText: any;   
upiIds : any = [];
/**
   * For loadding controller
   */
  loadder: any;
   async showLodder(message){
     this.loadder = await this.loadingCtrl.create(
       {
         message: message,
         duration: 180000
       }
     );
   this.loadder.present();
   }
/**
 * Hide Loader
 */
hideLoader(){
  this.loadder.dismiss();
}
/**
 * Alert message
 */
async alertMessage(header,message){
  let modal = await this.alertCtrl.create({
    header: header,
    message: message,
    buttons: ['OK']
  });
  await modal.present();
}

async loadingPresent() {
  this.isLoading = true;
  return await this.loadingCtrl.create({
    message: 'Please wait ...',
    spinner: 'circles',    
    duration: 30000 
  }).then(a => {
    a.present().then(() => {
      console.log('loading presented');
      if (!this.isLoading) {
        a.dismiss().then(() => console.log('abort laoding'));
      }
    });
  });
}

async loadingDismiss() {
  this.isLoading = false;
  return await this.loadingCtrl.dismiss().then(() => console.log('loading dismissed'));
}

async present(message,duration) {
  this.isLoading = true;
  return await this.loadingCtrl.create({
    duration: 10000,
    message: message,
  }).then(a => {
    a.present().then(() => {
      console.log('presented');
      console.log(message);
      if (!this.isLoading) {
        a.dismiss().then(() => console.log('abort presenting'));
      }
    });
  });
}
async dismiss() {
    this.isLoading = false;
    await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
}
      


/**
 * Open phone call
 */
phoneCall(number){
  this.callNumber.callNumber(number, true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
}


/**
 * 
 * For safe url for video or images
 */
getSafeUrl(url){
  return this.sanitizer.bypassSecurityTrustResourceUrl(url);
}


/**
 * Create Folio
 */
createFolio(){
  try {
    this.showLodder('Creating folio..');
     let accountDetails = JSON.parse(localStorage.getItem('tempAccountDetails'));
    this.bankService.createNewFolio(accountDetails).subscribe(
      res =>{
        this.hideLoader();
        /*
        *On success populate data to form
        */
       console.log(res)
        if(res['Success']){
         
        }else{
          this.alertMessage('Error','Unable to create Folio.');
        }
      }, err =>{
        this.hideLoader();
        this.alertMessage('Error','Unable to create Folio.');
      }
    )
  } catch (error) {
    this.alertMessage('Error','Unable to create Folio.');
  }
  
}

/**
 * Send folio OTP
 */
sendFolioOtp(number){
  try {
    this.showLodder('Sending otp to your registered mobile number..');
    this.bankService.sendFolioOtp(number).subscribe(
      res =>{
        this.hideLoader();
        /*
        *On success populate data to form
        */
       console.log(res)
       this.navCtrl.navigateForward('verify-folio-otp');
       localStorage.setItem('folioOtpDetails',JSON.stringify(res));
        if(res['Success']){
         this.navCtrl.navigateForward('verify-folio-otp');
        }else{
          this.alertMessage('Error','Unable to create Folio.');
        }
      }, err =>{
        this.hideLoader();
        this.alertMessage('Error','Unable to create Folio.');
      }
    )
  } catch (error) {
    this.alertMessage('Error','Unable to create Folio.');
  }
}
goToNextInput(charCode, currentStage){
    
  let next = 'input'+parseInt(currentStage+1);
  console.log(charCode.length)

  if (charCode > 31 && (charCode < 48 || charCode > 57) ) {
    return false;
}else {
 if(charCode.length == 0 ){
document.getElementById(next).querySelector('input').focus();
return true;
 }
}
}


/**
 * Get bank details by payment type accordingly
 */
getBankDetailsByType(type){
  try {
    this.showLodder('Getting list of banks..');
       this.transationService.getBankListByType(type).subscribe(
         res => {
           this.hideLoader();
           if(res['Success']){
             this.bankDetails = res['PurchaseBankList'];
             
           } else {
             this.alertMessage('Error', res['ExtendedMessage']);
           }         
         }, err =>{
          this.hideLoader();   
           this.alertMessage('Error', 'Server issue occured, please try again later')
         }
       )
  } catch (error) {
    this.alertMessage('Error','Sorry not able to fetch bank details')
  }
}

/**
 * Get bank details by payment type accordingly
 */
getBankAccountDetailsOfUser(){
  try {
      // this.showLodder('Getting list of banks..');
       this.transationService.getAccountDetailsByNumber(this.currentUserDetails.PhoneNumber).subscribe(
         res => {
           //this.hideLoader();
           if(res['Success']){
             this.userAccountDetails = res['Account'];
             localStorage.setItem('userAccountDetails', JSON.stringify(res['Account']))

             this.getUpiIdList(this.userAccountDetails['UPIID']);


             this.getTransationDetails();
           } else {
            // this.alertMessage('Error', res['ErrorMessage']);
           }         
         }, err =>{
          this.hideLoader();   
           this.alertMessage('Error', 'Server issue occured, please try again later')
         }
       )
  } catch (error) {
    this.alertMessage('Error','Sorry not able to fetch bank details')
  }
}


/**
 * Get bank details by payment type accordingly
 */
getTransationDetails(){
  this.transationService.getTransationHistory(this.userAccountDetails.FolioNumber).subscribe(
    res =>{
      //this.hideLoader();
      //console.log(res);
      if(res['Success']){
        this.transationDetails = res;
        localStorage.setItem('transationDetails', JSON.stringify(res))
        //console.log(this.transationDetails)
        //this.checkInvestWithdraw();
        //this.checkCurrentSavings();
      }else{
        this.alertMessage('Error',res['ExtendedMessage']);
      }
    }, err =>{
      //this.hideLoader();
      this.alertMessage('Error','Unable fetch faq details.');
    }
  )
}
/**
 * Get Safe url
 */

getSafeURL(url) {
  let showUrl = url.replace('watch','embed')
  let returnurl =  this.sanitizer.bypassSecurityTrustResourceUrl(showUrl);
  //console.log(returnurl);
  return returnurl;
}


/**
 * c
 */

userLogin() {
  let data = {
    PhoneNumber: localStorage.getItem('phoneno'),
    Pin: localStorage.getItem('pin'),
  }
  const urlparam = '/Authentication/Login';
  if (localStorage.getItem('phoneno') && localStorage.getItem('pin')) {
    if (data.PhoneNumber.toString().length == 10) {
      try {
        this.showLodder('Loading...')
          this.registerservice.postCall(urlparam, data, false).subscribe(result => {
              if (result['TokenResponse'] && result['TokenResponse']['Token']) {
                  const token = result['TokenResponse']['Token'];
                  localStorage.setItem('token', token);
                  localStorage.setItem('userDetails', JSON.stringify(result))
                  //this.languageService.setLanguage('en');
                  console.log(result['Status'])
                  setTimeout(() => {
                      if (parseInt(result['Status']) < 3) {
                          this.router.navigateByUrl('verifykyc');
                      } else if (parseInt(result['Status']) > 2 && parseInt(result['Status']) < 7) {
                          this.router.navigateByUrl('kyc-complete-status');
                      } else {
                          this.router.navigateByUrl('homeTab');
                      }
                  }, 3000);
                } else {
                  this.router.navigateByUrl('otplogin');				
                }
              }, (err) => {
                this.router.navigateByUrl('otplogin');				
            });  
        } catch (error) {
          this.router.navigateByUrl('otplogin');				          
        }
      } else {
        this.router.navigateByUrl('otplogin');				
      }      
    } else {  
      this.router.navigateByUrl('otplogin');				
    }
  }



/**
 * Custom alert
 */

 async customCnfAlert(message,page){
  const alert = await this.alertCtrl.create({
    header: 'Confirm!',
    message: message,
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Okay',
        handler: () => {
          this.navCtrl.navigateRoot(page);
        }
      }
    ]
  });

  await alert.present();
 }


 /**
  * Get tax Status
  */
 getAllTaxStatus(){
   console.log('hii')
   try {
     this.transationService.getTaxStatus().subscribe(
       res =>{
         if(res['Success']){
           this.taxData = res['TaxStatus'];
         }else{
          this.alertMessage('Error', (res['ExtendedMessage']) ? res['ExtendedMessage'] :
          'Something weent worng pleasse try again')
         }
       }
     );
   } catch (error) {
    this.alertMessage('Error','Something weent worng pleasse try again')
   }
 }

getUpiIdList(ids){
  
  let tempIds = ids.split(',')
  this.upiIds = [];
  console.log(tempIds)
  tempIds.forEach(tempElement => {
      let counter = 0;
      console.log(tempElement)
      this.upiIds.forEach(element => {
        if( element.upiId == tempElement){
          counter++;
        }
      });

      if(counter == 0){
        this.upiIds.push(
          {
            type : tempElement.split('@')[1],
            upiId : tempElement
          }
        )
      }
  });
  console.log('UPI IDS: ', this.upiIds)
}

openWhatsapp() {
  //console.log('hiiidkjfdskjhfkjsdfk0-----000-0')
  window.open("https://api.whatsapp.com/send?phone=917019782863&text=Hello","_system");
}


/**
 * Upload fiile to sz
 */

/** get international text */
getInternationalText(key,subkey){
  console.log(key,subkey)
  this.translate.get(key).subscribe(
    res =>{
      console.log(res, res[subkey])
      this.langText = res[subkey];
      return res[subkey]
    }
  )
}


/** Upi intentations  */
upiIntent() {

  const options= {
  action:this.webIntent.ACTION_VIEW,
  
  url:"upi://pay?pa=icicipruamc1@icici&pn=priyadarshani&tid=&tr=MNO14959016&am=1&cu=INR&tn=&mc=6012&cu=INR",
  }
  this.webIntent.startActivityForResult(options).then(
  onSuccess=> {
  console.log("Success", onSuccess);
  //this.toastService.toastBottom("Payment Successfully done.");
  },
  onError=> {
  console.log("error", onError);
  });
}

/**
 * End of class
 */
}
