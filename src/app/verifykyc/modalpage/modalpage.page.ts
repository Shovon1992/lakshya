import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterService } from './../../services/register.service';
import { ModalController, NavParams,AlertController,NavController } from '@ionic/angular';

@Component({
  selector: 'app-modalpage',
  templateUrl: './modalpage.page.html',
  styleUrls: ['./modalpage.page.scss']
})
export class ModalpagePage implements OnInit{
  public modalGroup : FormGroup;
  userDetails = JSON.parse(localStorage.getItem('userDetails'))
  constructor(public  router:  Router,
    public navCtrl : NavController,public translate: TranslateService, public registerservice:RegisterService,
    public formBuilder: FormBuilder, public alertCtrl: AlertController, public modalCrtl: ModalController,public navParam: NavParams) {
      const panno= localStorage.getItem('panno');
      this.modalGroup = this.formBuilder.group({
        PAN: panno
      });
   }

  pageFor: any; 
  meaasge: any;
  ngOnInit(){
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'))
    this.pageFor = this.navParam.get('type');
    this.meaasge = this.navParam.get('message');
    console.log(this.pageFor);
    
  }
  startkyc(){
    if (this.modalGroup.value['PAN'].toString().length != 10) {
      this.alertMessage('Error', 'Please enter valid PAN number.');
    } 
    else {
      this.router.navigateByUrl('startkyc');
    }    
  }
  startTransaction() {    
    console.log("-------------------------");
    // this.navCtrl.navigateRoot('homeTab');
    // this.userDetails = JSON.parse(localStorage.getItem('userDetails'))
    this.userDetails.Status = 7;
    localStorage.setItem('userDetails', JSON.stringify(this.userDetails));
    this.navCtrl.navigateRoot('masterpage');  
    // this.navCtrl.navigateRoot('kyc-complete-status');
    console.log("++++++++++++++++++++++++++");
  }

  async alertMessage(header,message){
    let modal = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });
    await modal.present();
  }
}
