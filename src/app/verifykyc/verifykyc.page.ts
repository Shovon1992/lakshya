import { Component,OnInit} from '@angular/core';
import { Router} from "@angular/router";
import { ModalController, LoadingController, NavController} from '@ionic/angular';
import { ModalpagePage} from './modalpage/modalpage.page';
import { TranslateService } from '@ngx-translate/core';
import { Validators, FormBuilder,FormGroup} from '@angular/forms';
import {RegisterService} from './../services/register.service';
import { Toast } from '@ionic-native/toast/ngx';
import { BaseComponent } from '../base/base.component';
@Component({
  selector: 'app-verifykyc',
  templateUrl: './verifykyc.page.html',
  styleUrls: ['./verifykyc.page.scss'],
})
export class VerifykycPage extends BaseComponent implements OnInit {
  userDetails = JSON.parse(localStorage.getItem('userDetails'))
  public verifyGroup : FormGroup;
  btnStatus = false;
  username: string = '';
  mobileno: any = '';
  emailid: any = '';
  panRegx = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');


  ngOnInit() {
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'))
    this.verifyGroup = this.formBuilder.group({
      PAN: ['',Validators.required],
    });
    document.addEventListener("backbutton",function(e) {
        console.log("disable back button")
    }, false);
  }  
 

  ionViewDidEnter() {  
    
    this.userDetails = JSON.parse(localStorage.getItem('userDetails'))
    
    //enable menu
    if (this.userDetails.Status < 4) {
      this.menuCtrl.enable(true);
    
      if (this.userDetails.POIInformation !== null) {
        this.username = this.userDetails['POIInformation']['Name'];
      }
      this.mobileno = this.userDetails['PhoneNumber'];
      this.emailid = this.userDetails['EmailId'];
      let data = {
        'name': this.username, 
        'mobileno': this.mobileno, 
        'emailid': this.emailid, 
        'UserId' : this.currentUserDetails.UserId,
        
      };
      this.events.publish('userLogged', data);    
    }
    document.addEventListener("backbutton",function(e) {
      console.log("disable back button")
    }, false);
  }

  /**
   * Validate PAN
   */

  showModal(){
    const urlparam = '/KYC/CheckPOIRegistration?PAN=';

    // if(!this.verifyGroup.value.PAN || regpan.test(this.verifyGroup.value.PAN)){
    // if(!this.verifyGroup.value.PAN || regpan.test(this.verifyGroup.value.PAN)){
     // console.log("<<<<<<<<<<>>>>>>>"+this.verifyGroup.value['PAN']);
     // console.log("<<<<<<<<<<>>>>>>>"+this.panRegx.test(this.verifyGroup.value['PAN']));

      if (!this.panRegx.test(this.verifyGroup.value['PAN'])) {
    //  if (this.panRegx.test(this.verifyGroup.value['PAN'].toString)) {
      this.alertMessage('Error', 'Please enter valid PAN number.');
    } else if(this.verifyGroup.valid){
      this.present('Validating PAN details..', 5000);
      
      this.registerservice.postCall(urlparam,this.verifyGroup.value,true).subscribe(result => {
        this.dismiss();    
        
        if(result.Success){      
          this.presentModal('panSuccess',(result['ExtendedMessage'])?result['ExtendedMessage']:result['ErrorMessage']);
        } else{          
          if( result['ExtendedMessage'] == 'KYC has been completed by ICICI'){
            //this.customCnfAlert('KYC has been completed by ICICI','homeTab');
            this.presentModal('iciciSuccess',(result['ExtendedMessage'])?result['ExtendedMessage']:result['ErrorMessage']);
          } else {
            this.presentModal('panFailed',(result['ExtendedMessage'])?result['ExtendedMessage']:result['ErrorMessage']);
          }
        }
      }, err => {
        this.toast.show( 'Something weent worng pleasse try again.', '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      });
    } else{
      this.toast.show('Enter pan number', '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    }
  }

  /**
   * For 
   */
  async presentModal(type,message) {
    localStorage.setItem('panno',this.verifyGroup.get('PAN').value);
    
    const modal = await this.modalController.create({
      component: ModalpagePage,
      componentProps: {
        type: type,
        message: message
      },
      cssClass: 'my-custom-modal-css',
      backdropDismiss: false
    });
    return await modal.present();
  }
  ionViewWillLeave(){
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
