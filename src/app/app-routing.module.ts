import { NgModule } from '@angular/core'; 
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginGurdGuard } from './gurd/login-gurd.guard';

const routes: Routes = [
  { path: '',redirectTo: 'masterpage',pathMatch: 'full'},
  { path: 'passbookTab', redirectTo:'/homeTab/tabs/passbook',pathMatch: 'full' },
  { path: 'soa', loadChildren: './tabs/soa/soa.module#SoaPageModule' },
  { path: 'faq', loadChildren: './tabs/faq/faq.module#FaqPageModule' },
  { path: 'soaTab', redirectTo:'/homeTab/tabs/soa',pathMatch: 'full' },
  // { path: 'faqTab', redirectTo:'/homeTab/tabs/faq',pathMatch: 'full' },
  { path: 'otplogin', loadChildren: './otplogin/otplogin.module#OtploginPageModule' },
  { path: 'homeTab',loadChildren: './tabs/tab/tab.module#TabPageModule'},
  { path: 'goldTab',loadChildren: './pages/gold-tabs/gold-tabs.module#GoldTabsPageModule'},

  { path: 'masterpage', canActivate: [LoginGurdGuard], loadChildren: './pages/masterpage/masterpage.module#MasterpagePageModule' },

  { path: 'login',loadChildren:  './auth/login/login.module#LoginPageModule'},
  { path: 'dashboard', redirectTo: 'menu', pathMatch: 'full' },
  { path: 'menu', loadChildren:  './dashboard/dashboard.module#DashboardPageModule'},
  // { path: 'homeTab',loadChildren: './tabs/tab/tab.module#TabPageModule'},
  { path: 'list',loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)},
  { path: 'register', loadChildren: './auth/register/register.module#RegisterPageModule' },
  { path: 'language-popover', loadChildren: './language-popover/language-popover.module#LanguagePopoverPageModule' },
  { path: 'videodemo', loadChildren: './videodemo/videodemo.module#VideodemoPageModule' },
  { path: 'otpverfication', loadChildren: './auth/otpverfication/otpverfication.module#OtpverficationPageModule' },
  { path: 'registeremail', loadChildren: './auth/registeremail/registeremail.module#RegisteremailPageModule' },
  { path: 'emailpage', loadChildren: './auth/emailpage/emailpage.module#EmailpagePageModule' },
  { path: 'emaillogin', loadChildren: './auth/emaillogin/emaillogin.module#EmailloginPageModule' },
  { path: 'pinconfirmation', loadChildren: './pinconfirmation/pinconfirmation.module#PinconfirmationPageModule' },
  { path: 'pinauthentication', loadChildren: './pinauthentication/pinauthentication.module#PinauthenticationPageModule' },
  { path: 'verifykyc', loadChildren: './verifykyc/verifykyc.module#VerifykycPageModule' },
  { path: 'modalpage', loadChildren: './verifykyc/modalpage/modalpage.module#ModalpagePageModule' },
  { path: 'startkyc', loadChildren: './startkyc/startkyc.module#StartkycPageModule' },
  { path: 'uploadpan', loadChildren: './uploadpan/uploadpan.module#UploadpanPageModule' },
  { path: 'docconfirmation', loadChildren: './docconfirmation/docconfirmation.module#DocconfirmationPageModule' },
  { path: 'panform', loadChildren: './panform/panform.module#PanformPageModule' },
  { path: 'uploadpoa', loadChildren: './uploadpoa/uploadpoa.module#UploadpoaPageModule' },
  { path: 'poaimage/:documentType', loadChildren: './poaimage/poaimage.module#PoaimagePageModule' },
  // { path: 'poaform', loadChildren: './poaform/poaform.module#PoaformPageModule' },
  { path: 'poaform/:documentType', loadChildren: './poaform/poaform.module#PoaformPageModule' },
  { path: 'infomismatch', loadChildren: './infomismatch/infomismatch.module#InfomismatchPageModule' },
  { path: 'uploadsign', loadChildren: './uploadsign/uploadsign.module#UploadsignPageModule' },
  { path: 'videoselfie', loadChildren: './videoselfie/videoselfie.module#VideoselfiePageModule' },
  { path: 'commonkyc', loadChildren: './commonkyc/commonkyc.module#CommonkycPageModule' },
  { path: 'captureselfie', loadChildren: './captureselfie/captureselfie.module#CaptureselfiePageModule' },
  { path: 'kycform', loadChildren: './kycform/kycform.module#KycformPageModule' },
  { path: 'goaldescription', loadChildren: './pages/goal/goaldescription/goaldescription.module#GoaldescriptionPageModule' },
  { path: 'goalsetting', loadChildren: './pages/goal/goalsetting/goalsetting.module#GoalsettingPageModule' },
  // { path: 'passbookTab', loadChildren: './pages/transation/savings/savings.module#SavingsPageModule' },
  { path: 'successpage', loadChildren: './successpage/successpage.module#SuccesspagePageModule' },
  { path: 'createaccount', loadChildren: './createaccount/createaccount.module#CreateaccountPageModule' },
  { path: 'bankaccount', loadChildren: './pages/account/bankaccount/bankaccount.module#BankaccountPageModule' },
  //{ path: 'supportdetails', loadChildren: './supportdetails/supportdetails.module#SupportdetailsPageModule' },
  //{ path: 'videosdetails', loadChildren: './videosdetails/videosdetails.module#VideosdetailsPageModule' },
  { path: 'verifydahboard', loadChildren: './verifydahboard/verifydahboard.module#VerifydahboardPageModule' },
  { path: 'verify-folio-otp', loadChildren: './pages/account/verify-folio-otp/verify-folio-otp.module#VerifyFolioOtpPageModule' },
  { path: 'kyc-complete-status', loadChildren: './pages/account/kyc-complete-status/kyc-complete-status.module#KycCompleteStatusPageModule' },
  { path: 'account-info', loadChildren: './pages/account/account-info/account-info.module#AccountInfoPageModule' },
  //  {path: 'passbook', loadChildren: '../transation-history/transation-history.module#TransationHistoryPageModule'},
   
  { path: 'account-statement', loadChildren: './pages/transation/account-statement/account-statement.module#AccountStatementPageModule' },
  { path: 'update-goal', loadChildren: './pages/goal/update-goal/update-goal.module#UpdateGoalPageModule' },
  { path: 'savings', loadChildren: './pages/transation/savings/savings.module#SavingsPageModule' },
  { path: 'withdraw', loadChildren: './pages/transation/withdraw/withdraw.module#WithdrawPageModule' },
  { path: 'video-test', loadChildren: './pages/video-test/video-test.module#VideoTestPageModule' },
  { path: 'addnewgoal', loadChildren: './pages/goal/addnewgoal/addnewgoal.module#AddnewgoalPageModule' },
  { path: 'covid19', loadChildren: './tabs/home/covid19/covid19.module#Covid19PageModule' },
  { path: 'passbook-filter', loadChildren: './tabs/transation-history/passbook-filter/passbook-filter.module#PassbookFilterPageModule' },
  // { path: 'savingsconfirm', loadChildren: './pages/transation/savingsconfirm/savingsconfirm.module#SavingsconfirmPageModule' },
  { path: 'savingsconfirm/:savingAmount/:message', loadChildren: './pages/transation/savingsconfirm/savingsconfirm.module#SavingsconfirmPageModule' },
  { path: 'withdrawalconfirm', loadChildren: './pages/transation/withdrawalconfirm/withdrawalconfirm.module#WithdrawalconfirmPageModule' },
  { path: 'withdrawalconfirm/:withdrawAmount/:paymentType/:transMessage', loadChildren: './pages/transation/withdrawalconfirm/withdrawalconfirm.module#WithdrawalconfirmPageModule' },
  { path: 'goalvideo', loadChildren: './pages/goal/goalvideo/goalvideo.module#GoalvideoPageModule' },
  { path: 'goalstatic', loadChildren: './pages/goal/goalstatic/goalstatic.module#GoalstaticPageModule' },
  { path: 'goalcreated', loadChildren: './pages/goal/goalcreated/goalcreated.module#GoalcreatedPageModule' },
  { path: 'savingFail/:message', loadChildren: './pages/transation/saving-fail/saving-fail.module#SavingFailPageModule' },
  { path: 'withdraw-otp/:transationID/:amount', loadChildren: './pages/transation/withdraw-otp/withdraw-otp.module#WithdrawOtpPageModule' },
  { path: 'esign', loadChildren: './pages/kyc/esign/esign.module#EsignPageModule' },
  { path: 'esign-modal', loadChildren: './pages/kyc/esign-modal/esign-modal.module#EsignModalPageModule' },
  { path: 'language', loadChildren: './pages/utility/language/language.module#LanguagePageModule' },
  { path: 'profile', loadChildren: './pages/account/profile/profile.module#ProfilePageModule' },
  // { path: 'gold', loadChildren: './tabs/gold/gold.module#GoldPageModule' },
  // { path: 'gold-tabs', loadChildren: './pages/gold-tabs/gold-tabs.module#GoldTabsPageModule' },
  // { path: 'gold-home', loadChildren: './pages/goldTabs/gold-home/gold-home.module#GoldHomePageModule' },
  // { path: 'gold-passbook', loadChildren: './pages/goldTabs/gold-passbook/gold-passbook.module#GoldPassbookPageModule' },

  //{ path: 'gold', loadChildren: './pages/tabs/gold/gold.module#GoldPageModule' },


 // { path: 'login', loadChildren: './auth/login/login.module#LoginPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
