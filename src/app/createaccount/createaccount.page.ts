import { Component, OnInit } from '@angular/core';

import { Events,Platform } from '@ionic/angular';
import { NavController } from '@ionic/angular';

import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from  "@angular/router";
@Component({
  selector: 'app-createaccount',
  templateUrl: './createaccount.page.html',
  styleUrls: ['./createaccount.page.scss'],
})
export class CreateaccountPage implements OnInit {
  public caGroup : FormGroup;
  public customBackButton: any;

//  createaccount
  constructor(public  router:  Router,
    public events: Events,public platform: Platform,
    public navCtrl : NavController,public formBuilder: FormBuilder) { 
    this.caGroup = this.formBuilder.group({
      occupation: ['', Validators.required],
      nominee: [''],
      relation: [''],
      checkbox: ['']
    });
  }

  ngOnInit() {
    document.addEventListener("backbutton",function(e) {
        console.log("disable back button")
    }, false);
  }
  createaccount(){
    console.log('caGroup..', this.caGroup);
    this.router.navigateByUrl('bankaccount');
  }
  submitaccount(){
    
  }
  
  /**
   * Confirmation alert
   */
  async getBack(){
    console.log("Clickeddddddddddddddddddddddddd");
    this.events.publish('reloadHome', 'Hello from page1!');
    this.navCtrl.navigateRoot(['/homeTab']);
  }
}
