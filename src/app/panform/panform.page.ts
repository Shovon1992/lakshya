import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterService } from './../services/register.service';
import { Toast } from '@ionic-native/toast/ngx';
import { LoadingController } from '@ionic/angular';
import { BaseComponent } from '../base/base.component';

@Component({
    selector: 'app-panform',
    templateUrl: './panform.page.html',
    styleUrls: ['./panform.page.scss'],
})
export class PanformPage extends BaseComponent implements OnInit {
    public panGroup: FormGroup;
    panimg: any = '';
    panRegx = new RegExp('^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$');
    currentDate = new Date();
    poadata = (localStorage.pandetails) ? JSON.parse(localStorage.pandetails) : [];
    showDOB: any;// = new Date(poadata['DOB']).toISOString();
    currentStep = 1; /** For taking user input from user */
    panDetails : any  = JSON.parse(localStorage.getItem('pandetails'))
    pandData = {
        "UserId": 0,
        "Name": this.panDetails.Name,
        "FatherName": this.panDetails.FatherName,
        "MotherName": this.panDetails.MotherName,            
        "PAN": (this.panDetails.PAN) ? (this.panDetails.PAN) : localStorage.getItem('panno').toUpperCase(),
        "DOB": '',
        "MerchantId": "NA",
        "OnBoardToken": "NA",
        "PhoneNumber": localStorage.getItem('phoneno'),
        "FileName": "demo.jpeg",
        "FileInfo": {
            "FileName": "demo.jpeg",
            "FileContentBase64": localStorage.getItem('panimg')
        },
        "Success": true,
        "ErrorMessage": "string",
        "ExtendedMessage": "string"
    };




    // tempPanData = JSON.parse(localStorage.getItem('panimgDetails'))
    ngOnInit() {
        this.panimg = this.sanitizer.bypassSecurityTrustUrl(localStorage.getItem('panimg'));
        //alert(this.panimg)
        this.showDOB = this.poadata['DOB'];
        const panimgDetails = (localStorage.getItem('panimgDetails')) ? JSON.parse(localStorage.getItem(
            'panimgDetails')) : [];
        const panImage = localStorage.getItem('panimg');
        this.panGroup = this.formBuilder.group({
            name: ['', Validators.required],
            fathername:  ['', Validators.required],
            mothername: ['', Validators.required],
            panno:  [panimgDetails.PAN, Validators.required],
            dob: ['', Validators.required],
        });
        //this.panGroup['dob'] = '2002-09-23T15:03:46.789';
    }
    //For Loadder 
    
    getpanDate(test){
        console.log(test)
    }
    loadder: any;
    async showLodder(message) {
        this.loadder = await this.loadingCtrl.create({
            message: message
        });
        this.loadder.present();
    }
    uploadpan() {
        const poadata = (localStorage.pandetails) ? JSON.parse(localStorage.pandetails) : '';
        // alert(poadata);
 
        // if(poadata['MerchantId'] !== null) {      
            const urlparam = '/KYC/SubmitPOI';
            const pangroup = {
                "UserId": 0,
                "Name": this.panGroup.get('name').value.trim(),
                "FatherName": this.panGroup.get('fathername').value.trim(),
                "MotherName": this.panGroup.get('mothername').value.trim(),            
                "PAN": this.panGroup.get('panno').value.trim(),
                "DOB": this.panGroup.get('dob').value.trim(),
                "MerchantId": "NA",
                "OnBoardToken": "NA",
                "PhoneNumber": localStorage.getItem('phoneno'),
                "FileName": "demo.jpeg",
                "FileInfo": {
                    "FileName": "demo.jpeg",
                    "FileContentBase64": localStorage.getItem('panimg')
                },
                "Success": true,
                "ErrorMessage": "string",
                "ExtendedMessage": "string"
            };
            
            let selectedDOB = new Date(this.pandData.DOB)
            let diffDays = Math.ceil(Math.abs((selectedDOB.valueOf() - this.currentDate.valueOf()) / (1000 * 3600 * 24*365))) ;
            console.log(diffDays, selectedDOB.valueOf(), this.currentDate.valueOf());

            if(this.panRegx.test(this.pandData.PAN)){
                if(this.pandData.Name && this.pandData.DOB && this.pandData.MotherName && this.pandData.Name && this.pandData.FatherName){
                    if ((diffDays) > 18) {
                        this.present('Uploading PAN details..',5000);
                        this.registerservice.postCall(urlparam, this.pandData, true).subscribe(result => {
                            this.dismiss();
                            if(result['Success']){
                                this.router.navigateByUrl('uploadpoa');
                            }
                            else{
                                this.toast.show((result.ErrorMessage)? (result.ErrorMessage): (result.ExtendedMessage), '5000', 'center').subscribe(
                                toast => {
                                    console.log(toast);
                                    //this.router.navigateByUrl('uploadpoa');
                                });
                            }                    
                        }, err => {
                            this.dismiss();
                            this.toast.show('Something weent worng pleasse try again.', '5000', 'center').subscribe(
                                toast => {
                                    console.log(toast);
                                });
                        });
                    } else {
                        alert('You mast be 18 years old.')
                        // this.toast.show('You mast be 18 years old.', '5000', 'center').subscribe(toast => {
                        //     console.log(toast);
                        // });
                    }
                } else{
                    alert('All fields are mandatory')
                }    
            } else {
                alert('Please enter valid Pan number.');
            } 
        //}
        // else {
        //     this.alertMessage('Error', 'Something went wrong ,please relogin after sometime')
        //     return false;
        // }        // this.router.navigateByUrl('uploadpoa');
    }


    panFetch(){
        if(this.pandData.PAN && this.pandData.MotherName && this.pandData.DOB){
            console.log(this.pandData);
            this.showLodder('loading');
            this.kycService.panFetch(this.pandData.PAN).subscribe(
                res =>{
                    this.hideLoader();
                    console.log(res);
                    this.pandData.FatherName = res['fatherName'];
                    this.pandData.Name = res['name']
                    this.currentStep = 2;
                }, err =>{
                    this.alertMessage('Alert', 'All fields mandatary');
                    console.log(err);
                }
            )
            
        } else{
            this.alertMessage('Alert', 'All fields mandatary');
        }
        
    }
}