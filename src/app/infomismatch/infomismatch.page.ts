import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { BaseComponent } from '../base/base.component';
@Component({
  selector: 'app-infomismatch',
  templateUrl: './infomismatch.page.html',
  styleUrls: ['./infomismatch.page.scss'],
})
export class InfomismatchPage extends BaseComponent implements OnInit {

informationDetails: any = [];

adharFromDetails = JSON.parse(localStorage.getItem('poaDetails'));
resMessage = "Please conifrm below details to continue.."
  ngOnInit() {
    this.getInformation();
  }

  uploadsignature(){
    this.router.navigateByUrl('uploadsign');
  // this.router.navigateByUrl('docconfirmation', { state: { page: 'uploadsign' }});
  }

/**
 * Get information
 */
getInformation(){
  let data = {
    "PhoneNumber": this.currentUserDetails.PhoneNumber,
    "PoaName": (this.adharFromDetails) ? this.adharFromDetails.Name : null,
    "PoaDob": (this.adharFromDetails) ? this.adharFromDetails.DOB : null
  }
  try {
     this.utilService.getInformationDetails(data).subscribe(
       res =>{
         //this.hideLoader();
         console.log(res);
         this.informationDetails = res;
         if(res['Success']){
          this.resMessage = "Please conifrm below details to continue.."
         }else{
          this.resMessage = res['ExtendedMessage'];
         }
       }, err =>{
         //this.hideLoader();
         this.alertMessage('Error','Unable to fetch informaion.');
       }
     )
   } catch (error) {
     console.log(error);
     this.alertMessage('Error','Unable to fetch informaion.');
   }
}
login(){
  this.router.navigateByUrl('homeTab');
}
  /**
   * End of class
   */
}
