import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { InfomismatchPage } from './infomismatch.page';

const routes: Routes = [
  {
    path: '',
    component: InfomismatchPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,TranslateModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [InfomismatchPage]
})
export class InfomismatchPageModule {}
