import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-qrscanner',
  templateUrl: './qrscanner.page.html',
  styleUrls: ['./qrscanner.page.scss'],
})
export class QrscannerPage implements OnInit {

  public isFlashLightOn: boolean = false;
  public scanSub: any;
  public msg="";

  constructor(public modalController: ModalController,
              public qrScanner: QRScanner,public translate: TranslateService,) {}

  ngOnInit() {
  }
  toggleFlashLight(){
    
  }
  ionViewWillEnter() {
    this.showCamera();

    this.qrScanner.prepare()
    .then((status: QRScannerStatus) => {
      if (status.authorized) {
        // camera permission was granted
        console.log('Camera Permission Given');
 
        // start scanning
        this.scanSub = this.qrScanner.scan().subscribe((text: string) => {
        console.log(text);
          alert(text);
          this.msg = text;
          this.qrScanner.hide();
          this.scanSub.unsubscribe();
          this.hideCamera();
        });

        // show camera preview
        this.qrScanner.show();

        // wait for user to scan something, then the observable callback will be called

      } else if (status.denied) {
        // camera permission was permanently denied
        // you must use QRScanner.openSettings() method to guide the user to the settings page
        // then they can grant the permission from there
        console.log('Camera permission denied');
      } else {
        // permission was denied, but not permanently. You can ask for permission again at a later time.
        console.log('Permission denied for this runtime.');
      }
    })
    .catch((e: any) => console.log('Error is', e));
  }

  showCamera() {
    (window.document.querySelector('html') as HTMLElement).classList.add('cameraView');
  }

  hideCamera() {
    (window.document.querySelector('html') as HTMLElement).classList.remove('cameraView');
  }

  closeModal() {
    this.modalController.dismiss();
  }

  ionViewWillLeave(){
    this.qrScanner.hide();
    this.scanSub.unsubscribe();
    this.hideCamera();
  }

 

}
