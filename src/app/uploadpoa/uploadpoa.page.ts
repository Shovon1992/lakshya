import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { RegisterService } from './../services/register.service';
//import { ImagePicker,ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
@Component({
  selector: 'app-uploadpoa',
  templateUrl: './uploadpoa.page.html',
  styleUrls: ['./uploadpoa.page.scss'],
})
export class UploadpoaPage implements OnInit {
  uploadimg:any;
  constructor(private  router:  Router,private translate: TranslateService,private registerservice:RegisterService,
    private camera: Camera) { }

  ngOnInit() {
  }

  // getPic(){
  //   const options: ImagePickerOptions = {
  //     maximumImagesCount: 1,
  //     quality: 100,
  //     width: 512,
  //     height: 200,
  //     outputType: 1
  //   }
  //   this.imagePicker.getPictures(options).then((results) => {
  //     for (var i = 0; i < results.length; i++) {
  //     this.uploadimg = 'data:image/jpeg;base64,' + results[0];
  //     }
  //     }, (err) => {
  //     // console.log(‘err’ + err);
  //     });
  //  // this.router.navigate(['docconfirmation'], { state: { page: 'uploadpan' } });
  // }
  capturePic(){
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 512,
      targetHeight: 150,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.uploadimg='data:image/jpeg;base64,' + imageData;
    }, (err) => {
     // Handle error
    });
  }
  
  movepoapage(documentType){
    this.router.navigateByUrl(`poaimage/${documentType}`);
  }

}
