import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';

import {Validators, FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-kycform',
  templateUrl: './kycform.page.html',
  styleUrls: ['./kycform.page.scss'],
})
export class KycformPage implements OnInit {
  public kycGroup : FormGroup;
  constructor(public  router:  Router, public translate: TranslateService,public formBuilder: FormBuilder) { 
    this.kycGroup = this.formBuilder.group({
      mothername: ['', Validators.required],
      occupation: [''],
      annualincome: [''],
      placeofbirth: [''],
    });
  }

  ngOnInit() { 
  } 
  kycconfiem(){
 this.router.navigateByUrl('infomismatch');
  }
}
