import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from  "@angular/router";
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { RegisterService } from './../services/register.service';
import { LanguageService } from './../services/language.service';
import { TransationService } from './../services/transationService/transation.service';


@Injectable({
  providedIn: 'root'
})
export class LoginGurdGuard implements CanActivate {
  constructor(public navCtrl: NavController, public transationService: TransationService, public languageService: LanguageService, public registerservice: RegisterService, public localStorage: Storage, public router: Router){
   
  }
  loginDetails: any = [];
  status: any;
  currentUserDetails : any =[]; //JSON.parse(localStorage.getItem('userDetails'));
  canActivate(): boolean {
      this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
		  const urlparam = '/Authentication/Login';
      
      if (this.currentUserDetails !== null && this.currentUserDetails.PhoneNumber !== null ) {
        this.status =parseInt(this.currentUserDetails['Status']);
        
         setTimeout(() => {
           this.navCtrl.navigateRoot('masterpage');          
          }, 10);
        return true;		
      }
      else {     
        this.navCtrl.navigateRoot('otplogin');
        return false;
      }
    }
  
}
