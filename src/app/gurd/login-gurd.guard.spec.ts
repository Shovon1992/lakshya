import { TestBed, inject, waitForAsync } from '@angular/core/testing';

import { LoginGurdGuard } from './login-gurd.guard';

describe('LoginGurdGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginGurdGuard]
    });
  });

  it('should ...', inject([LoginGurdGuard], (guard: LoginGurdGuard) => {
    expect(guard).toBeTruthy();
  }));
});
