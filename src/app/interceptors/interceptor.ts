import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
  } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';


@Injectable()
export class Interceptor implements HttpInterceptor {   
    loaderToShow: any;
    // lastLoginDate:number = Date.parse('2020-04-02T22:00:14.053Z');
    currentLoginDate:any;
    paymentDate:any;
    lastLoginDate = 0;
    notifyStatus = 0;
    notifyDate:Date;

    constructor(public loadingController: LoadingController,private router: Router, public toastController: ToastController) {
        this.currentLoginDate = Date.parse(new Date().toISOString());

        if (localStorage.getItem('lastLogin')) {
            this.lastLoginDate = Date.parse(localStorage.getItem('lastLogin'));
        }
    }

    /**
     * Intercepter call after each request
     * @param request 
     * 
     * @param next 
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("####################dfffffffffffff#######################");    
      
        if (localStorage.getItem('lastLogin')) {
            this.lastLoginDate = Date.parse(localStorage.getItem('lastLogin'));
        }
        console.log(this.lastLoginDate);
        console.log(localStorage.getItem('lastLogin'));
        console.log(request.url);

        if (this.lastLoginDate) {
            let diffTime = ((this.currentLoginDate - this.lastLoginDate) / (1000 * 60 * 2));
            console.log(request);
            console.log(diffTime);

            if (diffTime >= 1) {
                this.paymentDate = JSON.parse(localStorage.getItem('paymentDate'));
                this.notifyStatus = JSON.parse(localStorage.getItem('notifyStatus'));
                this.notifyDate = JSON.parse(localStorage.getItem('notifyDate'));
                localStorage.clear();
            
                if (this.paymentDate !== null) {
                    localStorage.setItem('paymentDate', JSON.stringify(this.paymentDate));
                    localStorage.setItem('notifyDate', JSON.stringify(this.notifyDate));
                    localStorage.setItem('notifyStatus', JSON.stringify(this.notifyStatus));
                }
                this.router.navigateByUrl('otplogin');	
            }
            else {
                localStorage.setItem('lastLogin', new Date().toISOString());                     
            }
        } 
        else {    
            this.paymentDate = JSON.parse(localStorage.getItem('paymentDate'));
            this.notifyStatus = JSON.parse(localStorage.getItem('notifyStatus'));
            this.notifyDate = JSON.parse(localStorage.getItem('notifyDate'));
          
            localStorage.clear();
        
            if (this.paymentDate !== null) {
                localStorage.setItem('paymentDate', JSON.stringify(this.paymentDate));
                localStorage.setItem('notifyDate', JSON.stringify(this.notifyDate));
                localStorage.setItem('notifyStatus', JSON.stringify(this.notifyStatus));
            }
            this.router.navigateByUrl('otplogin');	        
        }

        return next.handle(request);
    }

    showLoader() {
        this.loaderToShow = this.loadingController.create({
            message: 'Processing Server Request'
        }).then((res) => {
            res.present();
            res.onDidDismiss().then((dis) => {
                console.log('Loading dismissed!!');
            });
        });

        this.hideLoader();
    }

    hideLoader() {
        this.loadingController.dismiss();
    }
}