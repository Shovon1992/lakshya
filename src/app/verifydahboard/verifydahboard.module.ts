import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { VerifydahboardPage } from './verifydahboard.page';

const routes: Routes = [
  {
    path: '',
    component: VerifydahboardPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VerifydahboardPage]
})
export class VerifydahboardPageModule {}
