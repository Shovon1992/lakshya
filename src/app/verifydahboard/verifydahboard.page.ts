import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-verifydahboard',
  templateUrl: './verifydahboard.page.html',
  styleUrls: ['./verifydahboard.page.scss'],
})
export class VerifydahboardPage implements OnInit {

  constructor(private menu: MenuController) { }

  ngOnInit() {
    this.openFirst()
  }
  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

}
