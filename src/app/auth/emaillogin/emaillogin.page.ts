import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { ToastController ,AlertController} from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { DeviceAccounts } from '@ionic-native/device-accounts/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { RegisterService } from './../../services/register.service';
import { Toast } from '@ionic-native/toast/ngx';
@Component({
  selector: 'app-emaillogin',
  templateUrl: './emaillogin.page.html',
  styleUrls: ['./emaillogin.page.scss'],
})
export class EmailloginPage implements OnInit { 
  public email1 = 'a@gmail.com'; 
  selectedemail=''; 
  checkbox = true;
  selectedparam:boolean;
  email: string;
  public emailGroup : FormGroup;
// private  authService:  AuthService, 
  constructor(private toastCtrl: ToastController,private  router:  Router, private translate: TranslateService,private deviceAccounts: DeviceAccounts,
    private androidPermissions: AndroidPermissions,private alertCtrl: AlertController, private formBuilder: FormBuilder,private registerservice:RegisterService,private toast: Toast ) {
    this.email = 'a@gmail.com';
    this.emailGroup = this.formBuilder.group({
      PhoneNumber: localStorage.getItem('phoneno'),
      Email: this.email,
      PIN: localStorage.getItem('pin'),
      DeviceId: localStorage.getItem('uuid'),
      Lang: (localStorage.getItem('lang') == 'en') ? 'EN': 'KA' ,
      ReferralCode:localStorage.getItem('code'),
    });
  }
  /**
 * Alert message
 */
async alertMessage(header,message){
  let modal = await this.alertCtrl.create({
    header: header,
    message: message,
    buttons: ['OK']
  });
  await modal.present();
}

  async ionViewDidEnter() {
    //this.getPermissionsFromDevice()
  }
  getPermissionsFromDevice() {
    this.deviceAccounts.getPermissions().then(accounts => {
      this.getAccountsFromDevice();
    })
  }
  getAccountsFromDevice() {
    this.deviceAccounts.get()
      .then(accounts => { 
        this.email1 = accounts[0].name;
        this.email = this.email1;
        this.alertMessage(this.email,this.email);
      })
      .catch(error => console.log(JSON.stringify(error)))
  }
  ngOnInit() { }

  async presentToast(message, show_button, position, duration) {
    const toast = await this.toastCtrl.create({
      message: message,
      showCloseButton: show_button,
      position: position,
      duration: duration
    });
    toast.present();
  }

  login(){
    if(this.emailGroup.valid) {
      localStorage.setItem('email',this.emailGroup.get('Email').value);
      if(this.validateEmail(this.emailGroup.get('Email').value)){
        if(!this.checkbox){

          this.alertMessage('Alert !', 'In order to proceed ticking in the box is mandatory')
          
        } else {
          this.router.navigateByUrl('pinauthentication')
        }
        
      } else{
        this.toast.show('Please enter email', '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          });
      }
    } else{
      this.toast.show('Please enter email', '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        });
    }
  }

/**
 * Validate correct email
 */
 validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
  /**
   * End of class
   */
}
