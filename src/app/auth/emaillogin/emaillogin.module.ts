import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { EmailloginPage } from './emaillogin.page';

const routes: Routes = [
  {
    path: '',
    component: EmailloginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,TranslateModule,ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EmailloginPage]
})
export class EmailloginPageModule {}
