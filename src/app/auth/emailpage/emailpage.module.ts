import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { EmailpagePage } from './emailpage.page';

const routes: Routes = [
  {
    path: '',
    component: EmailpagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,TranslateModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EmailpagePage]
})
export class EmailpagePageModule {}
