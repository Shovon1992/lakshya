import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { DeviceAccounts } from '@ionic-native/device-accounts/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Toast } from '@ionic-native/toast/ngx';
@Component({
  selector: 'app-emailpage',
  templateUrl: './emailpage.page.html',
  styleUrls: ['./emailpage.page.scss'],
})
export class EmailpagePage implements OnInit {
  public email1 = 'a@gmail.com'; selectedemail=''; selectedparam:boolean;
  constructor(public alertCtrl: AlertController,private  router:  Router, private translate: TranslateService, private deviceAccounts: DeviceAccounts,
    private androidPermissions: AndroidPermissions, private toast: Toast) { }
  
  checkbox: any;
  async ionViewDidEnter() {
    this.getPermissionsFromDevice()
  }

  getPermissionsFromDevice() {
    this.deviceAccounts.getPermissions().then(accounts => {
      this.getAccountsFromDevice();
    })
  }

  getAccountsFromDevice() {
    this.deviceAccounts.get()
      .then(accounts => { 
        this.email1 = accounts[0].name;
        //this.selectedemail = this.email1;
      })
      .catch(error => console.log(JSON.stringify(error)))
  }

  async showAlert(content, type) {
    let alert = await this.alertCtrl.create({
      header: type,
      message: content,
      buttons: ['OK']
    });
    return await alert.present();
  }
  ngOnInit() { }
  getInfos(){ }

  signin(param , email){
    this.selectedparam = param;
    this.selectedemail = this.email1;
    localStorage.setItem('email',email);
      if(!param){
        this.router.navigate(['emaillogin'], { state: {  status: false } });
      }
  }

  verify(){    
    if(this.selectedemail !== ''){
      if(this.checkbox){
        this.toast.show('Please accept the condition', '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          });
      } else {
        this.router.navigate(['emaillogin'], { state: { status: true , email : this.selectedemail} });
      }
      
    } else {
    this.toast.show('Please select email', '5000', 'center').subscribe(
      toast => {
        console.log(toast);
      });
    }  
  }
}
