import { Component, OnInit } from '@angular/core';
//declare var SMSReceive: any;
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterService  } from './../../services/register.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LoadingController } from '@ionic/angular';
import { Toast } from '@ionic-native/toast/ngx';
//declare var SMS: any;
@Component({
    selector: 'app-otpverfication',
    templateUrl: './otpverfication.page.html',
    styleUrls: ['./otpverfication.page.scss'],
})
export class OtpverficationPage implements OnInit {
    OTP = [];
    regNumber = localStorage.getItem('phoneno');
    verifyCode = parseInt(localStorage.getItem('verifyCode')); 
 
    showOTPInput: boolean = false;
    public verifyGroup: FormGroup;
    public loginGroup: FormGroup;
    OTPmessage: string = 'An OTP is sent to your number. You should receive it in 15 s';
    constructor(public router: Router, public translate: TranslateService, public formBuilder: FormBuilder,
        public registerservice: RegisterService, public toast: Toast, public androidPermissions: AndroidPermissions,
        public loadingCtrl: LoadingController) {
        this.verifyGroup = this.formBuilder.group({
            PhoneNumber: localStorage.getItem('phoneno'),
            Code: this.OTP,
            VerificationType: this.verifyCode,
        });
    }
    ngOnInit() {
        console.log('verifyGroup', this.verifyGroup);
    }
    ionViewWillEnter() {
        this.regNumber = localStorage.getItem('phoneno');
        this.verifyCode = parseInt(localStorage.getItem('verifyCode')); 
     
   /**
    * Below commented for auto read SMS
    *       this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(success => {
            console.log('Permission granted');
            this.start();
        }
            , err => this.androidPermissions.requestPermission(this.androidPermissions
                .PERMISSION.READ_SMS));
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);  */
    }

   /** 
    * Below commented for the SMS auto read
    * 
    *  ionViewDidEnter() {
        this.start();
    }

    start() {
        SMSReceive.startWatch(
            (success) => {
                console.log(success)
                console.log("Suryabhana OTP");
                console.log(success);
                document.addEventListener('onSMSArrive', (e: any) => {
                    var IncomingSMS = e.data;
                    //IncomingSMS = IncomingSMS.replace(' . Wish you prosperity.','');
                    console.log(e.data)
                    this.OTP = IncomingSMS.body.slice(12, 16);
                    this.stopSMS();
                    //this.processSMS();
                });
            },
            (error) => {
                console.log(error);
            })
    }


    stopSMS(){
        SMSReceive.stopWatch(
            () => { console.log('watch stopped') },
            () => { console.log('watch stop failed') }
            )
    }

*/

    processSMS() {
        // Check SMS for a specific string sequence to identify it is you SMS
        // Design your SMS in a way so you can identify the OTP quickly i.e. first 6 letters
        // In this case, I am keeping the first 6 letters as OTP
        // const message = data.body;
        let otpCode = '';
        for (let i = 0; i < 4; i++) {
            otpCode = otpCode + '' + this.OTP[i];
        }
        console.log(otpCode.length, otpCode);
        if (otpCode) {
            // this.OTP = data.body.slice(12, 16);
            let data = {
                PhoneNumber: localStorage.getItem('phoneno'),
                Code: otpCode,
                VerificationType: localStorage.getItem('verifyCode'),
            };
            
            
            console.log("suryabhan data");
            console.log(data);
            
            const url = '/Verify/VerifyOTP?PhoneNumber=';
            this.showLodder('Verifing OTP..')

            this.registerservice.postCall(url, data, 'verifyotp').subscribe(result => {
                this.loadder.dismiss();
                if (result.Success) {
                    if (this.verifyCode == 2) {      
                        this.router.navigateByUrl('pinauthentication');
                    } else {
                        //this.router.navigateByUrl('emailpage');//emaillogin
                         this.router.navigateByUrl('emaillogin');
                    } 
                      
                } else {
                    this.toast.show(result['ExtendedMessage'], '5000', 'center').subscribe(toast => {
                        console.log(toast);
                    });
                }
                console.log('result', result);
            }, err => {
                this.loadder.dismiss();
                this.toast.show('Something went wrong please try again', '5000', 'center').subscribe(
                    toast => {
                        console.log(toast);
                    });
            });
            this.OTPmessage = 'OTP received. Proceed to register'
            //this.stop();
        }
    }
    //For Loadder 
    loadder: any;
    async showLodder(message) {
        this.loadder = await this.loadingCtrl.create({
            message: message
        });
        this.loadder.present();
    }
    
    /**
     * Resend otp
     */
    resendOtp() {
        this.showLodder('Resending OTP to ' + localStorage.getItem('phoneno') + '..')
        let data = {
            "PhoneNumber": localStorage.getItem('phoneno'),
        }
        const urlparam = '/Verify/GenerateOTP?PhoneNumber=';
        if (data) {
            this.registerservice.postCall(urlparam, data, false).subscribe(result => {
                this.loadder.dismiss();
                //  this.information = result;
                //alert(JSON.stringify(result) );
                console.log("Suryabhana OTP");
                console.log(result);

                if (result.Success) {
                    this.toast.show('Otp sent to your mobile', '5000', 'center').subscribe(toast => {
                        console.log(toast);
                    });
                } else {
                    this.toast.show('Something went wrong please try again', '5000', 'center').subscribe(
                        toast => {
                            console.log(toast);
                        });
                }
            }, err => {
                this.loadder.dismiss();
            });
        } else {
            this.toast.show('Please select number', '5000', 'center').subscribe(toast => {
                console.log(toast);
            });
        }
    }
    goToNextInput(charCode, currentStage) {
        let next = 'input' + parseInt(currentStage + 1);
        console.log(charCode.length, next)
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            if (charCode.length == 0) {
                document.getElementById(next).querySelector('input').focus();
                return true;
            }
        }
    }

    goToNextInput1(event: any,prev,current, next,keyPos, charCode) {
        const pattern = /[0-9,]/;
        let inputChar = String.fromCharCode(event.charCode);
    
        if (event.charCode > 31 && (event.charCode < 48 || event.charCode > 57)) {
            this.OTP[keyPos] = "";
            return false;
        } else {
            if (charCode.length == 0) {
                if (current.value.length == 0) {
                    next.setFocus()
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
    
    checkLength(ele, charCode) {
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            if (ele.value.length != 0) {
                return false
            }
        }
    }

    onKeydownEvent(event: KeyboardEvent,prev,keyPos): void {
        const pattern = /[0-9,]/;
        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode == 46 || event.keyCode == 8) {
            if (keyPos == 4) {
                this.OTP[3] = "";
            }
            else if (keyPos == 3) {
                this.OTP[3] = "";
                this.OTP[2] = "";
            }
            else if (keyPos == 2) {
                this.OTP[3] = "";
                this.OTP[2] = "";
                this.OTP[1] = "";
            }
            else if (keyPos == 1) {
                this.OTP[3] = "";
                this.OTP[2] = "";
                this.OTP[1] = "";
                this.OTP[0] = "";
            }
            console.log(this.OTP);
            prev.setFocus();
        }
    }

    /**
     * End of class
     */
}