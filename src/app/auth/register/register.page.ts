import { Component, OnInit } from '@angular/core';
import { Sim } from '@ionic-native/sim/ngx';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { RegisterService } from './../../services/register.service';
import { Toast } from '@ionic-native/toast/ngx';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public sim1no = ''; public sim2no = ''; selectednumber = ''; selectedParam:boolean;
  constructor(public sim: Sim, public  router:  Router,public translate: TranslateService,public toast: Toast,
    public registerservice:RegisterService) { }

  ngOnInit() {
    this.getInfos();
  }
  async getInfos(){
    

    this.sim.hasReadPermission().then(
      (info) => console.log('Has permission: ', info)
    );
    
    this.sim.requestReadPermission().then(
      () => {console.log('Permission granted');
      this.getPhonedetails();},
      () => console.log('Permission denied')
    );
  }
  getPhonedetails(){
    //this.getInfos();
    this.sim.getSimInfo().then(
      (info) => {
       //alert('Sim info: '+ JSON.stringify(info));
      const phonedetails = info.cards;
      // this.sim1no =  phonedetails[0].phoneNumber.slice(phonedetails[0].phoneNumber.length - 10);
      // this.sim2no =  phonedetails[1].phoneNumber.slice(phonedetails[1].phoneNumber.length - 10);
      this.sim1no =  (phonedetails[0]) ? phonedetails[0].phoneNumber : null;
      this.sim2no =  (phonedetails[1]) ? phonedetails[1].phoneNumber : null;
      //alert( this.sim1no +', '+this.sim2no);
      //this.selectednumber = this.sim1no;
    },
      (err) => console.log('Unable to get sim info: ', err)
    );
  }
  signin(param , number){
    
    this.selectednumber = number;
    this.selectedParam = param;
    if(!param){
      localStorage.setItem('phoneno',this.selectednumber);
      this.router.navigate(['login'], { state: {  status: false } });
    }
    // this.router.navigateByUrl('login');
  }

  moveLogin(){
    
   if(this.selectednumber !== ''){
    localStorage.setItem('phoneno',this.selectednumber);
      this.router.navigate(['login'], { state: { status: true , phonenumber : this.selectednumber} });
    }else{
      this.toast.show('Please select number', '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        });
    }
  }
}
