import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { ToastController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { RegisterService } from './../../services/register.service';
import { Toast } from '@ionic-native/toast/ngx';
import { BaseComponent } from 'src/app/base/base.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BaseComponent implements OnInit {
  public status:Boolean = false;
  phonenum: string = localStorage.getItem('phoneno'); 
  verifyCode = parseInt(localStorage.getItem('verifyCode')); 
  verifyCodeBtnStatus:Boolean = false; 
  loginForm = {};
  public loginGroup : FormGroup;
// private  authService:  AuthService, 
  // constructor(private toastCtrl: ToastController,private  router:  Router, private translate: TranslateService,private formBuilder: FormBuilder,
  //   private toast: Toast,private registerservice:RegisterService, public loadingCtrl: LoadingController) {
  
  //  }
  
  ngOnInit() {
    console.log("Verification Code");
    console.log(this.verifyCode);

    if (this.verifyCode == 2) {
      this.verifyCodeBtnStatus = true
    }  
 
    this.loginGroup = this.formBuilder.group({
      PhoneNumber: [  this.phonenum, Validators.required],
      ReferralCode: [''],
      VerificationType: this.verifyCode,
    });
    // this.status = this.router.getCurrentNavigation().extras.state.status;
    // if(!this.status){
    //  this.phonenum = '';
    // }
    // if(this.status){
    //   this.phonenum = this.router.getCurrentNavigation().extras.state.phonenumber;
      
    //   this.loginGroup.patchValue({
    //     PhoneNumber: this.phonenum, 
    //     // formControlName2: myValue2 (can be omitted)
    //   });
    //  }
  }
  // async presentToast(message, show_button, position, duration) {
  //   const toast = await this.toastCtrl.create({
  //     message: message,
  //     showCloseButton: show_button,
  //     position: position,
  //     duration: duration
  //   });
  //   toast.present();
  // }
  next() {
    this.router.navigateByUrl('language-popover');
  }
//For Loadder 
loadder: any;
async showLodder(message){
  this.loadder = await this.loadingCtrl.create(
    {
      message: message
    }
  );
this.loadder.present();
}

  verify() {
    
    localStorage.setItem('phoneno',this.loginGroup.controls['PhoneNumber'].value);
    localStorage.setItem('referalcode',this.loginGroup.controls['ReferralCode'].value);
    localStorage.setItem('verifyCode',this.loginGroup.controls['VerificationType'].value);
    console.log("Verify OTP");
    console.log(this.loginGroup.value);

    const urlparam = '/Verify/GenerateOTP?PhoneNumber=';
    if(this.loginGroup.controls['PhoneNumber'].value && this.loginGroup.controls['PhoneNumber'].value.toString().length == 10){
      this.showLodder('Validating phone number..')
      this.registerservice.postCall(urlparam,this.loginGroup.value, false).subscribe(result => {
        this.loadder.dismiss();
        //  this.information = result;
         //alert(JSON.stringify(result) );
         let errMsg = (result['ExtendedMessage']) ? result['ExtendedMessage'] : result['ErrorMessage'];
        
         if(result.Success){
           this.router.navigateByUrl('otpverfication');
         } else{
          this.alertMessage('Error',errMsg ? errMsg : 'Something went wrong please try again');
         }
         
        }, err=>{
          this.loadder.dismiss();
        });
    }else{
      this.alertMessage('Error','Please enter valid 10 digit mobile number');
    }      
  }
 
  login(form){
 
   // this.router.navigateByUrl('dashboard');
    // this.authService.login(form.value).subscribe((res)=>{
     
    // });
  }
  logForm(form){
    console.log(this.loginGroup.value)
  }

  checklength(val) {

    if (val.length < 10 ) {
        return true;
    } else {
        return false
    }

  }

  verifyForgotPin() {
    this.router.navigateByUrl('otpverfication');
  
    console.log("Forgot Password");
  }

}