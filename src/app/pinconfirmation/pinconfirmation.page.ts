import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-pinconfirmation',
  templateUrl: './pinconfirmation.page.html',
  styleUrls: ['./pinconfirmation.page.scss'],
})
export class PinconfirmationPage implements OnInit {
  OTP= [];
  showOTPInput: boolean = false;
  constructor(private  router:  Router,private translate: TranslateService,) { }

  ngOnInit() {
    setTimeout(() => {
    //  this.router.navigateByUrl('emailpage');
  }, 2500);
  }
  confirmpin(){
    this.router.navigateByUrl('pinauthentication');
  }
}
