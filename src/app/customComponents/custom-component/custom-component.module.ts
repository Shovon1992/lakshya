import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from '../header/header.component';
import { HeaderBackComponent } from '../header-back/header-back.component';
import { GoalListComponent } from '../goal/goal-list/goal-list.component';


const PAGES_COMPONENTS = [
  HeaderComponent,
  HeaderBackComponent,
  GoalListComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule.forRoot(),
  ],
  declarations: [
    PAGES_COMPONENTS
  ],
  exports: [
    PAGES_COMPONENTS
  ],
  entryComponents: [],
})
export class CustomComponentModule { }
