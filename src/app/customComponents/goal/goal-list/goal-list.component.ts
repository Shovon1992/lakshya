import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-goal-list',
  templateUrl: './goal-list.component.html',
  styleUrls: ['./goal-list.component.scss'],
})
export class GoalListComponent extends BaseComponent implements OnInit {

  mileStones: any = [];
  goalDetails: any = [];
  totalNumberSet = 0;
  permonthAmount = 0;
  currentMilestone = 1;
  tansationAmount : any;
  noOfDayInLastM = 0;
  noOfDayInNextM = 0;
  transationDetails: any = [];
  counter  =0;
  goalAmount: any;
  totalInvest: any;
  pathWidth: any;
  autoPosition = 5+"px";

	currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));

/**
 * For showing in save till date amount
 */
  currentSavings1 = 0;

  ngOnInit() {
    //this.getGoalDetails();
    //this.getBankAccountDetailsOfUser1();
  //   setTimeout(() => {
  //     this.transationDetails = JSON.parse(localStorage.getItem('transationDetails'));
  //     this.getTransationHistory();
  // }, 30000);
  }


	ionViewDidEnter() {
		this.getBankAccountDetailsOfUser1();
	}
	doRefresh(event) {
    console.log("Suryabhan");
    console.log('doRefresh.............++++++++++');
    this.ionViewDidEnter();
    
	}

  setAutoStyles() {
    let styles = {
      'width': "40px",
      'margin-bottom': "-20px",
      'margin-left': this.autoPosition 
    };
    return styles;
  }

  setPathStyles() {
    let styles = {
      'margin-bottom': '-3px' 
    };
    return styles;
  }


  getGoalDetails(){
    try {
      this.goalService.getGoalLists(this.currentUserDetails.PhoneNumber).subscribe(
        res => {
           //this.hideLoader();
          if(res['Success']){
            this.mileStones = (res['GoalDetail'][0])? res['GoalDetail'][0]['MileStone']: 0;
            this.goalDetails = res['GoalDetail'][0];
            this.totalNumberSet = Math.ceil((this.mileStones.length-3)/3);
            this.permonthAmount = res['GoalDetail'][0]['MileStone'][0]['MileStoneAmount'];
            this.currentMilestone = res['GoalDetail'][0]['CurrentMileStone'];
            this.goalAmount = res['GoalDetail']['GoalAmount'];
             //this.navCtrl.navigateForward('homeTab');
          } else {
            this.alertMessage('Error', res['ExtendedMessage']);
          }
          console.log(res,this.mileStones)
        }, err =>{
          // this.hideLoader();      
          this.alertMessage('Error', 'Server issue occured, please try again later')
        }
       )
     } catch (error) {
      // this.hideLoader();
       console.log(error)
       this.alertMessage('Error', 'Server issue occured, please try again later')
     }
     
   }


   /*
   Add amount
   */
  addAmount(amount){
    this.tansationAmount = (this.tansationAmount) ? this.tansationAmount : 0;
    this.tansationAmount = parseInt(this.tansationAmount) + parseInt(amount);
  }


  getTransationHistory(){
    try {
      //this.showLodder('Loadding your transation history..');
       this.transationService.getTransationHistory(this.userAccountDetails.FolioNumber).subscribe(
         res =>{
           //this.hideLoader();
           console.log(res);
           if(res['Success']){
             this.transationDetails = res;
             console.log(this.transationDetails)
             //this.checkInvestWithdraw()
             //this.checkcurrentSavings1();
             this.currentSavings1 = res['BalanceAmt'];
             this.totalInvest = res['BalanceAmt'];
            }else{
             this.alertMessage('Error',res['ExtendedMessage']);
           }
         }, err =>{
           //this.hideLoader();
           this.alertMessage('Error','Unable fetch transaction details.');
         }
       )
     } catch (error) {
       console.log(error);
       this.alertMessage('Error','Unable fetch transaction details.');
     }
  }
  


/**
 * For showing in save till date amount
 */

 checkcurrentSavings1(){
  this.currentSavings1 = 0;
  this.transationDetails.TransactionDetails.forEach(element => {
    
     if(element.TransType == 1){
      this.currentSavings1 += element.TransAmount;
     } 
     else if(element.TransType == 2){
      this.currentSavings1 -= element.TransAmount;
     }
 })
}

getTotalInvestment() {
  this.totalInvest = 0;
  this.transationDetails.TransactionDetails.forEach(element => {
    
     if(element.TransType == 1){
      this.totalInvest += element.TransAmount;
     } 
 })
}

getBankAccountDetailsOfUser1(){
  try {
    // this.showLodder('Getting list of banks..');
       this.transationService.getAccountDetailsByNumber(this.currentUserDetails.PhoneNumber).subscribe(
         res => {
           //this.hideLoader();
           if(res['Success']){
             this.userAccountDetails = res['Account'];
             localStorage.setItem('userAccountDetails', JSON.stringify(res['Account']))
             this.getTransationDetails1(res['Account']['FolioNumber']);
             
           } else {
            // this.alertMessage('Error', res['ErrorMessage']);
           }         
         }, err =>{
          //this.hideLoader();   
           this.alertMessage('Error', 'Server issue occured, please try again later')
         }
       )
  } catch (error) {
    this.alertMessage('Error','Sorry not able to fetch bank details')
  }
}


/**
 * Get bank details by payment type accordingly
 */
getTransationDetails1(FolioNumber){
  this.transationService.getTransationHistory(FolioNumber).subscribe(
    res =>{
      //this.hideLoader();
      //console.log(res);
      if(res['Success']){
        this.pathWidth = document.getElementById('auto-path').offsetWidth;
       
        this.transationDetails = res;
        localStorage.setItem('transationDetails', JSON.stringify(res))
        //console.log(this.transationDetails)
        //this.checkInvestWithdraw();
        //this.checkcurrentSavings1();
        this.currentSavings1 = res['BalanceAmt'];
        //this.getTotalInvestment();
        this.totalInvest = res['BalanceAmt'];
        this.mileStones = (res['GoalDetail'])? res['GoalDetail']['MileStone']: 0;
        this.goalDetails = res['GoalDetail'];
        this.totalNumberSet = Math.ceil((this.mileStones.length-3)/3);
        this.permonthAmount = res['GoalDetail']['MileStone'][0]['MileStoneAmount'];
        this.currentMilestone = res['GoalDetail']['CurrentMileStone'];
        this.goalAmount = res['GoalDetail']['GoalAmount'];
        this.autoPosition = (((this.totalInvest * 100)/this.goalAmount) * this.pathWidth)/100 + 'px';  
      }else{
        this.alertMessage('Error',res['ExtendedMessage']);
      }
    }, err =>{
      //this.hideLoader();
      this.alertMessage('Error','Unable fetch faq details.');
    }
  )
}
  /**
   * end of class
   */
}
