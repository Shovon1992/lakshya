import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-startkyc',
  templateUrl: './startkyc.page.html',
  styleUrls: ['./startkyc.page.scss'],
})
export class StartkycPage implements OnInit {
  constructor(public  router:  Router,public translate: TranslateService) { }
  ngOnInit() {
  }
  continuekyc(){
    this.router.navigateByUrl('uploadpan');
  }
}
