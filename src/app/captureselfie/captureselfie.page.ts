import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture/ngx';
import { RegisterService } from './../services/register.service';
import { Base64 } from '@ionic-native/base64/ngx';
import { File } from '@ionic-native/file/ngx';
import { BaseComponent } from '../base/base.component';
import { CreateThumbnailOptions } from '@ionic-native/video-editor/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
@Component({
  selector: 'app-captureselfie',
  templateUrl: './captureselfie.page.html', 
  styleUrls: ['./captureselfie.page.scss'],
})
export class CaptureselfiePage extends BaseComponent implements OnInit {
    fileTransfer
    videosrc: any = [];
    ttl: String = "3 years";
    videourl: any;
    showVideo: any;
    thumbImage: any;
    showThumb: any;
    makeBtnDsable = false;
    buttonText = "Continue";
    videoFile: any;
    fileContent = {
        video : null,
        image : null,
    }
    ngOnInit() {
        //this.showVideo = this.webview.convertFileSrc(this.videosrc);
        this.captureVideo();
        //this.fileTransfer =  this.transfer.create()
    }
    video() {
        this.videoEditor.transcodeVideo({
            fileUri: '/path/to/input.mp4',
            outputFileName: 'videoSelfi.mp4',
            outputFileType: 0,
        })
    }

    captureVideo() {
        let options: CaptureVideoOptions = {
            limit: 1,
            duration: 2,
            quality: 3
        }
        this.mediaCapture.captureVideo(options).then(
            (data: MediaFile[]) => {
                this.videosrc = data[0]['fullPath'];
                var dirpath = data[0]['fullPath'].substr(0, data[0]['fullPath'].lastIndexOf('/') + 1);
                dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
                this.uploadingFileToSZServer(dirpath,data[0]['name'],'video')
                this.createThumbnail(data[0]['fullPath'], data[0]['name']);
                console.log(data);
            },
            (err: CaptureError) => console.log(err));
    }
    createThumbnail(videodata, videoname) {
        let thumbnailoption: CreateThumbnailOptions = {
            fileUri: videodata,
            quality: 100,
            atTime: 1,
            outputFileName: videoname.replace('.mp4', ''),
        }
        this.videoEditor.createThumbnail(thumbnailoption).then((thumbnailPath) => {
            console.log("Thumbnail Responce =>", thumbnailPath);
            var dirpath =thumbnailPath.substr(0, thumbnailPath.lastIndexOf('/') + 1);
            dirpath = dirpath.includes("file://") ? dirpath : "file://" + dirpath;
            let fileName = thumbnailPath.substr(-1, thumbnailPath.lastIndexOf('/') + 1);
            console.log(dirpath,videoname.replace('.mp4', '')+'.jpg','video')
            this.uploadingFileToSZServer(dirpath,videoname.replace('.mp4', '')+'.jpg','image')
        }).catch((err) => {
            console.log("Thumbnail Responce Error=>", err)
        })
    }
    continue () {
        //if (this.currentUserDetails['MerchantId'] !== null) {                  
            const urlparam = '/KYC/UploadVideoSelfie';
            const videogroup = {
                "PhoneNumber": localStorage.getItem('phoneno'),
                "MerchantId": "NA",
                "OnBoardToken": "NA",
                // "MerchantId": this.currentUserDetails['MerchantId'],
                // "OnBoardToken": this.currentUserDetails['OnBoardToken'],
                "ImageUrl": {
                    "FileName": this.currentUserDetails['MerchantId']+"_selfi_image.jpeg",
                    "FileContentBase64": null
                },
                "VideoUrl": {
                    "FileName": this.currentUserDetails['MerchantId']+"_selfiVideo.mp4",
                    "FileContentBase64": null
                },
                "PhotoSelfieUrl": this.fileContent.image,
    "VideoSelfieUrl": this.fileContent.video
            };
            console.log(videogroup)
            this.makeBtnDsable = true;
            if (videogroup.PhotoSelfieUrl && videogroup.VideoSelfieUrl) {
                this.buttonText = "Uploading Image..";
                this.kycService.uploadVideoSelfi(videogroup).subscribe(result => {
                    this.makeBtnDsable = false;
                    this.buttonText = "Continue";
                    console.log(result)
                    if (result['Success']) {
                        this.currentUserDetails.Status = 7;
                        localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails));
                        // this.router.navigateByUrl('homeTab');
                        this.router.navigateByUrl('esign');
                    } else {
                        this.alertMessage('Error !!', (result['ExtendedMessage'])? result['ExtendedMessage']: result['ErrorMessage'])
                    }
                }, err => {
                    console.log(err);
                    this.alertMessage('Error !!', 'Something went wrong ,please relogin after sometime')

                });
            } else {
                this.alertMessage('Error !!', 'Please Capture video first')
            }

        // }
        // else {
        //     this.alertMessage('Error', 'Something went wrong ,please relogin after sometime')
        //     return false;
        // } 
    }


    /**
     * 
     * @param dirpath 
     * @param filename 
     * @param contentType 
     */
    uploadingFileToSZServer(dirpath, filename,contentType){
        let tempFileName = this.date.getTime() + '_' + filename;
        this.file.copyFile(dirpath, filename, this.file.dataDirectory, tempFileName).then(
            resss => {
                console.log(resss.nativeURL);
                let url = "https://persist.signzy.tech/api/files/upload";
                let parameter = new Object();
                parameter['ttl'] = this.ttl
                let options: FileUploadOptions = {
                    mimeType: (contentType == 'video') ? 'video/mp4': '',
                    params: parameter,
                    fileName: tempFileName,
                }
                this.transfer.create().upload(resss.nativeURL, url, options).then((data: any) => {
                    let returnData = JSON.parse(data.response)
                    console.log('fielupload success', data, returnData.file.directURL)
                    if(contentType == "video"){
                        this.fileContent.video = returnData.file.directURL;
                    } else if(contentType == "image"){
                        this.fileContent.image = returnData.file.directURL;
                    }
                }, (err) => {
                    console.log('file upload error', err)
                    // error
                });
            }, err => {
                console.log('file copy error', err)
            });
    }
    /**
     * End of class
     */
}