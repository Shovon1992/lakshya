import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-docconfirmation',
  templateUrl: './docconfirmation.page.html',
  styleUrls: ['./docconfirmation.page.scss'],
})
export class DocconfirmationPage implements OnInit {
 page: String = ''; side: String = '';
 headertext: String = '';uploadimg:any ='/assets/images/aadhaarcard.png';
  constructor(private  router:  Router, private translate: TranslateService,) { }
  ngOnInit() {

    this.page = this.router.getCurrentNavigation().extras.state.page;
    this.uploadimg = this.router.getCurrentNavigation().extras.state.uploadimg;
     
    if(this.page === 'uploadpan'){
     this.headertext = 'PAN';
    }
    if(this.page === 'uploadpoa'){
      this.headertext = 'POA';
      this.side = this.router.getCurrentNavigation().extras.state.side;

     }
     if(this.page === 'uploadsign'){
      this.headertext = 'Signature';
    //  this.side = this.router.getCurrentNavigation().extras.state.side;
     }
      
  }
  gotoupload(){
    if(this.page === 'uploadpan'){
      this.router.navigateByUrl('uploadpan');
     }
     if(this.page === 'uploadpoa'){
    this.router.navigateByUrl('poaimage');
    
      }
     if(this.page === 'uploadsign'){
      this.router.navigateByUrl('uploadsign');
      //  this.side = this.router.getCurrentNavigation().extras.state.side;
     }
   
  }

}
