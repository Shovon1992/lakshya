import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { DocconfirmationPage } from './docconfirmation.page';

const routes: Routes = [
  {
    path: '',
    component: DocconfirmationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,TranslateModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DocconfirmationPage]
})
export class DocconfirmationPageModule {}
