import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VideosdetailsPage } from './videosdetails.page';
import { CustomComponentModule } from 'src/app/customComponents/custom-component/custom-component.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: VideosdetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomComponentModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [VideosdetailsPage]
})
export class VideosdetailsPageModule {}
