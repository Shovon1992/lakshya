import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../../services/register.service';
import { DomSanitizer } from '@angular/platform-browser';
import { BaseComponent } from 'src/app/base/base.component';
@Component({
  selector: 'app-videosdetails',
  templateUrl: './videosdetails.page.html',
  styleUrls: ['./videosdetails.page.scss'],
})
export class VideosdetailsPage extends BaseComponent implements OnInit {
  videos:any = [];
  lang ="en";
  counter = 0;
  language = localStorage.getItem('lang');
  lang_intro = '';
  lang_save = '';
  lang_about = '';
  lang_fee = '';

  title_intro = 'Introduction to Lakshya';
  title_save = 'How to save in Lakshya';
  title_about = 'Frequently Asked Questions- About Lakshya';
  title_fee = 'Save for school fees with Lakshya';

  ngOnInit() {
    this.lang ="en";
    this.events.subscribe('reloadVideoLibrary', (data) =>{
      console.log("DEeeeeeeeeeeeeeeeeeeeeee");
      this.lang = '';
      this.lang_intro = this.lang;
      this.lang_save = this.lang;
      this.lang_about = this.lang;
      this.lang_fee = this.lang;
    });
  }

  ionViewDidEnter() {  
    this.lang ="en";
    this.lang_intro = this.lang;
    this.lang_save = this.lang;
    this.lang_about = this.lang;
    this.lang_fee = this.lang;
  }
 
  // ionViewWillLeave() {
  //   this.lang = '';
  //   this.lang_intro = this.lang;
  //   this.lang_save = this.lang;
  //   this.lang_about = this.lang;
  //   this.lang_fee = this.lang;
  //   this.lang = 'en';
  // }
/**
 * Get the video list
 */
  showIntroVideos(lang, type){
    this.lang_intro = lang;

    switch(type){
      case 'intro':
        this.lang_intro = this.lang_intro;
        break;
      case 'save':
        this.lang_save = this.lang_intro;
        break;
      case 'about':
        this.lang_about = this.lang_intro;
        break;
      case 'fee':
          this.lang_fee = this.lang_intro;
          break;
      default:
          break;
    }    
  }

  /**
 * Get the video list
 */
showSaveVideos(lang, type){
  this.lang_save = lang;

  switch(type){
    case 'intro':
      this.lang_intro = this.lang_save;
      break;
    case 'save':
      this.lang_save = this.lang_save;
      break;
    case 'about':
      this.lang_about = this.lang_save;
      break;
    case 'fee':
        this.lang_fee = this.lang_save;
        break;
    default:
        break;
  }    
}

/**
 * Get the video list
 */
showAboutVideos(lang, type){
  this.lang_about = lang;

  switch(type){
    case 'intro':
      this.lang_intro = this.lang_about;
      break;
    case 'save':
      this.lang_save = this.lang_about;
      break;
    case 'about':
      this.lang_about = this.lang_about;
      break;
    case 'fee':
        this.lang_fee = this.lang_about;
        break;
    default:
        break;
  }    
}

/**
 * Get the video list
 */
showFeeVideos(lang, type){
  this.lang_fee = lang;

  switch(type){
    case 'intro':
      this.lang_intro = this.lang_fee;
      break;
    case 'save':
      this.lang_save = this.lang_fee;
      break;
    case 'about':
      this.lang_about = this.lang_fee;
      break;
    case 'fee':
        this.lang_fee = this.lang_fee;
        break;
    default:
        break;
  }    
}
} 
  /**
   * End of class
   */
  // showVideos(){
  //   const urlparam = '/UserHelp/GetVideos?language=';
  //   const videoGroup ={
  //     'language':localStorage.getItem('lang')?"english":"kannada"}
  //     this.registerservice.postCall(urlparam,videoGroup,true).subscribe(result => {
  //       console.log('result',result);
  //       this.videogroup = result['VideosList'];
  //        }, err => {
  //          alert(JSON.stringify(err))
  //        });
  // }
 

      /*
          
      switch(lang) {
        case 'en':
          this.vidoLink_intro = 'https://www.youtube.com/embed/47ow5Rsx3QY';
          this.vidoLink_save = 'https://www.youtube.com/embed/47ow5Rsx3QY';
          this.vidoLink_about = 'Not Available';
          this.vidoLink_fee = 'Not Available';
          break;
        case 'ka':
          this.vidoLink_intro = 'https://www.youtube.com/embed/0DiQC9fxHfg';
          this.vidoLink_save = 'https://www.youtube.com/embed/R2ygVhWLx5E';
          this.vidoLink_about = 'https://www.youtube.com/embed/2vgyjq_Nlig';
          this.vidoLink_fee = 'https://www.youtube.com/embed/e56hJgUFOUg';
          break;
        case 'hn':
          this.vidoLink_intro = 'https://www.youtube.com/embed/zvrRziO-OcE';
          this.vidoLink_save = 'https://www.youtube.com/embed/ELyDfTj7O4E';
          this.vidoLink_about = 'Not Available';
          this.vidoLink_fee = 'Not Available';
          break;
        default:
          this.vidoLink_intro = 'https://www.youtube.com/embed/47ow5Rsx3QY';
          this.vidoLink_save = 'https://www.youtube.com/embed/47ow5Rsx3QY';
          this.vidoLink_about = 'Not Available';
          this.vidoLink_fee = 'Not Available';
          break;
      }


      try {
        let language = lang
        this.showLodder('Loading video details..');
        this.utilService.getVideoDetails(language).subscribe(
          res =>{
            this.hideLoader();
            console.log(res);
            if(res['Success']){
              this.videos = res['VideosList'];;
            }else{
              this.alertMessage('Error',res['ExtendedMessage']);
            }
          }, err =>{
            this.hideLoader();
            this.alertMessage('Error','Unable fetch video details.');
          }
        )
      } catch (error) {
        console.log(error);
        this.alertMessage('Error','Unable fetch video details.');
      }

      */