import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';
import { PassbookFilterPage } from './passbook-filter/passbook-filter.page';
import { last } from 'rxjs/operators';

@Component({
  selector: 'app-transation-history',
  templateUrl: './transation-history.page.html',
  styleUrls: ['./transation-history.page.scss'],
})
export class TransationHistoryPage extends BaseComponent implements OnInit {
  userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'))
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  transationDetails: any =[];
  allTransationDetails: any =[];
  transationList: any =[];
  startDate:any;
  lastDate:any;
  status: any;


  ionViewDidEnter() {
    this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'));
    this.status =parseInt(this.currentUserDetails['Status']);
    console.log(this.status)


    if(this.userAccountDetails && this.userAccountDetails.FolioNumber){
      this.getTransationHistory(this.userAccountDetails.FolioNumber);
    }else if (this.status && this.status > 7){
      this.getBankAccountDetailsOfUser1()
    }       
  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.ionViewDidEnter();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 20);
  }

  async openModal() {
    console.log("Open modal"); 
    const modal = await this.modalController.create({
        component: PassbookFilterPage,
        cssClass: 'passbookFilter'
    });
 
    modal.onDidDismiss().then((dataReturned) => {
      console.log("dataReturned");  
      let transactinType = dataReturned['data']['transactinType'];
      let monthWise = dataReturned['data']['monthWise'];
      let modalStartDate = dataReturned['data']['startDate'];
      let modalEndDate = dataReturned['data']['endDate'];
      console.log(transactinType);
      console.log(monthWise);
      console.log(modalStartDate);
      console.log(modalEndDate);
    
      this.transationList =  this.allTransationDetails['TransactionDetails'];
      
      for(var i=0; i<this.transationList.length; i++){
        var splitted = this.transationList[i].TransDate.split(" ",2)[0];
        let date_day =  splitted.split("/",3)[0];
        let date_month =  splitted.split("/",3)[1];
        let date_year =  splitted.split("/",3)[2];
        let new_date = date_year + '-' + date_month + '-' +date_day;
        let newDate = new Date(new_date);
        
        this.transationList[i].newDate =  newDate;
      }  

      


      if (transactinType) {
        if (transactinType == 'saving') {
          this.transationList =  this.transationList.filter(function(transaction) {
            return transaction.TransType == 1;
          });
        }
        else if (transactinType == 'withdrawal') {
          this.transationList =  this.transationList.filter(function(transaction) {
            return transaction.TransType == 2;
          });
        } else if (transactinType == 'Success' || transactinType == 'inProcess'  || transactinType == 'Failed' ) {
          let tempList = []
          this.allTransationDetails['TransactionDetails'].forEach(element => {
            if(element['Status'] == transactinType ){
              tempList.push(element)
              //console.log(element)
            }
          });
          this.transationList = tempList;
        }
      }
      console.log(this.transationList);

      if (monthWise) {
        let date = new Date();        
        if(monthWise == 'lastMonth') {
          date.setMonth(date.getMonth()-1);
        }        
        this.startDate = this.getFirstDateOfMonth(date);
        this.lastDate =  this.getLastDateOfMonth(date);
      }

      if (modalStartDate && modalEndDate) {
        let start_date = new Date(modalStartDate);
        let end_date = new Date(modalEndDate);
        this.startDate = start_date.setHours(0,0,0,0);
        this.lastDate = end_date.setHours(23,59,59,999);
      }

      let updateArr = [];

      if (this.startDate && this.lastDate) {
        for(var i =0; i<this.transationList.length; i++) {
          if (this.transationList[i].newDate >= this.startDate && this.transationList[i].newDate <= this.lastDate) { // && this.transationList[i].newDate <= this.lastDate
            updateArr.push(this.transationList[i]);
          }
        }
        console.log(updateArr);
        this.transationList = updateArr;
        this.startDate = '';
        this.lastDate = '';
      }

    });  

    return await modal.present();
  }

  getFirstDateOfMonth(date) { 
    date.setHours(0,0,0,0);
    let firstDate = new Date(date.getFullYear(), date.getMonth(), 1); 
    
    return firstDate;              
  } 

  getLastDateOfMonth(date) { 
    // date.setHours(23,59,59,999);
    let lastDate = new Date(date.getFullYear(), date.getMonth()+1, 1); 
       
    return lastDate;              
  } 

  getBankAccountDetailsOfUser1(){
    try {
      this.present('Getting list of banks..', 5000);
      this.transationService.getAccountDetailsByNumber(this.currentUserDetails.PhoneNumber).subscribe(
        res => {
          console.log('____________________________----')

          console.log(res);
          this.dismiss();
          if(res['Success']){
            this.userAccountDetails = res['Account'];
            localStorage.setItem('userAccountDetails', JSON.stringify(res['Account']))
            this.getTransationHistory(res['Account']['FolioNumber']);
          } else {
          this.alertMessage('Error', res['ErrorMessage']);
          }         
        }, err =>{
          this.dismiss();
          this.alertMessage('Error', 'Server issue occured, please try again later')
        }
      )
    } catch (error) {
      this.dismiss();
      this.alertMessage('Error','Sorry not able to fetch bank details')
    }
  }


  getTransationHistory(FolioNumber){
    try {
      this.present('Loading your transaction history..', 5000);
      this.transationService.getTransationHistory(FolioNumber).subscribe(
        res =>{
          this.dismiss();
          if(res['Success']){
            console.log(res['TransactionDetails']);
            this.transationList = res['TransactionDetails'];
            this.transationDetails = res;
            this.allTransationDetails = res;
          } else {
            this.alertMessage('Error',res['ExtendedMessage']);
          }
        }, err =>{
          this.dismiss();
          this.alertMessage('Error','No data');
        }
      )
    } catch (error) {
      this.dismiss();
      this.alertMessage('Error','No data.');
    }
  }


  checkDate(date){
    let newDate = new Date(date.substr(0,10));
    return newDate.getDate() + '/' + (newDate.getMonth() + 1)+ '/' + newDate.getFullYear();
  }

  /**
   * Convert string to date 
   */
  convertDate(string){
    return new Date(string)
  }
  /**
   * End of class
   */
}
