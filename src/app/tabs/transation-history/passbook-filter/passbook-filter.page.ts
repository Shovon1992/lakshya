import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-passbook-filter',
  templateUrl: './passbook-filter.page.html',
  styleUrls: ['./passbook-filter.page.scss'],
})
export class PassbookFilterPage implements OnInit {
    
  constructor(public modalCrtl: ModalController,public alertCtrl: AlertController, private navParams: NavParams,public translate: TranslateService,) { }


  selectedOpt = 'transaction';
  selectedDateOpt = false;

  transactinType: any;
  monthWise:any; 
  startDate:any;
  endDate:any;

  buttonStatus = true;

  // !monthWise || !transactinType || (startDate && endDate)

  ngOnInit() {
    this.openTransactionType(this.selectedOpt)
  }
  
  async dismiss() {
    const onClosedData: string = null;
    await this.modalCrtl.dismiss(onClosedData);
  }


  async applySelection() {
    if (this.startDate || this.endDate) {
      if (!(this.startDate && this.endDate)) {
        this.presentAlert('Select start date and end date both.');
        return false;
      }
      else {
        // this.lastLoginDate = Date.parse(localStorage.getItem('lastLogin'));
        let dateStart = Date.parse(this.startDate);
        let dateEnd = Date.parse(this.endDate);
        
        if (dateEnd < dateStart){
          this.presentAlert('Start date should not be greater than end date.');
          return false;
        }
      }
    } else if (!this.monthWise && !this.transactinType) {
      this.presentAlert('Please select atleast one option.');      
      return false;      
    } 

    console.log(this.transactinType);
    console.log(this.monthWise);
    console.log(this.startDate);
    console.log(this.endDate);
    let filterDetails = {
      transactinType: this.transactinType,
      monthWise: this.monthWise,
      startDate: this.startDate,
      endDate: this.endDate,
    };

    console.log(filterDetails);
    // const onClosedData: string = null;
    await this.modalCrtl.dismiss(filterDetails);
  }

  async presentAlert(message) {
    const alert = await this.alertCtrl.create({
      header: 'Error',
      // subHeader: 'Subtitle',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  openModalWithData() {
    return false;
  }

  //Change value based on tab change
  openTransactionType(tabOption) {
    if (tabOption == 'date'){
      this.selectedDateOpt = true;
    } else {
      this.selectedDateOpt = false;
    }
    this.selectedOpt = tabOption;
  }
  

    // document.getElementById(cityName).style.display = "block";
  // Get the element with id="defaultOpen" and click on it
  // document.getElementById("defaultOpen").click();
}
