import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TranslateModule } from '@ngx-translate/core';
import { TransationHistoryPage } from './transation-history.page';
import { CustomComponentModule } from 'src/app/customComponents/custom-component/custom-component.module';
// import { PassbookFilterPageModule } from 'src/app//tabs/transation-history/passbook-filter/passbook-filter.module';

const routes: Routes = [
  {
    path: '',
    component: TransationHistoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,TranslateModule,
    RouterModule.forChild(routes),
    CustomComponentModule,
    // PassbookFilterPageModule,
  ],
  declarations: [TransationHistoryPage]
})
export class TransationHistoryPageModule {}
