import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FaqPage } from './faq.page';
import { CustomComponentModule } from 'src/app/customComponents/custom-component/custom-component.module';

const routes: Routes = [
  {
    path: '',
    component: FaqPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FaqPage]
})
export class FaqPageModule {}
