import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage extends BaseComponent implements OnInit {

  faqs: any =[];
  ngOnInit() {
    this.getFaqDetails();
  }

  getFaqDetails(){
    try {
      //this.showLodder('Fetching faq details..');
      this.utilService.getFaq().subscribe(
        res =>{
          //this.hideLoader();
          if(res['Success']){
            this.faqs = res['Category'];
          }else{
            this.alertMessage('Error',res['ExtendedMessage']);
          }
        }, err =>{
          //this.hideLoader();
          this.alertMessage('Error','Unable fetch faq details.');
        }
      )
    } catch (error) {
      console.log(error);
      this.alertMessage('Error','Unable fetch faq details.');
    }
  }

  /**
   * Confirmation alert
   */
  async getBack(){                   
    //this.navCtrl.navigateRoot('homeTab');
    this.navCtrl.back()
  }
  /**
   * End of class
   */
}
