import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
 
import { IonicModule } from '@ionic/angular';
 
import { TabPage } from './tab.page';
import { TranslateModule } from '@ngx-translate/core'; 
 
const routes: Routes = [
  {
    path: 'tabs',
    component: TabPage,
    children:[
        { path: 'home', loadChildren: '../home/home.module#HomePageModule' },
        { path: 'soa', loadChildren: '../soa/soa.module#SoaPageModule' },
        { path: 'gold', loadChildren: '../gold/gold.module#GoldPageModule' },
        {path: 'support', loadChildren: '../supportdetails/supportdetails.module#SupportdetailsPageModule'},
        {path: 'video', loadChildren: '../videosdetails/videosdetails.module#VideosdetailsPageModule'},
        {path: 'passbook', loadChildren: '../transation-history/transation-history.module#TransationHistoryPageModule'},
    ]
  },
  {
    path:'',
    redirectTo:'/homeTab/tabs/home',
    pathMatch:'full'
  },
  {
    path:'gold',
    redirectTo:'/homeTab/tabs/gold',
    pathMatch:'full'
  },
  {
    path:'passbookTab',
    redirectTo:'/homeTab/tabs/passbook',
    pathMatch:'full'
  },
  {
    path:'soaTab',
    redirectTo:'/homeTab/tabs/soa',
    pathMatch:'full'
  }
];
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [TabPage]
})
export class TabPageModule {}
