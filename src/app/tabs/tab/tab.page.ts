import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.page.html',
  styleUrls: ['./tab.page.scss'],
})
export class TabPage extends BaseComponent implements OnInit {

  ngOnInit() {
    this.events.subscribe('reloadVideoLibrary', (data) =>{
      this.ionViewDidEnter();
    });
  }


  ionViewDidEnter() {  }

  // This will be executed before ionChange event
  public tapped() {
    this.events.publish('reloadVideoLibrary', 'Load '); 
  }
}
