import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SupportdetailsPage } from './supportdetails.page';
import { CustomComponentModule } from 'src/app/customComponents/custom-component/custom-component.module';

const routes: Routes = [
  {
    path: '',
    component: SupportdetailsPage
  }
];

@NgModule({ 
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,TranslateModule,
    CustomComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SupportdetailsPage]
})
export class SupportdetailsPageModule {}
