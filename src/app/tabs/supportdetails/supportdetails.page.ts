import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/base/base.component';

@Component({
  selector: 'app-supportdetails',
  templateUrl: './supportdetails.page.html',
  styleUrls: ['./supportdetails.page.scss'],
})
export class SupportdetailsPage extends BaseComponent implements OnInit {
  
  supportDetails: any =[];
  supportEmail = '';
  ngOnInit() {
    this.getFaqDetails();
  } 

  getFaqDetails(){
    try {
     // this.showLodder('Fetching support details..');
      this.utilService.getSupportDetails().subscribe(
        res =>{
          //this.hideLoader();
          console.log(res);
          if(res['Success']){
            this.supportDetails = res['Support'];
            this.supportEmail = this.supportDetails.Email;
          }else{
            this.alertMessage('Error',res['ExtendedMessage']);
          }
        }, err =>{
          //this.hideLoader();
          this.alertMessage('Error','Unable fetch faq details.');
        }
      )
    } catch (error) {
      console.log(error);
      this.alertMessage('Error','Unable fetch faq details.');
    }
  }
  /**
   * End of class
   */
}
