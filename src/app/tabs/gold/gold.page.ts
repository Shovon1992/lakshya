import { Component, OnInit } from '@angular/core';
import {BaseComponent} from 'src/app/base/base.component';

@Component({
  selector: 'app-gold',
  templateUrl: './gold.page.html',
  styleUrls: ['./gold.page.scss'],
})
export class GoldPage extends BaseComponent implements OnInit {

  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));

  ngOnInit() {
    this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userImage = 'https://lakresourcevmdiag.blob.core.windows.net/selfiimage/'+this.currentUserDetails.UserId+'.jpg?random+\=' +  new Date();     

    this.events.subscribe('userLogged', (data) => {
      this.userImage = (data['profileImage']) ? data['profileImage'] :'https://lakresourcevmdiag.blob.core.windows.net/selfiimage/'+data['UserId']+'.jpg?random+\=' +  new Date();    

    });
  }

}
 