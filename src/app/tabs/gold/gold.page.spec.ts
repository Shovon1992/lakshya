import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoldPage } from './gold.page';

describe('GoldPage', () => {
  let component: GoldPage;
  let fixture: ComponentFixture<GoldPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GoldPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoldPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
