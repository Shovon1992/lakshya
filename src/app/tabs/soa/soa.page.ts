import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../base/base.component';
import * as Azure from "@azure/storage-blob";


@Component({
  selector: 'app-soa',
  templateUrl: './soa.page.html',
  styleUrls: ['./soa.page.scss'],
})


export class SoaPage extends BaseComponent implements OnInit {

  supportDetails: any =[];
  supportEmail = '';
  soaBody = '';
  soaSubject = '';
  soaUrl = '';
  currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'));
  folioNumber = '';
  fileName = '';
  fileDirectory = '';
  fileExist:boolean;

  ngOnInit() {
    this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    this.userAccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'));

    if (this.userAccountDetails !== null && this.userAccountDetails.FolioNumber !== null ) {  
      this.folioNumber = this.userAccountDetails.FolioNumber;
      this.soaBody = 'SOA Statement';
      this.soaSubject = 'SOA Statement';
      this.soaUrl = "https://lakresourcevmdiag.blob.core.windows.net/soa/"+ this.folioNumber+".pdf";
      console.log("SOA RESPONSE>>>>>");
      console.log(this.soaUrl);
      this.fileName = this.folioNumber+".pdf";
      this.fileDirectory = "https://lakresourcevmdiag.blob.core.windows.net/soa";
    } else {
      this.getFaqDetails();
    }
  } 
  /**
   * Helper function to get details    
   */
  getFaqDetails(){
    try {
      this.present('Fetching support details..', 5000);
      this.utilService.getSupportDetails().subscribe(
        res =>{
          //this.hideLoader();
          console.log(res);
          if(res['Success']){
            this.dismiss()
            this.supportDetails = res['Support'];
            this.supportEmail = this.supportDetails.Email;
          }else{
            this.dismiss()
            this.alertMessage('Error',res['ExtendedMessage']);
          }
        }, err =>{
          this.dismiss()
          //this.hideLoader();
          // this.alertMessage('Error','Unable fetch faq details.');
        }
      )
    } catch (error) {
      this.dismiss()
      console.log(error);
      // this.alertMessage('Error','Unable fetch faq details.');
    }
  }  

  downloadAndOpenPdf() {
    let downloadUrl= "https://lakresourcevmdiag.blob.core.windows.net/soa/"+ this.folioNumber+".pdf";
    let path = this.file.dataDirectory;
    const transfer = this.ft.create();

    try {
      transfer.download(downloadUrl, path + 'soa.pdf').then(entry => {
        let url = entry.toURL();
        console.log('---------------------------------------');
        console.log(url);
        if (this.platform.is('ios')) {
          this.document.viewDocument(url, 'application/pdf', {});
        } else {
          this.fileOpener.open(url, 'application/pdf')
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error opening file', e));
        }
      });
    } catch (error) {
      this.alertMessage('Error', 'Sorry SOA not present')
    }
  }


  sendShare(message, subject, url) {
    this.socialSharing.share(message, subject, null, url);
  }  // End of fu


  // https://lakresourcevmdiag.blob.core.windows.net/soa/14986468.pdf
  // https://lakresourcevmdiag.blob.core.windows.net/soa/<folionumber>.pdf
  /*ViewPDFFromUrl() {
    let filename = "https://lakresourcevmdiag.blob.core.windows.net/soa/"+ this.folioNumber+".pdf";
    
     const transfer: FileTransferObject = this.ft.create();
    transfer.download(URL, this.file.dataDirectory + `${filename}.pdf`).then((entry) => {
      const entryUrl = entry.toURL();
      if (this.platform.is('ios')) {
        //// iOS Version
        this.document.viewDocument(entryUrl, 'application/pdf');
      } else {
        this.fileopen.open(entryUrl, 'application/pdf');
      }
    }, (error) => {
     console.log('Failed!', error);
    });

  }
  
  
        this.file.checkFile(this.fileDirectory, this.fileName)
        .then((result) => {
          this.fileExist = true;
          console.log('file exists :' + result);
        },(error) => {
          this.fileExist = false;
          console.log('error : ' + JSON.stringify(error))
      });
      
      **/
}
