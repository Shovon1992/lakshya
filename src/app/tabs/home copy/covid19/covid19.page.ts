import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-covid19',
  templateUrl: './covid19.page.html',
  styleUrls: ['./covid19.page.scss'],
})
export class Covid19Page implements OnInit {

  constructor(public modalCrtl: ModalController, private navParams: NavParams) { }

  ngOnInit() {
  }

  async closeModal() {
    const onClosedData: string = null;
    await this.modalCrtl.dismiss(onClosedData);
  }
}
