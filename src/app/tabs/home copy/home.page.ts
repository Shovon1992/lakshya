import {Component,OnInit} from '@angular/core';
import {BaseComponent} from 'src/app/base/base.component';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Covid19Page } from './covid19/covid19.page';
import { finalize } from 'rxjs/operators';
import { ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})

export class HomePage extends BaseComponent implements OnInit {
    transationDetails: any = [];
    slideOpts = {
        initialSlide: 0,
        speed: 400
    };
                                                                                                                                                                                                                                                                                                                                          
    /**
     * variables for creating calender 
     */
    currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    date: any = new Date;
    daysInThisMonth: any;
    daysInLastMonth: any;
    daysInNextMonth: any;
    monthNames: any = [];
    currentMonth: any;
    currentYear: any;
    currentDate: any;
    temShowMonth: any = [];
    transDDD = [];
    mileStones: any = [];
    goalDetails: any = [];
    getCurrentTransactionList: any =[];
    currentSaving = 0;
    totalNumberSet = 0;
    permonthAmount = 0;
    currentMilestone = 1;
    tansationAmount: any;
    noOfDayInLastM = 0;
    noOfDayInNextM = 0;
    mobileNo: any;
    pin: any;
    status: any;
    username: string = '';
    mobileno: any = '';
    emailid: any = '';
    goalStatus: any;
    nextStop:number = 0;
    currentSavings1 = 0;
    counter = 0;
    loginDetails: any = [];
    goalAmount: any;
    totalInvest: any;
    pathWidth: any;
    autoPosition = 5+"px";
    posRation = 0;
    greenPos1:boolean = false;
    greenPos2:boolean = false;
    greenPos3:boolean = false;
    slider1 = "./assets/images/confetti.svg";
    slider2 = "./assets/images/goal-completion1x.svg";
    dd: any;
    mm: any;
    yyyy:any;
    notifySMS = '';
    paymentDate: Date;
    notifyDate: Date;
    start_date:Date;
    notifyTime: Date;
    notify_date: Date;
    notifyStatus = 0;
    subscription: any              
    ngOnInit() {
        this.transDDD = new Array();
        this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
      
        if (this.currentUserDetails !== null && this.currentUserDetails.PhoneNumber !== null ) {
            this.status =parseInt(this.currentUserDetails['Status']);
      
            if (this.status < 3) {
                this.navCtrl.navigateRoot('verifykyc');
                // this.router.navigateByUrl('verifykyc');
            } else if (this.status > 2 && this.status < 7) {
                this.navCtrl.navigateRoot('kyc-complete-status');
            } 
            else {
                for (let i = 1; i < 34; i++) {
                    this.transDDD.push({
                        'transType': null,
                        'amount': 0
                    })
                }
                
                // document.addEventListener("backbutton",function(e) {
                //     console.log("disable back button")
                // }, false);

                this.events.subscribe('reloadHome', (data) =>{
                    this.ionViewDidEnter();
                    console.log("Suryabhannnnnnnn");
                    console.log(data); // Hello from page1!
                });
            }
        }    
    }

  
      
      
    ionViewDidEnter() { 
        this.subscription = this.platform.backButton.subscribeWithPriority(9999, () => {
            // do nothing
          });
        // document.addEventListener("backbutton",function(e) {
        //     console.log("disable back button")
        // }, false);   
        if (this.currentUserDetails !== null && this.currentUserDetails.PhoneNumber !== null ) {
            this.status =parseInt(this.currentUserDetails['Status']);

            if (this.status < 3) {
                this.navCtrl.navigateRoot('verifykyc');
                // this.router.navigateByUrl('verifykyc');
            } else if (this.status > 2 && this.status < 7) {
                this.navCtrl.navigateRoot('kyc-complete-status');
                // this.router.navigateByUrl('kyc-complete-status');
            } 
            else {
                // document.addEventListener("backbutton",function(e) {
                //     console.log("disable back button")
                // }, false);
                this.menuCtrl.enable(true);    
                
                console.log(this.currentUserDetails);
                if (this.currentUserDetails.POIInformation !== null) {
                    this.username = this.currentUserDetails['POIInformation']['Name'];
                }
                this.mobileno = this.currentUserDetails['PhoneNumber'];
                this.emailid = this.currentUserDetails['EmailId'];
                let data = {'name': this.username, 'mobileno': this.mobileno, 'emailid': this.emailid, 'UserId' : this.currentUserDetails.UserId};
                this.events.publish('userLogged', data);
            
                if (this.status > 7){
                    this.getBankAccountDetailsOfUser();
                    this.getDaysOfMonth();
                }
                else if(this.currentUserDetails.IsGoalCreated){
                    //Call goal list API
                    this.nextStop = 100;
                    this.getGoalDetails();
                }

                /* this.openModal();  Commented this by TP for covid poup*/
            }

            this.paymentDate = JSON.parse(localStorage.getItem('paymentDate'));
            this.notifyStatus = JSON.parse(localStorage.getItem('notifyStatus'));
            this.notifyDate = JSON.parse(localStorage.getItem('notifyDate'));
            
            let today = new Date();

            if (this.paymentDate !== null) {
                this.start_date = new Date(new Date(this.paymentDate).getTime() + 60 * 60 * 24 * 1000);                                
                if (this.notifyDate != null) {
                    this.notify_date = new Date(new Date(this.notifyDate).getTime() + 60 * 60 * 24 * 1000);
                } else {
                    localStorage.setItem('notifyDate', JSON.stringify(new Date()));
                    this.notify_date = new Date(new Date().getTime() + 60 * 60 * 24 * 1000);
                }

                if (this.notify_date < today && this.notifyStatus == 2) {
                    localStorage.setItem('notifyStatus', JSON.stringify(1));
                    this.notifyStatus = 1;
                } 
            } else {
                localStorage.setItem('paymentDate', JSON.stringify(new Date()));
                localStorage.setItem('notifyDate', JSON.stringify(new Date()));
                localStorage.setItem('notifyStatus', JSON.stringify(1));
                this.start_date = new Date(new Date().getTime() - 60 * 60 * 48 * 1000);
                this.notifyStatus = 1;
            }     

            // if (today > this.start_date && this.notifyStatus < 2) {
            //     this.dd = today.getDate();
            //     this.mm = today.getMonth()+1; 
            //     this.yyyy = today.getFullYear();
        
            //     if(this.dd<10) {
            //         this.dd='0'+this.dd;
            //     } 
                
            //     if(this.mm<10) {
            //         this.mm='0'+this.mm;
            //     }
        
            //     if (this.currentUserDetails.IsGoalCreated) {
            //         //below changed by TP to remove the Rupees milestone
            //         this.notifySMS = "Dear Investor, You have missed your saving cycle on " + this.mm+'/'+this.dd+'/'+this.yyyy+" + .You can save more today to reach your goal on time.";
            //         //this.notifySMS = "Dear Investor, You have missed your saving cycle on " + this.mm+'/'+this.dd+'/'+this.yyyy+" of amount Rs. " + this.mileStones + ".You can save more today to reach your goal on time.";
            //     } else {
            //         this.notifySMS = "Dear Investor, You have not set your saving goal. Set a goal to make saving fun";
            //     }
            //     /*Added to get notification for account creation */
            //     if(this.status == 7 ){
            //         console.log("for acct notification");
            //         this.notifySMS = "Dear Investor, your account creation is incomplete. Complete your account creation process to enjoy saving with Lakshya";
            //     }
                            
            //     this.notifyTime = new Date(today.setHours(23,59,59,999));  
            //     this.localNotifications.schedule({
            //         id:1,
            //         title: 'Lakshya Notification',
            //         text: this.notifySMS,
            //        //trigger: { at: new Date(new Date().getTime() + 5000) },
            //         //trigger: { at: this.notifyTime},
            //     });  	        
            //     localStorage.setItem('paymentDate', JSON.stringify(new Date()));
            //     localStorage.setItem('notifyDate', JSON.stringify(new Date()));
            //     localStorage.setItem('notifyStatus', JSON.stringify(2));
            // }
        }
    }
    ionViewWillLeave() {
       // this.subscription.unsubscribe();
        console.log('leaving view')
      }
/*    
Commented this by TP for covid poup
async openModal() {
        console.log("Open modal"); 
        const modal = await this.modalController.create({
            component: Covid19Page,
            cssClass: 'covid19'
        });

        return await modal.present();
    }
*/
    doRefresh(event) {
        console.log('Begin async operation');
        this.ionViewDidEnter();
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
        }, 200);

        // document.addEventListener("backbutton", function (e) {
        //     console.log("disable back button")
        // }, false);
    }

    setAutoStyles() {
        let styles = {
            'width': "40px",
            'margin-bottom': "3px",
            'margin-top': '-43px',
            'margin-left': this.autoPosition,
            'position': 'relative',
        };
        return styles;
    }

    setPathStyles() {
        let styles = {
            'margin-bottom': '-3px',
            'margin-top': '-20px',
        };
        return styles;
    }

    setGoalStyles() {
        let styles = {
            'display': 'inline',
            'width': '40px',
            'margin-bottom': '-8px',
            // 'margin-left': '40px',
            'float': 'right',
            'filter': 'invert(19%) sepia(165%) saturate(3270%) hue-rotate(1300deg) brightness(85%) contrast(500%)',
            'height': '65px'
        };
        return styles;
    }

    setFlagStyles(selectedFlag) {      
        if ((selectedFlag == 1 && this.greenPos1) || (selectedFlag == 2 && this.greenPos2) || (selectedFlag == 3 && this.greenPos3)) {
            let styles = {
                'display': 'inline',
                'width': '35px',
                'margin-bottom': '-8px',
                'float': 'right',
                'filter': 'invert(50%) sepia(164%) saturate(3007%) hue-rotate(101deg) brightness(95%) contrast(80%)',
                'height': '65px'
            };
            return styles;    
        }
        else {
            let styles = {
                'display': 'inline',
                'width': '35px',
                'margin-bottom': '-8px',
                // 'margin-left': '13%',
                'float': 'right',
                'filter': 'invert(480%) sepia(130%) saturate(7%) hue-rotate(10deg) brightness(74%) contrast(13%)',
                'height': '65px'
            };
            return styles;
        }
    }
    setGreenFlagStyles() {
        let styles = {
            'display': 'inline',
            'width': '25%',
            'margin-bottom': '-8px',
            // 'margin-left': '40px',
            'float': 'right',
            'filter': 'invert(50%) sepia(164%) saturate(3007%) hue-rotate(101deg) brightness(95%) contrast(80%)',
            'height': '65px'
        };
        return styles;
    }
    
    setGrayFlagStyles() {
        let styles = {
            'display': 'inline',
            'width': '40px',
            'margin-bottom': '-8px',
            'margin-left': '40px',
            'float': 'right',
            'filter': 'invert(480%) sepia(130%) saturate(7%) hue-rotate(10deg) brightness(74%) contrast(13%)',
            'height': '65px'
        };
        return styles;
    }

    /**
     * Function for creating a calender 
     */
    getDaysOfMonth() {
        this.temShowMonth = new Array();
        this.daysInThisMonth = new Array();
        this.daysInLastMonth = new Array();
        this.daysInNextMonth = new Array();
        this.currentMonth = this.date.getMonth();
        this.currentYear = this.date.getFullYear();

        if (this.date.getMonth() === new Date().getMonth()) {
            this.currentDate = new Date().getDate();
        } else {
            this.currentDate = 999;
        }

        var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();
        var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
        console.log(prevNumOfDays, firstDayThisMonth);


        for (var i = prevNumOfDays - (firstDayThisMonth - 1); i <= prevNumOfDays; i++) {
            this.temShowMonth.push(i);
            this.noOfDayInNextM
            if (i < 7) {
                this.daysInLastMonth.push(i);
            }

        }

        var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
        for (var i = 0; i < thisNumOfDays; i++) {
            this.temShowMonth.push(i + 1);
            this.daysInThisMonth.push(i + 1);
        }

        var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
        var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
        for (var i = 0; i < (6 - lastDayThisMonth); i++) {
            if (i < 7) {
                this.daysInNextMonth.push(i + 1);
            }

        }
        var totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
        if (totalDays < 36) {
            for (var i = (7 - lastDayThisMonth); i < ((7 - lastDayThisMonth) + 7); i++) {
                this.daysInNextMonth.push(i);
            }
        }
        console.log(this.temShowMonth);
    }


    goToLastMonth() {
        this.date.setMonth(this.date.getMonth()-1);
        this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        this.getDaysOfMonth();
        this.checkInvestWithdraw();
    }

    goToNextMonth() {
        this.date.setMonth(this.date.getMonth()+1);
        this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
        this.getDaysOfMonth();
        this.checkInvestWithdraw();
    }

    
    getBankAccountDetailsOfUser() {
        try {
            this.present('Loading Dashboard ...', 5000);
            
            console.log("currentUserDetails");
            console.log(this.currentUserDetails);


            this.transationService.getAccountDetailsByNumber(this.currentUserDetails.PhoneNumber).subscribe(
                res => {             
                    if (res['Success']) {      
                        this.dismiss();             
                        this.userAccountDetails = res['Account'];
                        console.log('sssssssssssssssssssssssssssssss')
                        console.log(this.userAccountDetails);
                        localStorage.setItem('userAccountDetails', JSON.stringify(res['Account']));
                        // if (!goalCreated && status === 8 && (typeof(res['GoalDetail']) != 'undefined' || res['GoalDetail'] !== null) ) {
           
                        // }
                        // else 
                        if (res['Account'] && res['Account']['FolioNumber']) {
                            this.getTransationDetails1(res['Account']['FolioNumber']);
                        }
                        else {
                            if(this.currentUserDetails.IsGoalCreated){
                                //Call goal list API
                                this.getGoalDetails();
                            }
                        }                        
                    } else {
                        this.dismiss();   
                        this.alertMessage('Error', res['ErrorMessage']);
                    }
                }, err => {
                    this.dismiss(); 
                    this.alertMessage('Error', 'Server issue occured, please try again later')
                }
            )
        } catch (error) {
            this.alertMessage('Error', 'Sorry not able to fetch bank details')
        }
        finalize (() => {
            this.dismiss();
        })
    }
    
 /** 
 * For getting new goal Shovon
 */
getGoalDetails() {
    this.present('Loading Dashboard...', 5000);  
    try {
        // this.goalService.getGoalLists(this.currentUserDetails.PhoneNumber)
       //this.showLodder('Getting your goal..');
       this.goalService.getGoalLists(this.currentUserDetails.PhoneNumber).subscribe(
         res => {
            this.dismiss();
            console.log('----------------------------')
            console.log(res);
            if(res['Success']) {
                if(typeof(res['GoalDetail']) != 'undefined' || res['GoalDetail'] !== null) {     
                    this.mileStones = res['GoalDetail'][0]['MileStone'];
                    this.goalDetails = res['GoalDetail'][0];
                    this.totalNumberSet = Math.ceil((this.mileStones.length-3)/3);
                    this.permonthAmount = res['GoalDetail'][0]['MileStone'][0]['MileStoneAmount']
                    this.goalAmount = res['GoalDetail'][0]['GoalAmount'];
                }
            }
            else {
                this.alertMessage('Error', res['ErrorMessage']);
            }
        }, err =>{
            this.dismiss();
            this.alertMessage('Error', 'Server issue occured, please try again later')
         }
       )
     } catch (error) {
        this.dismiss();
        this.alertMessage('Error', 'Server issue occured, please try again later')
     }
     
   }

    /**
     * Get bank details by payment type accordingly
     */
    getTransationDetails1(FolioNumber) {
        
        try {
            this.present('Loading Dashboard...', 5000);
            this.transationService.getTransationHistory(FolioNumber).subscribe(
              res => {

                this.dismiss();

                console.log('----------------------------')
                console.log(res);
                if (res['Success']) {
                    this.transationDetails = res;
                    this.checkInvestWithdraw();
                    console.log("------------------------------");
                    console.log(res['GoalDetail']);
                    localStorage.setItem('transationDetails', JSON.stringify(res));
                    this.currentSavings1 = parseInt(res['BalanceAmt']);
                    this.totalInvest = parseInt(res['BalanceAmt']);
                    //console.log(this.transationDetails)
                    if(typeof(res['GoalDetail']) != 'undefined' || res['GoalDetail'] !== null) {     
             
                        this.currentGoalDetails(res['GoalDetail'],res['TransactionDetails']);
                        this.currentSaving = 0;
                        this.calculateCurrentSaving();//currentSaving
                        this.goalStatus = (res['GoalDetail'])? res['GoalDetail']['GoalStatus']: 0;
                        this.mileStones = (res['GoalDetail'])? res['GoalDetail']['MileStone']: 0;
                        this.goalDetails = res['GoalDetail'];
                        this.totalNumberSet = Math.ceil((this.mileStones.length-3)/3);
                        this.permonthAmount = parseInt(res['GoalDetail']['MileStone'][0]['MileStoneAmount']);
                        this.currentMilestone = res['GoalDetail']['CurrentMileStone'];
                        this.goalAmount = parseInt(res['GoalDetail']['GoalAmount']);
                        this.nextStop = this.permonthAmount*(Math.floor(this.currentSavings1/this.permonthAmount) + 1);

                        if(this.currentUserDetails.IsGoalCreated){         
                            this.pathWidth = document.getElementById('auto-path').offsetWidth-56;
                            this.posRation = Math.floor(this.pathWidth/4);
                            let autoPos = Math.floor((((this.currentSaving * 100)/this.goalAmount) * this.pathWidth)/100);  
                            let autoExactPos;

                            console.log(autoPos);
                            if (autoPos <= 40) {
                                autoExactPos = Math.floor(5);
                            }
                            else if(autoPos > this.pathWidth) {
                                autoExactPos = Math.floor(this.pathWidth);
                            }
                            else {
                                autoExactPos = Math.floor(autoPos);
                            }

                            this.autoPosition = autoExactPos + 'px';
                            this.greenPos1 = (this.posRation > autoPos) ? false : true;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
                            this.greenPos2 = (this.posRation * 2 > autoPos) ? false : true;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               ;
                            this.greenPos3 = (this.posRation * 3 > autoPos) ? false : true; 
                        }
                    }
                } else {
                    this.alertMessage('Error', res['ExtendedMessage']);
                }
            }, err => {
                this.dismiss();
                this.alertMessage('Error', err);
            })
        }
        catch (error) {
            this.dismiss();
           console.log(error)
           this.alertMessage('Error', 'Server issue occured, please try again later')
        }    
    }

    /**
     * Calculate Current goal
     */
    currentGoalDetails(goalDetails, transationList) {        
        let goalId = goalDetails['GoalId'];
        let goalStatus = goalDetails['GoalStatus'];
        this.getCurrentTransactionList = [];

        if (goalId && goalStatus == 2) {
            for(var i =0; i<transationList.length; i++) {
                if (transationList[i].GoalId == goalId) {
                    this.getCurrentTransactionList.push(transationList[i]);
                }
            }
        }
    }
    calculateCurrentSaving() {//currentSaving
       
        this.getCurrentTransactionList.forEach(element => {
            console.log(element.TransAmount);
            if (element.TransType == 1) {
                this.currentSaving += parseInt(element.TransAmount);
            } else if (element.TransType == 2) {
                this.currentSaving -= parseInt(element.TransAmount);
            }
        });

        console.log(this.currentSaving);
    }

    checkInvestWithdraw() {
        this.transDDD = new Array();
        for (let i = 1; i < 34; i++) {
            this.transDDD.push({
                'transType': null,
                'amount': 0
            })
        }
        let tempDay;

        console.log(this.transationDetails);
        this.transationDetails.TransactionDetails.forEach(element => {
        // this.getCurrentTransactionList.forEach(element => {
                //let dates = new Date(element.TransDate.substr(1,10));
            let day = Number(element.TransDate.substr(0, 2).replace('/', '')); //(dates.getMonth() + 1)+'/'+dates.getDate()+'/'+dates.getFullYear();
            let tempDate = this.date.getMonth() + 1;
            let selectedMonth = element.TransDate.substr(2, 3).replace('/', '');
            let tempYear = this.date.getFullYear();
            let selectedYear = element.TransDate.substr(5, 5).replace('/', '');

            if (tempDate == parseInt(selectedMonth) && tempYear == parseInt(selectedYear)) {
                if (element.TransType == 1) {
                    this.transDDD[day].amount += element.TransAmount;
                } else if (element.TransType == 2) {
                    this.transDDD[day].amount -= element.TransAmount;
                }

                if (this.transDDD[day].amount > 0) {
                    this.transDDD[day].transType = 1;
                }
                else {
                    this.transDDD[day].transType = 2;
                }
                tempDay = day
            }
        });
    }

    getAmount(slot, day) {
        let amount = 0;
        let month = this.date.getMonth() + 1;
        if ((slot > 1 && day > 1) || slot == 1 && day < 8) {
            return Math.abs(this.transDDD[day].amount)
        }
    }


    getTransType(slot, day) {
        //console.log(this.transDDD)
        if ((slot > 1 && day > 1) || slot == 1 && day < 8) {
            //let dayNum = this.temShowMonth[slot*7-day]
            return this.transDDD[day].transType
        }
    }

    /**
     * Check if IsKYCCompleted than allow to withdraw
     */

     checkAndNavigate(path){
        //this.navCtrl.navigateRoot(path);
        console.log(this.currentUserDetails)
        if(this.currentUserDetails.IsKYCCompleted){
            this.navCtrl.navigateRoot(path);
        } else{
            this.translate.get('messages').subscribe(
                res => {
                    console.log(res)
                    this.alertMessage('Alert !', res.kycnc)
                }
            ) // CommonUtility.txtnxtstop
            
        }
     }
    /**
     * End of class
     */
}