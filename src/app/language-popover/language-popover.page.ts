import { LanguageService } from './../services/language.service';
import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { RegisterService } from './../services/register.service';
import { Toast } from '@ionic-native/toast/ngx';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-language-popover',
  templateUrl: './language-popover.page.html',
  styleUrls: ['./language-popover.page.scss'],
})
export class LanguagePopoverPage implements OnInit {
  languages = [];
  selected = ''; header = '';selectedlng='';
  constructor(public translate: TranslateService,public languageService: LanguageService,public  router:  Router,
    public registerservice:RegisterService, public toast: Toast, public navCtrl : NavController) {  
     }
 
  ngOnInit() {
    this.languages = this.languageService.getLanguages();
    this.selected = this.languageService.selected;
  //  this.header = this.translate.instant('SELECTLANGUAGE.languageselection');
  }
 
  select(lng) {
    this.selectedlng = lng;
      }
  selectLanguage(){
   if(this.selectedlng!== ''){
    localStorage.setItem('lang',this.selectedlng);
    this.languageService.setLanguage(this.selectedlng);    
    // this.router.navigateByUrl('videodemo');
    this.router.navigate(['videodemo'], { state: { pageheader: 'lakhshyaworks' } });
  
   }else{
    this.toast.show('Please select language', '5000', 'center').subscribe(
      toast => {
        console.log(toast);
      });
   }

  }

}
