import { Component } from '@angular/core';
import { Platform, Events, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from  "@angular/router";
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { LanguageService } from './services/language.service'; 
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';
import { TransationService } from './services/transationService/transation.service';
// import { AppVersion } from '@ionic-native/app-version';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import { File } from '@ionic-native/file/ngx';
import { environment } from 'src/environments/environment.prod';
import { UtilService } from './services/utilityService/util.service';
//import { CodePush } from '@ionic-native/code-push/ngx';
//import { SyncStatus } from '@ionic-native/code-push/ngx';
import { Market } from '@ionic-native/market/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})

export class AppComponent { 
    currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    useraccountDetails = JSON.parse(localStorage.getItem('userAccountDetails'));
    navigate : any;
    username: string;
    mobileno: any = '';
    emailid: any = '';
    account : any = '';
    nominee : any ='';
    relation: any ='';
    status = 0;
    paymentDate:any;
    currentLoginDate:any;
    lastLoginDate = 0;
    notifyStatus = 0;
    notifyDate:Date;
    notifySMS : any;
    version = environment.appVersion;
    sameVerstion = true;
    ismandatoryUpdate= false;
    UserId : any;
    userImage :any;    

    // ,public appVersion: AppVersion
  constructor(
    public transationService: TransationService,public router: Router,private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar,
      private ga: GoogleAnalytics,public localNotifications: LocalNotifications, private languageService: LanguageService, private uniqueDeviceID:
      UniqueDeviceID,private events: Events,public alertController : AlertController, public navCtrl : NavController, public file : File, public utilService : UtilService,public market: Market) {

    this.events.subscribe('userLogged', (data) => {
      this.username = data['name'];
      this.mobileno = data['mobileno'];
      this.emailid = data['emailid'];
      this.account = data['account'];
      this.nominee  = data['nominee'];
      this.relation = data['relation'];
      this.UserId = data['UserId'];
      this.userImage = (data['profileImage']) ? data['profileImage'] :'https://lakresourcevmdiag.blob.core.windows.net/selfiimage/'+data['UserId']+'.jpg?random+\=' +  new Date();    

    });
    console.log('userImage app', this.userImage)
    this.initializeApp();
    this.getAnalytics();
    this.utilService.getAppVersion().subscribe(
      res =>{
       this.sameVerstion = (this.version == res['Version_Number']) ?  true :false;
       this.ismandatoryUpdate = res['Mandetory'];
        if(! this.sameVerstion){
          this.updateAlert()
        }
       
       //alert(this.sameVerstion)
        console.log('app version' , res)
      },err =>{
        console.log('app version' , err)
      }
    )
  }

  async initializeApp() {
    this.platform.ready().then(() => { 
      this.sendNotificationPurchase() 
      this.notification();
      this.localNotifications.on('click').subscribe(
        res => {
          //alert("notification clicked.");
          console.log(res)
          if(res.id == 12221){
            this.navCtrl.navigateForward('savings')
          }
        }
      );
      
      // this.codePush.sync().subscribe((status) => { 
      //   if( status === SyncStatus.CHECKING_FOR_UPDATE){
      //     alert("Checking for updates.");
      //   }
  
      //      switch(status) {
                  
                      
                    
      //             case SyncStatus.DOWNLOADING_PACKAGE:
      //                 alert("Downloading package.");
      //                 break;
      //             case SyncStatus.INSTALLING_UPDATE:
      //                 alert("Installing update.");
      //                 break;
      //             case SyncStatus.UP_TO_DATE:
      //                   alert("Up-to-date.");
      //                 break;
      //             case SyncStatus.UPDATE_INSTALLED:
      //                 alert("Update installed.");
      //                 break;
      //         }
      //         this.splashScreen.hide();
      // });
      // this.codePush.sync({}, progress =>{

      // }).subscribe((status) => {
      //     this.codePush.sync().subscribe((syncStatus) => console.log(syncStatus));
      //     const downloadProgress = (progress) => { 
      //       console.log(`Downloaded ${progress.receivedBytes} of ${progress.totalBytes}`); 
      //     }
      //     this.codePush.sync({}, downloadProgress).subscribe((syncStatus) => console.log(syncStatus));
      // });
      

      this.statusBar.backgroundColorByHexString('373588b0');
      
      this.languageService.setInitialAppLanguage();
      this.splashScreen.hide();
      this.uniqueDeviceID.get().then((uuid: any) => localStorage.setItem('uuid',uuid)).catch((error: any) => console
          .log(error));
         
          // this.localNotifications.on('click').subscribe(notification => {
          //   localStorage.setItem('notifyStatus', JSON.stringify(1));
          // });
          // this.localNotifications.schedule({
          //      text: 'Delayed ILocalNotification',
          //       trigger: {at: new Date(new Date().getTime() + 3600),in : 10,},
          //       led: 'FF0000',
          //       sound: null,
          //       vibrate: true
          //    });
      // this.appVersion.getAppName();
      // this.appVersion.getPackageName();
      // this.appVersion.getVersionCode();
      // this.appVersion.getVersionNumber();    
        // Schedule delayed notification
        this.platform.resume.subscribe(() => {// foreground
          
          this.currentLoginDate = Date.parse(new Date().toISOString());
          if (localStorage.getItem('lastLogin')) {
            this.lastLoginDate = Date.parse(localStorage.getItem('lastLogin'));
            let diffTime = ((this.currentLoginDate - this.lastLoginDate) / (1000 * 60 * 30 ));
            console.log('comes fore ground',diffTime)
            
            if (diffTime >= 1) {
              this.paymentDate = JSON.parse(localStorage.getItem('paymentDate'));
              this.notifyStatus = JSON.parse(localStorage.getItem('notifyStatus'));
              this.notifyDate = JSON.parse(localStorage.getItem('notifyDate'));
              localStorage.clear();
          
              if (this.paymentDate !== null) {
                  localStorage.setItem('paymentDate', JSON.stringify(this.paymentDate));
                  localStorage.setItem('notifyDate', JSON.stringify(this.notifyDate));
                  localStorage.setItem('notifyStatus', JSON.stringify(this.notifyStatus));
              }
              this.router.navigateByUrl('otplogin');	
            }          
          } 
        });
    });
    console.log('++++++++++++++++++++++++++++++++++', this.useraccountDetails);
  }

  getAnalytics() {
    this.ga.startTrackerWithId('UA-146917096-1').then(() => {
      console.log('Google analytics is ready now');
      this.ga.trackView('test');
      // Tracker is ready
      // You can now track pages or set additional information such as AppVersion or UserId
    }).catch(e => console.log('Error starting GoogleAnalytics', e));
  }

  
  logout() {
    this.paymentDate = JSON.parse(localStorage.getItem('paymentDate'));

    localStorage.clear();

    if (this.paymentDate !== null) {
      localStorage.setItem('paymentDate', JSON.stringify(this.paymentDate));
    }
    this.navCtrl.navigateRoot('otplogin');
  }

  /**
   * NOtification 
   */
  async notification(){
    let today = new Date();
    let dd = (today.getDate() < 10) ? '0'+today.getDate() : today.getDate();
    let mm = (today.getMonth()+1 < 10) ? '0'+ today.getMonth()+1:  today.getMonth()+1; 
    let yyyy = today.getFullYear();
  //this.localNotifications.cancelAll();
  let data = await this.file.readAsText(this.file.dataDirectory,'userinfo.json').then(
      res =>{
        console.log('hurrrrr ',res)
        this.currentUserDetails = JSON.parse(res)
      }
    );
  let isInvested = false;
  let inputFormat = mm+'/'+(today.getDate())+'/'+yyyy
  try {
    await this.file.readAsText(this.file.dataDirectory,'transation.json').then(
      res =>{
        res = JSON.parse(res);
        console.log('hurrrrr trans',res)

          if(res[0]['user'] == this.currentUserDetails.PhoneNumber){
            
            if(res && res[0]['data']){
              for(let i =0 ; i< res.length; i ++){
                var tempDiff = new Date(res[i]['date'])
                var dateDiff = (today.getDate() - tempDiff.getDate() )
                
              }
              console.log(tempDiff,dateDiff)
              if(dateDiff >= 1 ){
                  isInvested = true;
              }
              console.log(tempDiff,dateDiff, isInvested)
            }
          } else {
            let investData = new Array()
            this.file.writeFile(this.file.dataDirectory, 'transation.json', JSON.stringify(investData), {replace: true}).then(
              res =>{
                  console.log('file write res', res)
            })
          }
      }, err =>{
        let investData = new Array()
        console.log(err)
            this.file.writeFile(this.file.dataDirectory, 'transation.json', JSON.stringify(investData), {replace: true}).then(
              res =>{
                  console.log('file write res', res)
            })
      }
    ).catch(
      error =>{
        console.log(error);
        let investData = new Array()
            this.file.writeFile(this.file.dataDirectory, 'transation.json', JSON.stringify(investData), {replace: true}).then(
              res =>{
                  console.log('file write res', res)
            })
      }
    );
  } catch (error) {
    console.log(error)
  }
  //console.log('hurrrrr00000 ', data);
  //this.currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
  if (this.currentUserDetails !== null && this.currentUserDetails.PhoneNumber !== null ) {
    console.log(this.currentUserDetails);
    this.status = parseInt(this.currentUserDetails['Status']);
    console.log(this.status);
    //this.pageNavigation(this.status);          
  } /** else {
    this.navCtrl.navigateRoot('otplogin'); //added 
  } **/

    

    // if (this.currentUserDetails.IsGoalCreated) {
    //     //below changed by TP to remove the Rupees milestone
    //     this.notifySMS = "Dear Investor, You have missed your saving cycle on " + mm+'/'+dd+'/'+yyyy+" + .\nYou can save more today to reach your goal on time.";
    //     //this.notifySMS = "Dear Investor, You have missed your saving cycle on " + this.mm+'/'+this.dd+'/'+this.yyyy+" of amount Rs. " + this.mileStones + ".You can save more today to reach your goal on time.";
    // } else {
    //     this.notifySMS = "Dear Investor, You have not set your saving goal. \nSet a goal to make saving fun";
    // }
    /*Added to get notification for account creation */
    // if(this.status == 7 ){
    //     console.log("for acct notification");
    //     this.notifySMS = "Dear Investor, your account creation is incomplete. \nComplete your account creation process to enjoy saving with Lakshya";
    // }
    console.log(' appp lllllllllllllll',this.currentUserDetails['Status'])
    if(this.currentUserDetails['Status'] < 7){
      this.notifySMS = "Please complete your KYC to complete your profile ";
      this.sendNotification(1,this.notifySMS)
    }
    if(this.currentUserDetails['Status'] === 7){
      this.notifySMS = "Dear Investor, your account creation is incomplete. \nComplete your account creation process to enjoy saving with Lakshya";
      this.sendNotification(2,this.notifySMS)
    } else{
      if (!isInvested) {
        //below changed by TP to remove the Rupees milestone
        this.notifySMS = "Dear Investor, You have missed your saving cycle .\nYou can save more today to reach your goal on time.";
        this.sendNotification(3,this.notifySMS);
        console.log(this.notifySMS);
        //this.notifySMS = "Dear Investor, You have missed your saving cycle on " + this.mm+'/'+this.dd+'/'+this.yyyy+" of amount Rs. " + this.mileStones + ".You can save more today to reach your goal on time.";
      } 
      if(!this.currentUserDetails.IsGoalCreated) {
          
          this.notifySMS = "Dear Investor, You have not set your saving goal. \nSet a goal to make saving fun";
          this.sendNotification(4,this.notifySMS)
      }
    }
      
  }
/**
 * 
 */

 async updateAlert(){
   let buttons = (!this.ismandatoryUpdate) ? [
      
    {
      text: 'Update',
      handler: () => {
        this.openDeveloper();
      }
    },
    {
      text: 'Skip for now',
      role: 'cancel',
      cssClass: 'secondary',
      handler: (blah) => {
        console.log('Confirm Cancel: blah');
      }
    }
  ] : [
      
    {
      text: 'Update',
      handler: () => {
        this.openDeveloper();
      }
    }
  ]

  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: 'Update!',
    message: 'A new update available please update for better performance ',
    buttons: buttons,
    backdropDismiss : false
  });

  await alert.present();
 }
/***
 * 
 */
  sendNotification(id,message){
    this.localNotifications.clearAll();
    let today = new Date();
    this.localNotifications.schedule({
      id: id,
      led: { color: '#FF00FF', on: 500, off: 500 },
      title: 'Lakshya Notification',
      text: message,
      icon: 'http://climberindonesia.com/assets/icon/ionicons-2.0.1/png/512/android-chat.png',
      sound:  '../assets/swiftly.mp3',
      lockscreen: true,
     //trigger: { at: new Date(new Date().getTime() + 5000) },
      trigger: { every: {hour : 12},count : 2},
      //trigger: { in : 2 , every: ELocalNotificationTriggerUnit.MINUTE},
  }); 
  }

  sendNotificationPurchase(){
   /* this.localNotifications.clearAll();
    let today = new Date();
    this.localNotifications.schedule({
      id: 12221,
      led: { color: '#FF00FF', on: 500, off: 500 },
      title: 'Lakshya Notification',
      text: "Open Purchase page",
      icon: 'http://climberindonesia.com/assets/icon/ionicons-2.0.1/png/512/android-chat.png',
      sound:  '../assets/swiftly.mp3',
      lockscreen: true,
     //trigger: { at: new Date(new Date().getTime() + 5000) },
      // trigger: { every: {hour : 12},count : 2},
      trigger: { in : 1 , every: ELocalNotificationTriggerUnit.MINUTE},
      actions: [
        { id: 'yes',  title: 'Yes' , launch: true, },
        { id: 'no',  title: 'No', launch: false}
      ],
      autoClear: true,
      foreground: true 
  }); */
  }
  openDeveloper() {
    //console.log('hiiidkjfdskjhfkjsdfk0-----000-0')
    window.open("https://play.google.com/store/apps/details?id=com.app.lakhshya&hl=en_IN","_system");
  }

  
  
}
