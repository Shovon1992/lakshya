import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-videoselfie',
  templateUrl: './videoselfie.page.html',
  styleUrls: ['./videoselfie.page.scss'],
})
export class VideoselfiePage implements OnInit {

  constructor(private  router:  Router,private translate: TranslateService) { }

  ngOnInit() {
  }
  startvideo(){
    this.router.navigateByUrl('captureselfie');  
  }
  watchvideo(){
    this.router.navigateByUrl('commonkyc');
  }
  pankyc(){}
}
