import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';


import { IonicModule } from '@ionic/angular';

import { SuccesspagePage } from './successpage.page';
import { CustomComponentModule } from '../customComponents/custom-component/custom-component.module';

const routes: Routes = [
  {
    path: '',
    component: SuccesspagePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,TranslateModule,
    RouterModule.forChild(routes),
    CustomComponentModule
  ],
  declarations: [SuccesspagePage]
})
export class SuccesspagePageModule {}
