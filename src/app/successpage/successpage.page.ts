import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';

@Component({
  selector: 'app-successpage',
  templateUrl: './successpage.page.html',
  styleUrls: ['./successpage.page.scss'],
})
export class SuccesspagePage extends BaseComponent implements OnInit {

  firstMilestone = 100;
  ngOnInit() {  }

  ionViewDidEnter() {
    this.getGoalDetails();  
  }

 /** 
 * For getting new goal Shovon
 */
getGoalDetails() {
  this.present('Loading Dashboard...', 5000);  
  try {
    this.goalService.getGoalLists(this.currentUserDetails.PhoneNumber).subscribe(
      res => {
        this.dismiss();
        if(res['Success']){
          // console.log(res['GoalDetail'][0]);
          this.firstMilestone = (res['GoalDetail'][0]['MileStone'][0]['MileStoneAmount']) ? res['GoalDetail'][0]['MileStone'][0]['MileStoneAmount'] : 100;
        }
      }, err =>{
        this.dismiss();
      });
    } catch (error) {
      this.dismiss();
    }   
  }  
}
