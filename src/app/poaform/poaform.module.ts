import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { PoaformPage } from './poaform.page';

const routes: Routes = [
  {
    path: '',
    component: PoaformPage
  }
];

@NgModule({
  imports: [
    CommonModule,TranslateModule,
    FormsModule,ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PoaformPage]
})
export class PoaformPageModule {}
