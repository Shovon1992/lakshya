import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RegisterService } from './../services/register.service';
import { Toast } from '@ionic-native/toast/ngx';
import { LoadingController } from '@ionic/angular';
import { BaseComponent } from '../base/base.component';
import { QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
@Component({
    selector: 'app-poaform',
    templateUrl: './poaform.page.html',
    styleUrls: ['./poaform.page.scss'],
})
export class PoaformPage extends BaseComponent implements OnInit {
    currentUserDetails = JSON.parse(localStorage.getItem('userDetails'));
    public poaGroup: FormGroup;
    poafrontimg: any;
    poabackimg : any;
    selectedState: any;
    selectedDOB:Date;
    alertCounter = 0;
    dlIssue = new Date();
    dlExp = new Date();
    currentStep = 1;
    genderOption = [
        'Male',
        'Female'
    ];
    documentType : any = this.activateRoute.snapshot.paramMap.get('documentType');

    // documentType = '2';
    docs = ['Null', 'aadhaar', 'drivingLicence',  'voterId', 'passport']
    docs2 = ['Null', 'Aadhaar Card', 'Driving Licence', 'Voter Id', 'Passport']
    adharDetails = JSON.parse(localStorage.getItem('adharDetails'))
    adharRegx = new RegExp('^[0-9]+$');

    poaData = {
        "UserId": this.currentUserDetails.UserId,
        "Name": '',
        "Gender": '',// this.form.value.gender;
        "Address": '',
        "DOB": '',
        "City": '',
        "State": '',
        "PinCode": '',
        "POAID": '',
        "District": '',
        "FileName": '',
        "AddressType": this.docs[this.documentType],
        "MerchantId": "NA",
        "OnBoardToken": "NA",
        //"MerchantId": this.currentUserDetails.MerchantId,
        //"OnBoardToken": this.currentUserDetails.OnBoardToken,
        "IssueDate": null,
        "ExpiryDate": null,
        "ImageList": [{
            "FileName": '',
            "FileContentBase64": localStorage.getItem('frontimage')
        }, {
            "FileName": '',
            "FileContentBase64": localStorage.getItem('backimage')
        }],
        //"PhoneNumber": "8519990892",
        "PhoneNumber": localStorage.getItem('phoneno'),
        "POAType": this.docs[this.documentType], //"aadhaar",
        "Success": true,
        "ErrorMessage": "string",
        "ExtendedMessage": "string"
    };
    stateOption = [
       // 'Andaman & Nicobar', 
        'Andhra Pradesh',
        'Arunachal Pradesh',
        'Assam',
        'Bihar',
        'Chandigarh',
        'Chattisgarh',
      //  'Dadra and Nagar Haveli',
       // 'Daman & Diu',
        'Delhi',
        'Goa',
        'Gujarat',
        'Haryana',
        'Himachal Pradesh',
        'Jammu & Kashmir',
        'Jharkhand',
        'Karnataka',
        'Kerala',
      //  'Lakshadweep',
        'Madhya Pradesh',
        'Maharashtra',
        'Manipur',
        'Meghalaya',
        'Mizoram',
        'Nagaland',
        'Odisha',
        'Pondicherry',
        'Punjab',
        'Rajasthan',
        'Sikkim',
        'Tamil Nadu',
        'Telangana',
        'Tripura',
        'Uttar Pradesh',
        'Uttarakhand',
        'West Bengal',
        'Other',
    ];

   
    // adharDetails = {"UserId":10223,"Name":"","Gender":"","Address":"","DOB":null,"City":"","State":"","PinCode":" ","POAID":"","District":"","FileName":"10223_adhar_front.jpeg,10223_adhar_back.jpeg","AddressType":"aadhaar","MerchantId":"5e8f3314664abb291be04fc6","OnBoardToken":"3Bqj9vAUsvK0kkWSiIooejNvWclIV8feAFoxKtEpAxKbWRPsOSo0Z1AQIZOhrFIi","ImageList":null,"PhoneNumber":null,"POAType":"aadhaar","IssueDate":"01/01/2020","ExpiryDate":"01/01/2021","Success":true,"ErrorMessage":null,"ExtendedMessage":null};
    
    ngOnInit() {
        if(this.documentType == 2){
            this.currentStep = 1
        }else{
            this.currentStep = 2
            if(this.documentType == 1 ){
                this.openQr();
            }
        }
        this.poafrontimg = this.sanitizer.bypassSecurityTrustUrl(localStorage.getItem('frontimage'));
        this.poabackimg = this.sanitizer.bypassSecurityTrustUrl(localStorage.getItem('backimage'));
        this.documentType = this.activateRoute.snapshot.paramMap.get('documentType');
        this.adharDetails = JSON.parse(localStorage.getItem('adharDetails'))
        this.poaData.POAID = this.adharDetails['POAID'];
        this.poaData.DOB = this.adharDetails['DOB'];
        // if (this.documentType == '2' || this.documentType == '4') {
        //     this.poaGroup = this.formBuilder.group({
        //         POAID: ['', Validators.required],
        //         name: ['', Validators.required],
        //         gender: ['', Validators.required],
        //         city: ['', Validators.required],
        //         state: ['', Validators.required],
        //         pin: ['', Validators.required],
        //         dob: ['', Validators.required],
        //         address: ['', Validators.required],
        //         District: ['', Validators.required],
        //         ExpiryDate: ['', Validators.required],
        //         IssueDate: ['', Validators.required],
        //     });            
        // } else if (this.documentType == '1' || this.documentType == '3') {
        //     this.poaGroup = this.formBuilder.group({
        //         POAID: ['', Validators.required],
        //         name: ['', Validators.required],
        //         gender: ['', Validators.required],
        //         city: ['', Validators.required],
        //         state: ['', Validators.required],
        //         pin: ['', Validators.required],
        //         dob: ['', Validators.required],
        //         address: ['', Validators.required],
        //         District: ['', Validators.required],
        //         IssueDate: [''],
        //         ExpiryDate: [''],
        //     });       
        // }
    }

    uploadpoa() {

        //if(this.currentUserDetails.MerchantId !== null) {      
            const urlparam = "/KYC/SubmitPOA";
            let IssueDate: any = this.poaData.IssueDate ? new Date(this.poaData.IssueDate) : new Date();
            let ExpiryDate: any = this.poaData.ExpiryDate ? new Date(this.poaData.ExpiryDate) : new Date();
            let forIssueDate:any;
            let forExpiryDate:any;
            
            //console.log(this.poaGroup);
            //mm-dd-yyyy
            if (ExpiryDate != null) {
                forExpiryDate = ExpiryDate.getMonth() +'-'+ ExpiryDate.getDate() + '-'+ ExpiryDate.getFullYear();
            }
            
            if (IssueDate != null) {
                forIssueDate = IssueDate.getMonth() +'-'+ IssueDate.getDate() + '-'+ IssueDate.getFullYear();
            }

            let poagroup = {
                "UserId": this.currentUserDetails.UserId,
                "Name": this.poaData.Name,
                "Gender": this.poaData.Gender,// this.form.value.gender;
                "Address": this.poaData.Address,
                "DOB": this.datePipe.transform(this.poaData.DOB,'MM/dd/yyyy'),
                "City": this.poaData.City,
                "State": this.poaData.State, 
                "PinCode": this.poaData.PinCode,
                "POAID": this.getLat4Digit(this.poaData.POAID),
                "District": this.poaData.District,
                "FileName": this.adharDetails.FileName,
                "AddressType": this.docs[this.documentType],
                "MerchantId": "NA",
                "OnBoardToken": "NA",
                //"MerchantId": this.currentUserDetails.MerchantId,
                //"OnBoardToken": this.currentUserDetails.OnBoardToken,
                "IssueDate": forIssueDate,
                "ExpiryDate": forExpiryDate,
                "ImageList": [{
                    "FileName": this.docs[this.documentType] + "_" + this.poaData.POAID +
                        "_front.jpeg",
                    "FileContentBase64": localStorage.getItem('frontimage')
                }, {
                    "FileName": this.docs[this.documentType] + "_" + this.poaData.POAID +
                        "_back.jpeg",
                    "FileContentBase64": localStorage.getItem('backimage')
                }],
                //"PhoneNumber": "8519990892",
                "PhoneNumber": localStorage.getItem('phoneno'),
                "POAType": this.docs[this.documentType], //"aadhaar",
                "Success": true,
                "ErrorMessage": "string",
                "ExtendedMessage": "string"
            };

            // this.poaData.ImageList = [{
            //         "FileName": this.docs[this.documentType] + "_" + this.poaData.POAID +
            //             "_front.jpeg",
            //         "FileContentBase64": localStorage.getItem('frontimage')
            //     }, {
            //         "FileName": this.docs[this.documentType] + "_" + this.poaData.POAID +
            //             "_back.jpeg",
            //         "FileContentBase64": localStorage.getItem('backimage')
            //     }]
            
            let currentDate = new Date();
            let selectedDOB = new Date(this.poaData.DOB)
            let selecteIssue = this.poaData.IssueDate ? new Date(this.poaData.IssueDate) : new Date();
            let selectedexp = this.poaData.ExpiryDate ? new Date(this.poaData.ExpiryDate) : new Date();

            let diffDays = Math.ceil(Math.abs((selectedDOB.valueOf() - currentDate.valueOf()) / (1000 * 3600 * 24 *
                365)));
            let issueDate = (((selecteIssue.valueOf() - currentDate.valueOf()) / (1000 * 3600 * 24)));
            let expDate = (((selectedexp.valueOf() - currentDate.valueOf()) / (1000 * 3600 * 24)));
    
            if (diffDays > 18) {               
                
                if((this.documentType == '2' || this.documentType == '4') && this.poaData.Name && this.poaData.POAID && this.poaData.Address && this.poaData.City && this.poaData.State && this.poaData.District && this.poaData.PinCode && this.poaData.DOB && this.poaData.IssueDate && this.poaData.ExpiryDate) {              
                    //   dd/mm/yyyy----date set for        
                    let issue_dd = (selecteIssue.getDate() < 10) ? '0'+selecteIssue.getDate() : selecteIssue.getDate();
                    let issue_mm = ((selecteIssue.getMonth()+1) < 10) ? '0' + (selecteIssue.getMonth()+1) : (selecteIssue.getMonth()+1); 
                    let issue_yyyy = selecteIssue.getFullYear();
                    poagroup.IssueDate =  issue_dd + '/' + issue_mm + '/' + issue_yyyy;

                    let exp_dd = (selectedexp.getDate() < 10) ? '0'+selectedexp.getDate() : selectedexp.getDate();
                    let exp_mm = ((selectedexp.getMonth()+1) < 10) ? '0' + (selectedexp.getMonth()+1) : (selectedexp.getMonth()+1); 
                    let exp_yyyy = selectedexp.getFullYear();
                    poagroup.ExpiryDate =  exp_dd + '/' + exp_mm + '/' + exp_yyyy;
                    
                    this.present('Validating ' + this.docs2[this.documentType]+ ' details..',5000);
                    localStorage.setItem('poaDetails', JSON.stringify(poagroup))
                    this.registerservice.postCall(urlparam, poagroup, true).subscribe(result => {
                        console.log(poagroup)
                        this.dismiss();
                        if (result.Success) {
                            //this.currentUserDetails.Status = 4;
                            localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails));
                            this.router.navigateByUrl('infomismatch');
                        } else {
                            let errMessage = (result.ErrorMessage) ? result.ErrorMessage : result.ExtendedMessage;
                            this.alertMessage('Error', errMessage);
                        }
                        //alert(JSON.stringify(result));
                    }, err => {
                        this.dismiss();
                        this.alertMessage('Error', 'Something weent worng pleasse try again');
                    });
                } else if(this.documentType == '1' && this.poaData.Name && this.poaData.POAID && this.poaData.Address && this.poaData.City && this.poaData.State && this.poaData.District && this.poaData.PinCode && this.poaData.DOB){
                    if(this.adharRegx.test(poagroup.POAID)){
                        this.present('Validating ' + this.docs2[this.documentType]+ ' details..',5000);
                    localStorage.setItem('poaDetails', JSON.stringify(poagroup))
                    this.registerservice.postCall(urlparam, poagroup, true).subscribe(result => {
                    
                        this.dismiss();
                        if (result.Success) {
                            //this.currentUserDetails.Status = 4;
                            localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails));
                            this.router.navigateByUrl('infomismatch');
                        } else {
                            let errMessage = (result.ErrorMessage) ? result.ErrorMessage : result.ExtendedMessage;
                            this.alertMessage('Error', errMessage);
                        }
                        //alert(JSON.stringify(result));
                    }, err => {
                        this.dismiss();
                        this.alertMessage('Error', 'Something weent worng pleasse try again');
                    });
                    }else{
                        this.alertMessage('Error', 'Please valid Addhar no');
                    }
                    
                } else if(this.documentType == '3' && this.poaData.Name && this.poaData.POAID && this.poaData.Address && this.poaData.City && this.poaData.State && this.poaData.District && this.poaData.PinCode && this.poaData.DOB){
                    this.present('Validating ' + this.docs2[this.documentType]+ ' details..',5000);
                    localStorage.setItem('poaDetails', JSON.stringify(poagroup))
                    this.registerservice.postCall(urlparam, poagroup, true).subscribe(result => {
                    
                        this.dismiss();
                        if (result.Success) {
                            //this.currentUserDetails.Status = 4;
                            localStorage.setItem('userDetails', JSON.stringify(this.currentUserDetails));
                            this.router.navigateByUrl('infomismatch');
                        } else {
                            let errMessage = (result.ErrorMessage) ? result.ErrorMessage : result.ExtendedMessage;
                            this.alertMessage('Error', errMessage);
                        }
                        //alert(JSON.stringify(result));
                    }, err => {
                        this.dismiss();
                        this.alertMessage('Error', 'Something weent worng pleasse try again');
                    });
                }else{
                    let message = (this.documentType == '2' || this.documentType == '4') ? 'All data are mandatory' : 'All data are mandatory and issue date mast be previous date and Expiry date must be future date '
                    this.alertMessage('Error', message);
                }            
            } else {
                this.alertMessage('Error', 'You age must be greater then 18 years');
            }
        // }
        // else {
        //     this.alertMessage('Error', 'Something went wrong ,please relogin after sometime')
        //     return false;
        // } 
    }
    /**
     * Get lat 4 digit of adhar
     */
    getLat4Digit(string) {
        let digit;
        let adharString = string.toString()
   
        if (this.documentType == '4') {
            digit = adharString;
        }
        else {
            digit = '00000000' + adharString.slice(adharString.length - 4);
        }
        //console.log(string, digit,adharString.length)
        // "POAID": '00000000' + this.getLat4Digit(this.poaGroup.get('POAID').value),

        return digit;
    }
    pincheck(pin){
        
    }
    showAddressMessage(){
        let message = 'Please write complete address as mentioned in your '+ this.docs2[ this.documentType] + ' Incomplete address may lead to cancellaiton of KYC'
        if(this.alertCounter ==0){
            this.alertMessage('Alert !', message);
            this.alertCounter++;
        }
        

    } 



    fetchDl(){

        if(this.poaData.POAID && this.poaData.DOB){
            console.log(this.poaData);
            this.showLodder('loading');
            let dob = new Date(this.poaData.DOB)
            let showMonth = (dob.getMonth()+1) > 9 ? (dob.getMonth()+1) : '0'+(dob.getMonth()+1)
            let sendingDate = dob.getDate()+'/'+showMonth+'/'+dob.getFullYear();
            this.kycService.fetchDL(this.poaData.POAID , this.datePipe.transform(this.poaData.DOB,'dd/MM/yyyy')).subscribe(
                res =>{
                    this.hideLoader();
                    console.log(res,new Date(res['dlValidityto']));
                    if(res['Success']){
                        this.poaData.State = (res['state']) ? res['state'].charAt(0).toUpperCase() + res['state'].substr(1).toLowerCase(): res['state'];
                        this.poaData.Address = res['completeAddress'];
                        this.poaData.PinCode = res['pincode'];
                        this.poaData.IssueDate = (res['dlValidityfrom']) ? this.getDateFormat(res['dlValidityfrom']) : new Date();
                        this.poaData.ExpiryDate = (res['dlValidityto']) ? this.getDateFormat(res['dlValidityto']) : new Date();
                        this.poaData.Name = res['name'];
                        this.poaData.City = res['City'];
                        this.poaData.District = res['District'];
                        this.poaData.Gender = (res['Gender']) ? res['Gender'].charAt(0).toUpperCase() + res['Gender'].substr(1).toLowerCase(): res['Gender'];;
                        console.log(this.poaData);
                    } else{
                        this.alertMessage('Alert', (res['ErrorMessage']) ? res['ErrorMessage'] : res['ExtendedMessage']);
                    }
                    
                    this.currentStep = 2;
                }, err =>{
                    this.alertMessage('Alert', 'All fields mandatary');
                    console.log(err,this.dlIssue, this.dlExp);
                }
            )
            
        } else{
            this.alertMessage('Alert', 'All fields mandatary');
        }
    }

    getDateFormat(date){
        let tempdate = date.split('/')
        let dob = new Date(tempdate[1]+'/'+tempdate[0]+'/'+tempdate[2])
        //let showMonth = (dob.getMonth()+1) > 9 ? (dob.getMonth()+1) : '0'+(dob.getMonth()+1)
        
        return dob;
    }


    openQr(){
        // Optionally request the permission early
this.qrScanner.prepare()
.then((status: QRScannerStatus) => {
   if (status.authorized) {
     // camera permission was granted


     // start scanning
     let scanSub = this.qrScanner.scan().subscribe((text: string) => {
       console.log('Scanned something', text);

       this.qrScanner.hide(); // hide camera preview
       scanSub.unsubscribe(); // stop scanning
     });

   } else if (status.denied) {
     // camera permission was permanently denied
     // you must use QRScanner.openSettings() method to guide the user to the settings page
     // then they can grant the permission from there
   } else {
     // permission was denied, but not permanently. You can ask for permission again at a later time.
   }
})
.catch((e: any) => console.log('Error is', e));
    }
    /**
     * End of class
     */
}